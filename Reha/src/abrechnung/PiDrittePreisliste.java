package abrechnung;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.star.container.NoSuchElementException;
import com.sun.star.lang.IndexOutOfBoundsException;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.sheet.XSheetCellCursor;
import com.sun.star.sheet.XSpreadsheet;
import com.sun.star.sheet.XSpreadsheets;
import com.sun.star.table.XCell;
import com.sun.star.text.XText;
import com.sun.star.uno.UnoRuntime;

import ag.ion.bion.officelayer.application.IOfficeApplication;
import ag.ion.bion.officelayer.application.OfficeApplicationException;
import ag.ion.bion.officelayer.document.DocumentDescriptor;
import ag.ion.bion.officelayer.document.IDocument;
import ag.ion.bion.officelayer.document.IDocumentDescriptor;
import ag.ion.bion.officelayer.document.IDocumentService;
import ag.ion.bion.officelayer.spreadsheet.ISpreadsheetDocument;
import ag.ion.noa.NOAException;
import environment.Path;
import hauptFenster.Reha;


public class PiDrittePreisliste {

    public static IOfficeApplication officeapplication;
    public static String preisDatei;
    public static int startZeile = 1;
    public static int endZeile = 60;
    public static int positionsSpalte = 0;
    public static int preis0Spalte = 3;
    public static int zuzahl0Spalte = 4;
    public static int preis1Spalte = 5;
    public static int zuzahl1Spalte = 6;
    public static int preis2Spalte = 7;
    public static int zuzahl2Spalte = 8;
    public static HashMap<String,List<Double>> preisMap = new HashMap<String,List<Double>>();

    AbrechnungGKV eltern;
    private Logger logger = LoggerFactory.getLogger(PiDrittePreisliste.class);

    public PiDrittePreisliste(AbrechnungGKV xeltern) {
        this.eltern = xeltern;
        if (!Reha.officeapplication.isActive()) {
            try {
                Reha.starteOfficeApplication();
                if (!Reha.officeapplication.isActive()) {
                    JOptionPane.showMessageDialog(null,
                            "Das OpenOffice-System reagiert nicht korrekt!\nAbrechnung wird nicht gestartet");
                    return;
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null,
                        "Das OpenOffice-System reagiert nicht korrekt!\nAbrechnung wird nicht gestartet");
                return;

            }
        }
        preisDatei = Path.Instance.getProghome() + "tools/Hoechstpreise-2022-2023.ods";

        officeapplication = Reha.officeapplication;
    }

    public void starteCalc() {
        ISpreadsheetDocument spreadsheetDocument = null;;
        IDocument document  = null;
        XSheetCellCursor cellCursor = null;
        String sheetName = null;
        try {
            IDocumentService documentService = officeapplication.getDocumentService();
            IDocumentDescriptor docdescript = new DocumentDescriptor();
            docdescript.setHidden(true);
            docdescript.setAsTemplate(false);
            System.out.println("öffne: " + preisDatei);
            document = documentService.loadDocument(preisDatei, docdescript);
            spreadsheetDocument = (ISpreadsheetDocument) document;
            
            XSpreadsheets spreadsheets = spreadsheetDocument.getSpreadsheetDocument().getSheets();
            sheetName= "Tabelle1";
            XSpreadsheet spreadsheet1 = (XSpreadsheet)UnoRuntime.queryInterface(XSpreadsheet.class,spreadsheets.getByName(sheetName));
            cellCursor = spreadsheet1.createCursor();
            
            XCell cell = null;
            XText cellText = null;
            String pos = null;
            Double d1 = null; Double d2 = null;Double d3 = null; Double d4 = null; Double d5 = null; Double d6 = null;

            for(int i = (startZeile); i < (endZeile);i++) {
                try {
                    cell= cellCursor.getCellByPosition(positionsSpalte,i);
                } catch (com.sun.star.lang.IndexOutOfBoundsException e) {
                    e.printStackTrace();
                    break;
                }
                cellText = (XText)UnoRuntime.queryInterface(XText.class, cell);
                pos = cellText.getText().getString().substring(1);
                
                cell= cellCursor.getCellByPosition(preis0Spalte,i);
                cellText = (XText)UnoRuntime.queryInterface(XText.class, cell);
                d1 = Double.parseDouble( (cellText.getText().getString().isEmpty() ? "0.00" : cellText.getText().getString().replace(",", ".")) );
                
                cell= cellCursor.getCellByPosition(zuzahl0Spalte,i);
                cellText = (XText)UnoRuntime.queryInterface(XText.class, cell);
                d2 = Double.parseDouble( (cellText.getText().getString().isEmpty() ? "0.00" : cellText.getText().getString().replace(",", ".")) );

                cell= cellCursor.getCellByPosition(preis1Spalte,i);
                cellText = (XText)UnoRuntime.queryInterface(XText.class, cell);
                d3 = Double.parseDouble( (cellText.getText().getString().isEmpty() ? "0.00" : cellText.getText().getString().replace(",", ".")) );

                cell= cellCursor.getCellByPosition(zuzahl1Spalte,i);
                cellText = (XText)UnoRuntime.queryInterface(XText.class, cell);
                d4 = Double.parseDouble( (cellText.getText().getString().isEmpty() ? "0.00" : cellText.getText().getString().replace(",", ".")) );

                cell= cellCursor.getCellByPosition(preis2Spalte,i);
                cellText = (XText)UnoRuntime.queryInterface(XText.class, cell);
                d5 = Double.parseDouble( (cellText.getText().getString().isEmpty() ? "0.00" : cellText.getText().getString().replace(",", ".")) );

                cell= cellCursor.getCellByPosition(zuzahl2Spalte,i);
                cellText = (XText)UnoRuntime.queryInterface(XText.class, cell);
                d6 = Double.parseDouble( (cellText.getText().getString().isEmpty() ? "0.00" : cellText.getText().getString().replace(",", ".")) );
                
                preisMap.put(pos,Arrays.asList(new Double[]{d1,d2,d3,d4,d5,d6})); // Doubletten?
            }
            document.close();
            officeapplication.getDesktopService().terminate();
            officeapplication.deactivate();

            
        } catch (OfficeApplicationException e) {
            e.printStackTrace();
        } catch (NOAException e) {
            e.printStackTrace();
        } catch (NoSuchElementException e) {
            e.printStackTrace();
        } catch (WrappedTargetException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
                
    }

    public double getPreis(String currHmPos) {
        final int idxAb2303 = 0;
        final int idxAb2301 = 2;
        final int idxBis2212 = 4;
        double preisBis2212 = 0; 
        String posWithoutDiszi = currHmPos.substring(1);
        List<Double> preise4pos = preisMap.get(posWithoutDiszi);
        if (preise4pos == null) {
            logger.error("Preis fehlt: " + currHmPos);
            JOptionPane.showMessageDialog(null,
                    "Preis bis 12-22 fehlt: " + currHmPos);
        } else {
            preisBis2212 = preise4pos.get(idxBis2212);
        }
        return preisBis2212;
    }
}
