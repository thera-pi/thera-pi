package arztFenster;

import java.util.Vector;

import javax.swing.JOptionPane;

import CommonTools.SqlInfo;

public class CheckDocDouble {
    String arztId = null;
    String nName = null;
    String vName = null;
    String lanr = null;
    Boolean lanrIstVergeben = false;

    public CheckDocDouble(String lanr) {
        Vector<Vector<String>> arztDat = SqlInfo.holeFelder(
                "select id, nachname, vorname, arztnum from arzt where arztnum='" + lanr + "' LIMIT 1");         // TODO: was ist mit mehreren Treffern?
        if (arztDat.size() > 0) {
            lanrIstVergeben = true;
            extractFields(arztDat.get(0));
        }
    }

    public CheckDocDouble(String nName, String vName) {
        Vector<Vector<String>> arztDat = SqlInfo.holeFelder(
                "select id, nachname, vorname, arztnum from arzt "
                    + "where nachname sounds like '" + nName 
                    + "' and vorname sounds like '" + vName
                    + "' LIMIT 1");         // TODO: was ist mit mehreren Treffern?
        if (arztDat.size() > 0) {
            extractFields(arztDat.get(0));
        }
    }

    private void extractFields (Vector<String> result) {
        arztId = result.get(0);
        nName = result.get(1);
        vName = result.get(2);
        lanr = result.get(3);
    }
    
    public boolean updateDouble() {
        if (arztId != null) {
            int answer = -1;
            String tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            String end = "<br>";
            String meldung = "<html>Es existiert bereits ein Arzteintrag mit folgenden Daten:<b>" + end
                    + tab + "</b>Name:<b>&nbsp;&nbsp;" + nName + ", " + vName + end
                    + tab + "</b>LANR:<b>&nbsp;&nbsp;" + lanr + end + "</html>\n";
            if (lanrIstVergeben) {
                meldung = meldung + "<html><font color=#FF0000><b>Doppelte LANR sind nur in Ausnahmefällen sinnvoll</b></font>" + end
                        + tab + "(z.B. wenn ein Arzt an mehreren Betriebsstätten arbeitet)" + end
                    + "Sollen die Daten des vorhandenen Arztes jetzt geöffnet werden?</html>";
                
                Object[] options = { "Ja", "Nein, neuen Eintrag anlegen" };
                
                answer = JOptionPane.showOptionDialog(null, meldung,
                        "Vorsicht, doppelte LANR!", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options,
                        options[0]);
                if (answer != JOptionPane.NO_OPTION) {
                    return true;
                }

            } else {
                meldung = meldung + "Sollen die Daten dieses Arztes geöffnet werden?";
                answer = JOptionPane.showConfirmDialog(null, meldung, "Achtung wichtige Benuterzanfrage",
                        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                if (answer != JOptionPane.NO_OPTION) {
                    return true;
                }
            }
            answer = JOptionPane.showConfirmDialog(null, "Sie wollen wirklich einen neuen Eintrag mit diesen Daten anlegen?", "Sicherheitsabfrage",
                    JOptionPane.YES_NO_OPTION);
            if (answer != JOptionPane.YES_OPTION) {
                return true;
            }
        }
        return false;
    }
    
    public String getArztId () {
        return arztId;
    }
}