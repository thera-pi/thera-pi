package arztFenster;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyVetoException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.jdesktop.swingx.JXDialog;
import org.jdesktop.swingx.JXPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import CommonTools.DatFunk;
import CommonTools.ExUndHop;
import CommonTools.JCompTools;
import CommonTools.JRtaComboBox;
import CommonTools.JRtaTextField;
import CommonTools.SqlInfo;
import events.PatStammEvent;
import events.PatStammEventClass;
import hauptFenster.AktiveFenster;
import hauptFenster.Reha;
import systemEinstellungen.SystemConfig;
import rehaInternalFrame.JArztInternal;

public class ArztNeuanlage extends JXPanel implements ActionListener, KeyListener, FocusListener {
    /**
     * 
     */
    private static final long serialVersionUID = -2705163104113657236L;

    public JRtaTextField tfs[] = { null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null };

    private static final int IDX_TF_ANREDE = 0;
    private static final int IDX_TF_TITEL = 1;
    private static final int IDX_TF_NACHNAME = 2;
    private static final int IDX_TF_VORNAME = 3;
    private static final int IDX_TF_STRASSE = 4;
    private static final int IDX_TF_PLZ = 5;
    private static final int IDX_TF_ORT = 6;
    private static final int IDX_TF_LANR = 7;
    private static final int IDX_TF_BSNR = 8;
    private static final int IDX_TF_TEL = 9;
    private static final int IDX_TF_FAX = 10;
    private static final int IDX_TF_EMAIL1 = 11;
    private static final int IDX_TF_KLINIK = 12;
    private static final int IDX_TF_FACHARZT = 13;
    private static final int IDX_TF_ID = 14;

    public int[] felderpos = { 0, 1, 2, 3, 4, 5, 6, 11, 17, 8, 9, 14, 12 };
    public JButton speichern;
    public JButton abbrechen;
    public ArztNeuDlg eltern;
    public JScrollPane jscr;
    public JRtaComboBox arztgruppe;
    public JTextArea jta;
    private ArztPanel apan;
    Vector<String> arztDaten = null;
    String arztId = "";
    ImageIcon hgicon;
    int icx, icy;
    AlphaComposite xac1 = null;
    AlphaComposite xac2 = null;
    boolean neuAnlage;
    private Logger logger = LoggerFactory.getLogger(ArztNeuanlage.class);


    public ArztNeuanlage(ArztNeuDlg eltern, ArztPanel apanel, Vector<String> vec, String id) {
        super();
        setBackgroundPainter(Reha.instance.compoundPainter.get("ArztNeuanlage"));
        this.arztDaten = vec;
        this.arztId = id;
        this.eltern = eltern;
        this.apan = apanel;
        if (id.equals("")) {
            this.neuAnlage = true;
        } else {
            this.neuAnlage = false;
        }

        hgicon = Reha.rehaBackImg;
        icx = hgicon.getIconWidth() / 2;
        icy = hgicon.getIconHeight() / 2;
        xac1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.07f);
        xac2 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f);
        this.setDoubleBuffered(true);

        this.setLayout(new BorderLayout());

        add(getFelderPanel(), BorderLayout.CENTER);
        add(getButtonPanel(), BorderLayout.SOUTH);
        if (!this.neuAnlage) {
            new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    try {
                        fuelleFelder();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    return null;
                }

            }.execute();
        }
        validate();
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        if (hgicon != null) {
            g2d.setComposite(this.xac1);
            g2d.drawImage(hgicon.getImage(), (getWidth() / 2) - icx, (getHeight() / 2) - icy, null);
            g2d.setComposite(this.xac2);
        }
    }

    public JScrollPane getFelderPanel() {
        FormLayout lay = new FormLayout(
                //  1                 2     3     4                   5     6
                "right:max(60dlu;p), 4dlu, 60dlu,right:max(60dlu;p), 4dlu, 60dlu",
                // 1  2  3   4  5   6  7   8  9  10  11 12 13  14 15  16  17   18  19  20 21  22 23   24  25   26  27  28 29  30 31   32   33   34
                "3dlu,p,2dlu,p,2dlu,p,2dlu,p,2dlu,p,5dlu,p,5dlu,p,2dlu,p, 5dlu, p, 5dlu,p,2dlu,p,2dlu, p, 5dlu, p, 5dlu,p,2dlu,p,2dlu,0dlu,2dlu,5dlu");
        PanelBuilder jpan = new PanelBuilder(lay);
        jpan.setDefaultDialogBorder();
        jpan.getPanel()
            .setOpaque(false);
        jpan.getPanel()
            .addKeyListener(this);
        CellConstraints cc = new CellConstraints();

        jpan.add(new JLabel("Anrede"), cc.xy(1, 2));
        tfs[IDX_TF_ANREDE] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_ANREDE].addKeyListener(this);
        tfs[IDX_TF_ANREDE].addFocusListener(this);
        tfs[IDX_TF_ANREDE].setName("anrede");
        jpan.add(tfs[IDX_TF_ANREDE], cc.xy(3, 2));

        jpan.add(new JLabel("Titel"), cc.xy(4, 2));
        tfs[IDX_TF_TITEL] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_TITEL].addKeyListener(this);
        tfs[IDX_TF_TITEL].addFocusListener(this);
        tfs[IDX_TF_TITEL].setName("titel");
        jpan.add(tfs[IDX_TF_TITEL], cc.xy(6, 2));

        jpan.add(new JLabel("Nachname"), cc.xy(1, 4));
        tfs[IDX_TF_NACHNAME] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_NACHNAME].addKeyListener(this);
        tfs[IDX_TF_NACHNAME].addFocusListener(this);
        tfs[IDX_TF_NACHNAME].setName("nachname");
        jpan.add(tfs[IDX_TF_NACHNAME], cc.xyw(3, 4, 4));

        jpan.add(new JLabel("Vorname"), cc.xy(1, 6));
        tfs[IDX_TF_VORNAME] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_VORNAME].addKeyListener(this);
        tfs[IDX_TF_VORNAME].addFocusListener(this);
        tfs[IDX_TF_VORNAME].setName("vorname");
        jpan.add(tfs[IDX_TF_VORNAME], cc.xyw(3, 6, 4));

        jpan.add(new JLabel("Strasse"), cc.xy(1, 8));
        tfs[IDX_TF_STRASSE] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_STRASSE].addKeyListener(this);
        tfs[IDX_TF_STRASSE].addFocusListener(this);
        tfs[IDX_TF_STRASSE].setName("strasse");
        jpan.add(tfs[IDX_TF_STRASSE], cc.xyw(3, 8, 4));

        jpan.add(new JLabel("Plz/Ort"), cc.xy(1, 10));
        tfs[IDX_TF_PLZ] = new JRtaTextField("ZAHLEN", true);
        tfs[IDX_TF_PLZ].addKeyListener(this);
        tfs[IDX_TF_PLZ].addFocusListener(this);
        tfs[IDX_TF_PLZ].setName("plz");
        jpan.add(tfs[IDX_TF_PLZ], cc.xy(3, 10));

        tfs[IDX_TF_ORT] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_ORT].addKeyListener(this);
        tfs[IDX_TF_ORT].addFocusListener(this);
        tfs[IDX_TF_ORT].setName("ort");
        jpan.add(tfs[IDX_TF_ORT], cc.xyw(4, 10, 3));

        jpan.addSeparator("Arztidentifikation", cc.xyw(1, 12, 6));

        jpan.add(new JLabel("LANR"), cc.xy(1, 14));
        tfs[IDX_TF_LANR] = new JRtaTextField("ZAHLEN", true);
        tfs[IDX_TF_LANR].addKeyListener(this);
        tfs[IDX_TF_LANR].addFocusListener(this);
        tfs[IDX_TF_LANR].setName("arztnum");
        jpan.add(tfs[IDX_TF_LANR], cc.xyw(3, 14, 4));

        jpan.add(new JLabel("Betriebsstätte"), cc.xy(1, 16));
        tfs[IDX_TF_BSNR] = new JRtaTextField("ZAHLEN", true);
        tfs[IDX_TF_BSNR].addKeyListener(this);
        tfs[IDX_TF_BSNR].addFocusListener(this);
        tfs[IDX_TF_BSNR].setName("bsnr");
        jpan.add(tfs[IDX_TF_BSNR], cc.xyw(3, 16, 4));

        jpan.addSeparator("Kontakt", cc.xyw(1, 18, 6));

        jpan.add(new JLabel("Telefon"), cc.xy(1, 20));
        tfs[IDX_TF_TEL] = new JRtaTextField("NORMAL", true);
        tfs[IDX_TF_TEL].addKeyListener(this);
        tfs[IDX_TF_TEL].addFocusListener(this);
        tfs[IDX_TF_TEL].setName("telefon");
        jpan.add(tfs[IDX_TF_TEL], cc.xyw(3, 20, 4));

        jpan.add(new JLabel("Telefax"), cc.xy(1, 22));
        tfs[IDX_TF_FAX] = new JRtaTextField("NORMAL", true);
        tfs[IDX_TF_FAX].addKeyListener(this);
        tfs[IDX_TF_FAX].addFocusListener(this);
        tfs[IDX_TF_FAX].setName("fax");
        jpan.add(tfs[IDX_TF_FAX], cc.xyw(3, 22, 4));

        jpan.add(new JLabel("Email"), cc.xy(1, 24));
        tfs[IDX_TF_EMAIL1] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_EMAIL1].addKeyListener(this);
        tfs[IDX_TF_EMAIL1].addFocusListener(this);
        tfs[IDX_TF_EMAIL1].setName("email1");
        jpan.add(tfs[IDX_TF_EMAIL1], cc.xyw(3, 24, 4));

        jpan.addSeparator("Zusätze", cc.xyw(1, 26, 6));

        jpan.add(new JLabel("Facharzt"), cc.xy(1, 28));
        arztgruppe = new JRtaComboBox(SystemConfig.arztGruppen);
        arztgruppe.addFocusListener(this);
        jpan.add(arztgruppe, cc.xyw(3, 28, 4));

        jpan.add(new JLabel("Klinik"), cc.xy(1, 30));
        tfs[IDX_TF_KLINIK] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_KLINIK].addKeyListener(this);
        tfs[IDX_TF_KLINIK].addFocusListener(this);
        tfs[IDX_TF_KLINIK].setName("klinik");
        jpan.add(tfs[IDX_TF_KLINIK], cc.xyw(3, 30, 4));

        tfs[IDX_TF_FACHARZT] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_FACHARZT].setName("facharzt");
        tfs[IDX_TF_ID] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_ID].setName("id");

        jpan.getPanel()
            .validate();
        jscr = JCompTools.getTransparentScrollPane(jpan.getPanel());
        jscr.getVerticalScrollBar()
            .setUnitIncrement(15);
        jscr.validate();
        return jscr;
    }

    /**************************************************/
    public JXPanel getButtonPanel() {
        JXPanel jpan = JCompTools.getEmptyJXPanel();
        jpan.addKeyListener(this);
        jpan.setOpaque(false);
        FormLayout lay = new FormLayout(
                // 1                2     3                 4     5
                "fill:0:grow(0.33),50dlu,fill:0:grow(0.33),50dlu,fill:0:grow(0.33)",
                // 1  2  3
                "5dlu,p,5dlu");
        CellConstraints cc = new CellConstraints();
        jpan.setLayout(lay);
        speichern = new JButton("speichern");
        speichern.setActionCommand("speichern");
        speichern.addActionListener(this);
        speichern.addKeyListener(this);
        speichern.setMnemonic(KeyEvent.VK_S);
        jpan.add(speichern, cc.xy(2, 2));

        abbrechen = new JButton("abbrechen");
        abbrechen.setActionCommand("abbrechen");
        abbrechen.addActionListener(this);
        abbrechen.addKeyListener(this);
        abbrechen.setMnemonic(KeyEvent.VK_A);
        jpan.add(abbrechen, cc.xy(4, 2));

        return jpan;
    }

    public JXPanel getNeuKurzPanel() {
        return this;
    }

    public void setzeFocus() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                tfs[IDX_TF_ANREDE].requestFocus();
            }
        });
    }

    private void drecksFocus() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                setzeFocus();
            }
        });
    }

    private void toClipboard() {
        try {
            StringBuffer buf = new StringBuffer();
            buf.append("@Thera-Pi-Arztdaten@\n");
            for (int i = 0; i < tfs.length; i++) {
                if (tfs[i] != null) {
                    buf.append("TF-" + tfs[i].getName() + "=" + tfs[i].getText() + "\n");
                }
            }
            buf.append("CO-facharzt" + "=" + arztgruppe.getSelectedItem()
                                                       .toString()
                    + "\n");
            Toolkit.getDefaultToolkit()
                   .getSystemClipboard()
                   .setContents(new StringSelection(buf.toString()), null);
            JOptionPane.showMessageDialog(this, "Arztdaten erfolgreich in die Zwischenablage übertragen");
            drecksFocus();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "Fehler beim Übertrag der Arztdaten in die Zwischenablage");
            drecksFocus();
        }
    }

    private void fromClipboard() {
        String dummy1 = null;
        String dummy2 = null;
        String dummy3 = null;
        String dummy4 = null;
        int i = -1;
        int i1 = -1;

        try {
            int frage = JOptionPane.showConfirmDialog(this,
                    "Soll der Inhalt der Zwischenablage auf Arztdaten untersucht werden\nund ggfls. die Daten übernommen werden?",
                    "Wichtige Benutheranfrage", JOptionPane.YES_NO_OPTION);
            String clipstring = null;
            if (frage != JOptionPane.YES_OPTION) {
                drecksFocus();
                return;
            }
            Transferable t = Toolkit.getDefaultToolkit()
                                    .getSystemClipboard()
                                    .getContents(null);
            if (t != null && t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                clipstring = (String) t.getTransferData(DataFlavor.stringFlavor);
                String[] clip = clipstring.split("\n");
                if (!clip[0].startsWith("@Thera-Pi-Arztdaten@")) {
                    JOptionPane.showMessageDialog(this, "Daten der Zwischenablage sind keine verwertbaren Arztdaten");
                    drecksFocus();
                    return;
                }
                for (i = 1; i < clip.length; i++) {
                    try {
                        dummy1 = clip[i].split("=")[0];
                        dummy2 = clip[i].split("=")[1];
                        dummy3 = dummy1.split("-")[0];
                        dummy4 = dummy1.split("-")[1];
                    } catch (Exception ex) {
                        dummy2 = "";
                        continue;
                    }
                    if (dummy3.equals("TF")) {
                        for (i1 = 0; i1 < tfs.length; i1++) {
                            if (tfs[i1] != null) {
                                if (tfs[i1].getName()
                                           .equals(dummy4)
                                        && (!tfs[i1].getName()
                                                    .equals("id"))) {
                                    tfs[i1].setText(dummy2.replace("\n", ""));
                                }
                            }
                        }
                    } else if (dummy3.equals("CO")) {
                        arztgruppe.setSelectedItem(dummy2.replace("\n", ""));
                    }
                }
                JOptionPane.showMessageDialog(this, "Inhalt der Zwischenablage wurde erfolgreich übertragen");
                drecksFocus();
                return;
            } else {
                JOptionPane.showMessageDialog(this, "Daten der Zwischenablage sind keine verwertbaren Arztdaten");
                drecksFocus();
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            // System.out.println("i = "+i);
            // System.out.println("i1 = "+i1);
            JOptionPane.showMessageDialog(this, "Fehler beim Übertrag der Zwischenablage");
            drecksFocus();
        }
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {

        String comm = arg0.getActionCommand();
        final String xcomm = comm;
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (xcomm.equals("speichern")) {
                    datenSpeichern();
                    tabelleAktualisieren();
                    fensterSchliessen();
                } else if (xcomm.equals("abbrechen")) {
                    fensterSchliessen();
                }
            }
        });
    }

    public void tabelleAktualisieren() {

        List<String> list = Arrays.asList(new String[] { tfs[IDX_TF_LANR].getText(), tfs[IDX_TF_NACHNAME].getText(), tfs[IDX_TF_VORNAME].getText(),
                tfs[IDX_TF_STRASSE].getText(), tfs[IDX_TF_ORT].getText(), tfs[IDX_TF_TEL].getText(), tfs[IDX_TF_FAX].getText(), tfs[IDX_TF_KLINIK].getText(),
                (arztgruppe.getSelectedItem() == null ? "" : (String) arztgruppe.getSelectedItem()), this.arztId });
        if (this.neuAnlage) {
            Vector<String> vec = new Vector<String>();
            for (int i = 0; i < list.size(); i++) {
                vec.add(list.get(i));
            }
            apan.atblm.addRow(vec);
            // System.out.println("Tabellenzeile eingefügt");
        } else {
            int row = apan.arzttbl.getSelectedRow();
            int model = apan.arzttbl.convertRowIndexToModel(row);

            for (int i = 0; i < 8; i++) {
                apan.atblm.setValueAt(list.get(i), model, i);
                // KassenPanel.thisClass.kassentbl.setValueAt(list.get(i), row, i);
            }
            // System.out.println("Tabellenzeile aktualisiert");
        }
        apan.arzttbl.revalidate();
        apan.arzttbl.repaint();
    }

    public void allesAufNull() {
        for (int i = 0; i < 15; i++) {
            tfs[i].setText("");
        }
    }

    public void datenSpeichern() {
        int anzahlf = felderpos.length;
        String dbid = this.arztId;
        StringBuffer stmt = new StringBuffer();
        stmt.append("update arzt set ");
        if (this.neuAnlage) {
            String lanr = tfs[IDX_TF_LANR].getText()
                                          .trim();
            CheckDocDouble chkDbl = null;
            if (!"".equals(lanr)) {
                System.out.println("LANR " + lanr);

                chkDbl = new CheckDocDouble(lanr);
            } else {
                String nName = tfs[IDX_TF_NACHNAME].getText()
                                                   .trim();
                String vName = tfs[IDX_TF_VORNAME].getText()
                                                  .trim();
                chkDbl = new CheckDocDouble(nName, vName);
            }

            if (chkDbl.updateDouble()) {
                logger.info("schreibeInDb: Arzt-Doublette gefunden - wechsle zum Original");
                final String xarztid = chkDbl.getArztId();

                new SwingWorker<Void, Void>() {
                    @Override
                    // auf exist. Arzt umstellen
                    protected Void doInBackground() throws Exception {
                        JComponent arzt = AktiveFenster.getFensterAlle("ArztVerwaltung");
                        if (arzt != null) {
                            Reha.containerHandling(((JArztInternal) arzt).getDesktop());
                            ((JArztInternal) arzt).aktiviereDiesenFrame(((JArztInternal) arzt).getName());
                            ((JArztInternal) arzt).starteArztID(xarztid);
                            if (((JArztInternal) arzt).isIcon()) {
                                try {
                                    ((JArztInternal) arzt).setIcon(false);
                                } catch (PropertyVetoException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        return null;
                    }

                }.execute();
                // TODO: re-init form with existing data (?)
                fensterSchliessen();
                return;
            }
            // *********************
            int iid = SqlInfo.holeId("arzt", "NACHNAME");
            if (iid == -1) {
                JOptionPane.showMessageDialog(null,
                        "Fehler beim Anlegen einer neuen Arzt-ID, bitte erneut versuchen -> speichern");
                return;
            }
            dbid = Integer.toString(iid);
            this.arztId = dbid;
        }
        for (int i = 0; i < anzahlf; i++) {
            stmt.append(tfs[i].getName() + "='" + tfs[i].getText()
                                                        .trim()
                    + "' ,");
        }
        stmt.append("facharzt ='" + (arztgruppe.getSelectedItem() == null ? "" : (String) arztgruppe.getSelectedItem())
                + "'");
        stmt.append(" where id='" + dbid + "'");
        // System.out.println("Kommando = "+stmt);
        new ExUndHop().setzeStatement(stmt.toString());

    }

    public void fensterSchliessen() {
        ((JDialog) this.eltern).dispose();
    }

    private void fuelleFelder() {
        List<String> nichtlesen = Arrays.asList(new String[] {});
        Vector<String> felder = SqlInfo.holeSatz("arzt", "*", "id='" + this.arztId + "'", nichtlesen);
        int gros = felder.size();
        // System.out.println("Arztdaten von id"+this.arztId+" = "+felder);
        int anzahlf = felderpos.length;
        if (gros > 0) {
            for (int i = 0; i < anzahlf; i++) {
                tfs[i].setText(felder.get(felderpos[i]));
            }
            arztgruppe.setSelectedItem(felder.get(7));
        }

    }

    public void panelWechsel(boolean uebernahme) {
        if (uebernahme) {
            new SwingWorker<Void, Void>() {

                @Override
                protected Void doInBackground() throws Exception {
                    if (tfs[IDX_TF_NACHNAME].getText()
                                            .trim()
                                            .equals("")) {
                        JOptionPane.showMessageDialog(null,
                                "Also der Name des Arztes sollte wenigstens angegeben werden!");
                        return null;
                    }
                    int iid;
                    tfs[IDX_TF_FACHARZT].setText((String) arztgruppe.getSelectedItem());
                    tfs[IDX_TF_ID].setText(Integer.toString(iid = SqlInfo.holeId("arzt", "nachname")));
                    System.out.println("new Arzt Id: " + tfs[IDX_TF_ID].getText());
                    String stmt = "update arzt set ";
                    for (int i = 0; i < 14; i++) {
                        stmt = stmt + (i == 0 ? "" : ", ") + tfs[i].getName() + "='" + tfs[i].getText() + "'";
                    }
                    stmt = stmt + " where id ='" + Integer.toString(iid) + "'";
                    //// System.out.println(stmt);
                    new ExUndHop().setzeStatement(stmt);
                    // eltern.zurueckZurTabelle(tfs); **************wichtig
                    return null;
                }

            }.execute();
        } else {
            // eltern.zurueckZurTabelle(null);******wichtig
        }

    }

    @Override
    public void keyPressed(KeyEvent arg0) {
        if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
            arg0.consume();
            if (((JComponent) arg0.getSource()).getName()
                                               .equals("speichern")) {
                datenSpeichern();
                tabelleAktualisieren();
                fensterSchliessen();
            }
            if (((JComponent) arg0.getSource()).getName()
                                               .equals("abbrechen")) {
                fensterSchliessen();
            }
        }
        if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
            fensterSchliessen();
        }
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
        if (arg0.getKeyCode() == KeyEvent.VK_F3) {
            toClipboard();
        } else if (arg0.getKeyCode() == KeyEvent.VK_F2) {
            fromClipboard();
        }
    }

    @Override
    public void keyTyped(KeyEvent arg0) {

    }

    @Override
    public void focusGained(FocusEvent arg0) {

        Rectangle rec1 = ((JComponent) arg0.getSource()).getBounds();
        Rectangle rec2 = jscr.getViewport()
                             .getViewRect();
        JViewport vp = jscr.getViewport();
        vp.getVisibleRect();
        if ((rec1.y + ((JComponent) arg0.getSource()).getHeight()) > (rec2.y + rec2.height)) {
            vp.setViewPosition(new Point(0, (rec2.y + rec2.height) - rec1.height));
        }
        if (rec1.y < (rec2.y)) {
            vp.setViewPosition(new Point(0, rec1.y));
        }

    }

    @Override
    public void focusLost(FocusEvent arg0) {

    }

}
