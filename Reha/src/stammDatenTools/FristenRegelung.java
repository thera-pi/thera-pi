package stammDatenTools;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Vector;

import CommonTools.DatFunk;
import commonData.Rezeptvector;
import systemEinstellungen.SystemPreislisten;

public class FristenRegelung {    // TODO: handle Entlassungsmanagement (nicht in 'verordn', nicht in Eingabemaske)
    Rezeptvector rezept;
    String rezNum = null;
    LocalDate ablaufTag = null;
    String ablaufGrund = null;
    String okCheckTxt = null;
    int monateBisAblauf = 0;
    int wochenBisAblauf = 0;
    int tageBisAblauf = 0;
    
    private static final DateTimeFormatter inFormatter = DateTimeFormatter.ofPattern("d.M.yyyy");
    private static final DateTimeFormatter outFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");

    public FristenRegelung(Rezeptvector rezept) {
        super();
        this.rezept = rezept;
        rezNum = rezept.getRezNb();
        String alleTermine = rezept.getTermine();
        if (alleTermine.length() == 0) {
            return;
        }
        if (RezTools.isPhysio(rezNum) || RezTools.isMassage(rezNum)) {    // Rahmenvertr. v. 01.08.2021 gilt fuer Physio / Massage
            if (rezept.getAnzVorrBeh() > 6) {   // BVB- / LHM-VO
                monateBisAblauf = 6;
            } else {
                monateBisAblauf = 3;    // Std-VO
            }
            String erster = RezTools.holeErstenTermin(rezNum, alleTermine);
            LocalDate datumErster = LocalDate.parse(erster, inFormatter);
            ablaufTag = datumErster.plusMonths(monateBisAblauf);
            ablaufGrund = "VO ist ab erster Behandlung " + monateBisAblauf + " Monate gültig";
            okCheckTxt = monateBisAblauf + " Mon.";
        } else if (RezTools.isErgo(rezNum)) {   // keine Pruefung: VO laeuft ab, wenn Summe aller Unterbrechungen > 14 Tage groesser 70 Tage wird
            LocalDate datumHeute = LocalDate.parse(DatFunk.sHeute(), inFormatter);
            ablaufTag = datumHeute.plusMonths(1);
            okCheckTxt = "(kein Check)";
        } else if (RezTools.isPodo(rezNum)) {    // entspr. Rahmenvertr. v. 01.01.2021 fuer Podo
            int pgrp = rezept.getPreisGrpIdx();
            String disziplin = RezTools.getDisziplinFromRezNr(rezNum);
            tageBisAblauf = (Integer) ((Vector<?>) SystemPreislisten.hmFristen.get(disziplin).get(2)).get(pgrp);    // SysInit->Fristen

            String vorgaenger = RezTools.holeLetztenTermin(rezNum, alleTermine);
            if (vorgaenger.equals(DatFunk.sHeute())) {  // Aufruf ueber (+)-Button in org.therapi.reha.patient.AktuelleRezepte.actionPerformed()
                Vector<String> behTage = RezTools.holeEinzelTermineAusRezept(rezNum, alleTermine);
                int targetIdx = behTage.size() - 2;
                if (targetIdx >= 0) {
                    vorgaenger = behTage.get(targetIdx);
                }
            }
            LocalDate datumVorgaenger = LocalDate.parse(vorgaenger, inFormatter);
            ablaufTag = datumVorgaenger.plusDays(tageBisAblauf); 
            wochenBisAblauf = (int)(tageBisAblauf / 7);
            ablaufGrund = "Behandlung > " + tageBisAblauf + " Tage unterbrochen";
            okCheckTxt = tageBisAblauf + " Tg.";
        } else if (RezTools.isLogo(rezNum)) {    // entspr. Rahmenvertr. v. 16.03.2021 fuer Logo
            int monateBisAblauf = 7;     // bis 10 Therapieeinheiten 
            if (rezept.getAnzVorrBeh() > 10) {
                monateBisAblauf = 9;
            }
            LocalDate rezDatum = LocalDate.parse(rezept.getRezeptDatum());
            ablaufTag = rezDatum.plusMonths(monateBisAblauf); 
            ablaufGrund = "VO ist ab Ausstellungsdatum " + monateBisAblauf + " Monate gültig";
            okCheckTxt = monateBisAblauf + " Mon.";
        }
    }
    
    public String getVerfallDatum() {
        return ablaufTag.format(outFormatter);
    }
    public String getLastChance() {
        return ablaufTag.minusDays(1).format(outFormatter);
    }
    public String getVerfallGrund() {
        return ablaufGrund;
    }
    public String getCheckTxt() {
        return okCheckTxt;
    }
    public boolean dateIsValid (String currDate) {
        LocalDate behDatum = LocalDate.parse(currDate, inFormatter);
        if (ablaufTag != null) {
            return (behDatum.isBefore(ablaufTag));
        }
        return false;
    }
}