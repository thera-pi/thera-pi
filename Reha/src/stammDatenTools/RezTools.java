package stammDatenTools;

import java.awt.Point;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.UIManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.therapi.hmrCheck.HMRCheck2020;
import org.therapi.hmrCheck.Nagelspangenbehandlung;

import CommonTools.DatFunk;
import CommonTools.SqlInfo;
import CommonTools.StringTools;
import abrechnung.Disziplinen;
import commonData.HmPosList;
import commonData.Rezeptvector;
import core.Disziplin;
import environment.Path;
import hauptFenster.Reha;
import rezept.RezeptDto;
import systemEinstellungen.SystemConfig;
import systemEinstellungen.SystemPreislisten;
import terminKalender.BestaetigungsDaten;
import terminKalender.TerminBestaetigenAuswahlFenster;
import terminKalender.TermineErfassen;
import wecker.AePlayWave;

public class RezTools {
    public static final int REZEPT_IST_JETZ_VOLL = 0;
    public static final int REZEPT_IST_BEREITS_VOLL = 1;
    private static final int REZEPT_HAT_LUFT = 2;
    public static final int REZEPT_FEHLER = 3;
    public static final int REZEPT_ABBRUCH = 4;
    public static final int DIALOG_ABBRUCH = -1;
    public static final int DIALOG_OK = 0;
    public static int DIALOG_WERT;
    
    final static int WARNING_MESSAGE = 2;

    private static Logger logger = LoggerFactory.getLogger(RezTools.class);

    public static boolean mitJahresWechsel(String datum) {
        boolean ret = false;
        try {
            if (Integer.parseInt(datum.substring(6)) - Integer.parseInt(SystemConfig.aktJahr) < 0) {
                ret = true;
            }
        } catch (Exception ex) {
        }
        return ret;
    }

    private static Object[] sucheDoppel(int pos, List<String> list, String comperator) {
        // System.out.println("Position="+pos+" fistIndex="+list.indexOf(comperator)+"
        // lastIndex="+list.lastIndexOf(comperator));
        if (pos == list.indexOf(comperator)) {
            return new Object[] { true, list.indexOf(comperator), list.lastIndexOf(comperator) };
        } else {
            return new Object[] { true, list.lastIndexOf(comperator), list.indexOf(comperator) };
        }
    }

    private static Vector<ArrayList<?>> holePosUndAnzahlAusTerminen(String xreznr) {
        String disziKurz = getDisziplinFromRezNr(xreznr);
        Disziplinen diszis = new Disziplinen();
        diszis.setCurrDisziKurz(disziKurz);
        int disziIdx = diszis.getCurrDisziIdx();
        Vector<ArrayList<?>> xvec = new Vector<>();
        String icd10_1 = "";
        String icd10_2 = "";

        Vector<String> rezvec = SqlInfo.holeSatz("verordn",
                "termine,pos1,pos2,pos3,pos4,kuerzel1,kuerzel2,kuerzel3,kuerzel4,preisgruppe,indikatschl",
                "rez_nr='" + xreznr + "'", Arrays.asList(new String[] {}));
        Vector<String> termvec = holeEinzelZiffernAusRezept(null, rezvec.get(0));

        List<String> tmpList = Arrays.asList(rezvec.get(1), rezvec.get(2), rezvec.get(3), rezvec.get(4));
        if (isPodo(xreznr) ) {
            // TODO
            Vector<Vector<String>> vec = SqlInfo.holeFelder(
                    "select ICD10,ICD10_2 from verordn where rez_nr = '" + xreznr + "' LIMIT 1");
            icd10_1 = vec.get(0).get(0);
            icd10_2 = vec.get(0).get(1);
            if ("L60.0".equals(icd10_1) || "L60.0".equals(icd10_2)) {
                // Anpassung für > 4 HM-Pos notwendig
                // Ergänzung von 'immer da'-Positionen mit fixer Anzahl (Anpassung, Fertigung, ...) auf Kosten der Anz. des eindeutigen vorr. HM
                Vector<String> positionenVorr = null;
                
//                Nagelspangenbehandlung l60pos = new Nagelspangenbehandlung(positionenVorr, anzahl);
//                positionenVorr = l60pos.getAllVorr();
            }
        }
        List<String> list = new ArrayList<String>();
        for (int i = tmpList.size() - 1; i >= 0; i--) {
            String sTmp = tmpList.get(i);
            if (!sTmp.equals("")) { // Liste umsortieren auf 'alten Stand' (belegte Felder vorn)
                list.add(0, sTmp);
            } else {
                list.add(sTmp);                
            }
        }

        ArrayList<String> positionen = new ArrayList<>();
        String behandlungen = null;
        String[] einzelbehandlung = null;
        ArrayList<Integer> anzahl = new ArrayList<>();
        ArrayList<Boolean> vorrangig = new ArrayList<>();
        ArrayList<Boolean> einzelerlaubt = new ArrayList<>();
        ArrayList<Object[]> doppelpos = new ArrayList<>();
        boolean[] bvorrangig = null;
        ArrayList<String> tmpArr = new ArrayList();
        if (!(isRSport(xreznr) || isFTrain(xreznr))) {
            String kap = Reha.hmrXML.getKapitelFromHMap(disziKurz);
            String diagnosegruppe = rezvec.get(10);
            tmpArr = Reha.hmrXML.getErlaubteErgaenzendeHM(kap, diagnosegruppe);
        }
        HmPosList listeErgaenzendeHM = new HmPosList(tmpArr);
        String prefix = diszis.getPrefix(disziIdx);
        listeErgaenzendeHM.setPrefix(prefix);
        String aktHmPos = "";
        int idxPos = 0;
        for (int i = 1; i < 5; i++) {
            aktHmPos = rezvec.get(i)
                             .trim();
            if (! "".equals(aktHmPos)) {
                positionen.add(String.valueOf(aktHmPos));
                bvorrangig = isVorrangigAndExtra(rezvec.get(i + 4), xreznr.substring(0, 2));
                if ((i > 1) && listeErgaenzendeHM.contains(aktHmPos)) { 
                    vorrangig.add(false);   // eigentl. vorr. HM hier als ergänzend verordnet (z.B. KMT bei AT) 
                } else {
                    vorrangig.add(Boolean.valueOf(bvorrangig[0]));
                }
                einzelerlaubt.add(Boolean.valueOf(bvorrangig[1]));
                anzahl.add(0);
                if (countOccurence(list, aktHmPos) > 1) {
                    doppelpos.add(sucheDoppel(idxPos, list, aktHmPos));
                } else {
                    Object[] obj = { false, idxPos, idxPos };
                    doppelpos.add(obj.clone());
                }
                idxPos++;
            }
        }
        if (isPodoL60(xreznr)) {
            // TODO 
            // - zusätzliche Positionen positionen.add(), anzahl.add()
            // - immer Auswahl
        }

        Vector<String> imtag = new Vector<>();
        Object[] tests = null;
        for (int i = 0; i < termvec.size(); i++) {
            // Über alle Behandlungstage hinweg
            try {
                behandlungen = termvec.get(i);
                if (!"".equals(behandlungen)) {
                    einzelbehandlung = behandlungen.split(",");
                    imtag.clear();
                    int i2;
                    for (i2 = 0; i2 < einzelbehandlung.length; i2++) {  // über Behandlungen des Tages
                        try {
                            // Jetzt testen ob Doppelbehandlung
                            tests = doppelpos.get(list.indexOf(einzelbehandlung[i2]));
                            if ((Boolean) tests[0]) {
                                // Ja Doppelbehandlung
                                imtag.add(String.valueOf(einzelbehandlung[i2]));
                                // Jetzt testen ob erste oder Zweite
                                if (imtag.indexOf(einzelbehandlung[i2]) == imtag.lastIndexOf(einzelbehandlung[i2])) {
                                    // Erstes mal
                                    anzahl.set((Integer) tests[1], anzahl.get((Integer) tests[1]) + 1);
                                } else {
                                    // Zweites mal
                                    anzahl.set((Integer) tests[2], anzahl.get((Integer) tests[2]) + 1);
                                }
                            } else {
                                // Nein keine Doppelbehandlung
                                anzahl.set((Integer) tests[1], anzahl.get((Integer) tests[1]) + 1);
                            }
                        } catch (Exception ex) {
                            try {
                                String disziplin = getDisziplinFromRezNr(xreznr);
                                String kuerzel = getKurzformFromPos(einzelbehandlung[i2], rezvec.get(9),
                                        SystemPreislisten.hmPreise.get(disziplin)
                                                                  .get(Integer.parseInt(rezvec.get(9)) - 1));
                                JOptionPane.showMessageDialog(null,
                                        "<html><font color='#ff0000' size=+2>Fehler in der Ermittlung der Behandlungspositionen!</font><br><br>"
                                                + "<b>Bitte kontrollieren sie die bereits gespeicherten Behandlungspositionen!!<br><br>"
                                                + "Der problematische Termin ist der <font color='#ff0000'>" + (i + 1)
                                                + ".Termin</font>,<br>bestätigte Behandlungsart ist <font color='#ff0000'>"
                                                + kuerzel + " (" + einzelbehandlung[i2] + ")<br>"
                                                + "<br>Diese Behandlungsart ist im Rezeptblatt nicht, oder nicht mehr verzeichnet</font><br><br>"
                                                + "<br>"
                                                + "<b><font color='#ff0000'>Lösung:</font> Klicken Sie die Termintabelle an, drücken Sie dann die rechte Maustaste und wählen Sie dann die Option<br><br>"
                                                + "<b><u>\"alle Behandlungsarten den Rezeptdaten angleichen\"</u></b><br>"
                                                + "</b>oder<br><b><u>\"alle im Rezept gespeicherten Behandlungsarten löschen\"</u></b><br></html>");
                                return xvec;
                            } catch (Exception ex2) {
                                JOptionPane.showMessageDialog(null,
                                        "<html><font color='#ff0000' size=+2>Fehler in der Ermittlung der Behandlungspositionen!</font><br><br>"
                                                + "<b>Bitte kontrollieren sie die bereits gespeicherten Behandlungspositionen!!<br><br>"
                                                + "Der Fehler kann nicht genau lokalisiert werden!<br><br>"
                                                + "Vermutlich wurden in den bisherigen Terminen Positionen bestätigt, die im Rezeptblatt<br>"
                                                + "<u>nicht oder nicht mehr aufgeführt sind.</u><br><br>"
                                                + "<b>Klicken Sie die Termintabelle an, drücken Sie dann die rechte Maustaste und wählen Sie eine Option aus.<b><br></html>");
                                return xvec;
                            }
                        }
                    }
                } else {
                    for (int i3 = 0; i3 < positionen.size(); i3++) {
                        anzahl.set(i3, anzahl.get(i3) + 1);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        xvec.add((ArrayList<?>) positionen.clone());
        xvec.add((ArrayList<?>) anzahl.clone());
        xvec.add((ArrayList<?>) vorrangig.clone());
        xvec.add((ArrayList<?>) einzelerlaubt.clone());
        xvec.add((ArrayList<?>) doppelpos.clone());
        return xvec;
    }

    private static int countOccurence(List<String> list, String comperator) {
        int ret = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i)
                    .trim()
                    .equals(comperator.trim())) {
                ret++;
            }
        }
        return ret;
    }

    public static boolean[] isVorrangigAndExtra(String kuerzel, String rezClass) {
        Disziplinen diszis = new Disziplinen();
        final int VORRANGIG = 0, EXTRAOK = 1;
        boolean[] bret = { false, false };
        try {
            Vector<String> vec = SqlInfo.holeFelder("select vorrangig,extraok from kuerzel where kuerzel='" + kuerzel
                    + "' and disziplin ='" + rezClass + "' LIMIT 1")
                                        .get(0);
            if (vec.isEmpty()) {
                String msg = "Achtung!\n\n" + "Ihre Kürzelzuordnung in den Preislisten ist nicht korrekt!!!!!\n"
                        + "Kürzel: " + kuerzel + "\n" + "Disziplin: " + diszis.getDisziKurzFromRK(rezClass) + "\n\n"
                        + "Für die ausgewählte Diziplin ist das angegebene Kürzel nicht in der Kürzeltabelle vermerkt!";
                JOptionPane.showMessageDialog(null, msg);
                return null;
            }
            bret[0] = "T".equals(vec.get(VORRANGIG));
            bret[1] = "T".equals(vec.get(EXTRAOK));
        } catch (Exception e) {
            logger.error("could not retrieve is vorrangig for " + kuerzel + " " + rezClass, e);
        }
        return bret;
    }

    public static Vector<String> holeEinzelZiffernAusRezept(String xreznr, String termine) {
        Vector<String> xvec = null;
        Vector<String> retvec = new Vector<>();
        String terms = null;
        if ("".equals(termine)) {
            xvec = SqlInfo.holeSatz("verordn", "termine,pat_intern", "rez_nr='" + xreznr + "'",
                    Arrays.asList(new String[] {}));
            if (xvec.isEmpty()) {
                return retvec;
            } else {
                terms = xvec.get(0);
            }
        } else {
            terms = termine;
        }
        if (terms == null || "".equals(terms)) {
            return retvec;
        }
        String[] tlines = terms.split("\n");
        int lines = tlines.length;

        for (int i = 0; i < lines; i++) {
            String[] terdat = tlines[i].split("@");
            retvec.add(terdat[3].trim());
        }
        return retvec;  // HM-Codes der Beh.-Termine
    }

    public static Object[] holeTermineAnzahlUndLetzter(String termine) {    // TODO: unbenutzt (?)
        Object[] retobj = { null, null };
        try {
            String[] tlines = termine.split("\n");
            int lines = tlines.length;
            if (lines <= 0) {
                retobj[0] = 0;
                retobj[1] = null;
                return retobj;
            }
            String[] terdat;
            terdat = tlines[lines - 1].split("@");
            retobj[0] = Integer.valueOf(lines);
            retobj[1] = String.valueOf(terdat[0]);
        } catch (Exception ex) {
        }
        return retobj;
    }

    public static String holeErstenTermin(String xreznr, String termine) {
        try {
            Vector<String> xvec = null;
            String terms = null;
            if ("".equals(termine)) {
                xvec = SqlInfo.holeSatz("verordn", "termine,pat_intern", "rez_nr='" + xreznr + "'",
                        Arrays.asList(new String[] {}));
                if (xvec.isEmpty()) {
                    return "";
                } else {
                    terms = xvec.get(0);
                }
            } else {
                terms = termine;
            }
            if (terms == null || "".equals(terms)) {
                return "";
            }
            String[] tlines = terms.split("\n");
            String[] terdat = tlines[0].split("@");
            return terdat[0];
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static String holeLetztenTermin(String xreznr, String termine) {
        try {
            Vector<String> xvec = null;
            String terms = null;
            if ("".equals(termine)) {
                xvec = SqlInfo.holeSatz("verordn", "termine,pat_intern", "rez_nr='" + xreznr + "'",
                        Arrays.asList(new String[] {}));
                if (xvec.isEmpty()) {
                    return "";
                } else {
                    terms = xvec.get(0);
                }
            } else {
                terms = termine;
            }
            if (terms == null || "".equals(terms)) {
                return "";
            }
            String[] tlines = terms.split("\n");
            int line = tlines.length - 1;
            String[] terdat = tlines[line].split("@");
            return terdat[0];
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static Vector<String> holeEinzelTermineAusRezept(String xreznr, String termine) {
        Vector<String> xvec = null;
        Vector<String> retvec = new Vector<>();
        String terms = null;
        if ("".equals(termine)) {
            xvec = SqlInfo.holeSatz("verordn", "termine,pat_intern", "rez_nr='" + xreznr + "'",
                    Arrays.asList(new String[] {}));
            if (xvec.isEmpty()) {
                return (Vector<String>) retvec.clone();
            } else {
                terms = xvec.get(0);
            }
        } else {
            terms = termine;
        }
        if (terms == null || "".equals(terms)) {
            return (Vector<String>) retvec.clone();
        }
        String[] tlines = terms.split("\n");
        int lines = tlines.length;
        String[] terdat = null;
        for (int i = 0; i < lines; i++) {
            terdat = tlines[i].split("@");
            retvec.add("".equals(terdat[0].trim()) ? "  .  .    " : String.valueOf(terdat[0]));
        }
        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                String strings1 = DatFunk.sDatInSQL(s1);
                String strings2 = DatFunk.sDatInSQL(s2);
                return strings1.compareTo(strings2);
            }
        };
        Collections.sort(retvec, comparator);
        return (Vector<String>) retvec.clone();
    }

    public static Vector<Vector<String>> holeTermineUndBehandlerAusRezept(String xreznr, String termine) {
        Vector<String> xvec = null;
        Vector<String> retvec = new Vector<>();
        Vector<Vector<String>> retbeides = new Vector<>();
        String terms = null;
        if ("".equals(termine)) {
            xvec = SqlInfo.holeSatz("verordn", "termine,pat_intern", "rez_nr='" + xreznr + "'",
                    Arrays.asList(new String[] {}));
            if (xvec.isEmpty()) {
                return (Vector<Vector<String>>) retbeides.clone();
            } else {
                terms = xvec.get(0);
            }
        } else {
            terms = termine;
        }
        if (terms == null || "".equals(terms)) {
            return (Vector<Vector<String>>) retbeides.clone();
        }
        String[] tlines = terms.split("\n");
        int lines = tlines.length;
        String[] terdat = null;
        for (int i = 0; i < lines; i++) {
            retvec.clear();
            terdat = tlines[i].split("@");
            retvec.add("".equals(terdat[0].trim()) ? "  .  .    " : String.valueOf(terdat[0]));
            retvec.add("".equals(terdat[1].trim()) ? "k.A." : String.valueOf(terdat[1]));
            retbeides.add((Vector) retvec.clone());
        }
        Comparator<Vector> comparator = new Comparator<Vector>() {
            @Override
            public int compare(Vector o1, Vector o2) {
                String s1 = DatFunk.sDatInSQL((String) o1.get(0));
                String s2 = DatFunk.sDatInSQL((String) o2.get(0));
                return s1.compareTo(s2);
            }
        };
        Collections.sort(retbeides, comparator);

        return (Vector<Vector<String>>) retbeides.clone();
    }

    private static String holePosAusIdUndRezNr(String id, String reznr) {
        String diszi = getDisziplinFromRezNr(reznr);
        String preisgruppe = SqlInfo.holeEinzelFeld(
                "select preisgruppe from verordn where rez_nr='" + reznr + "' LIMIT 1");

        Vector<Vector<Vector<String>>> vector = null;
        try {
            vector = SystemPreislisten.hmPreise.get(diszi);
        } catch (ArrayIndexOutOfBoundsException e) {
            logger.error("Keine SystemPreislisten eingelesen? Error: " + e.getLocalizedMessage());
        }

        if (vector == null) {
            System.out.println("baeh"); // Boo! If inner Vector was NULL, it can still be wrapped & outer Vec != NULL
        }
        Vector<Vector<String>> preisvec = vector.get(Integer.parseInt(preisgruppe) - 1);
        String pos = getPosFromID(id, preisgruppe, preisvec);
        return pos == null ? "" : pos;
    }

    public static Vector<Vector<String>> macheTerminVector(String termine) {
        String[] tlines = termine.split("\n");
        int lines = tlines.length;
        //// System.out.println("Anzahl Termine = "+lines);
        Vector<Vector<String>> tagevec = new Vector<>();
        Vector<String> tvec = new Vector<>();
        String[] terdat = null;
        for (int i = 0; i < lines; i++) {
            terdat = tlines[i].split("@");
            int ieinzel = terdat.length;
            //// System.out.println("Anzahl Splits = "+ieinzel);
            tvec.clear();
            for (int y = 0; y < ieinzel; y++) {
                if (y == 0) {
                    tvec.add(String.valueOf("".equals(terdat[y].trim()) ? "  .  .    " : terdat[y]));
                    if (i == 0) {
                        SystemConfig.hmAdrRDaten.put("<Rerstdat>",
                                String.valueOf("".equals(terdat[y].trim()) ? "  .  .    " : terdat[y]));
                    }
                } else {
                    tvec.add(String.valueOf(terdat[y]));
                }
                //// System.out.println("Feld "+y+" = "+terdat[y]);
            }
            //// System.out.println("Termivector = "+tvec);
            tagevec.add((Vector<String>) tvec.clone());
        }
        if (!tagevec.isEmpty()) {
            Comparator<Vector<String>> comparator = new Comparator<Vector<String>>() {
                @Override
                public int compare(Vector<String> o1, Vector<String> o2) {
                    String s1 = o1.get(4);
                    String s2 = o2.get(4);
                    return s1.compareTo(s2);
                }
            };
            Collections.sort(tagevec, comparator);
        }
        return (Vector<Vector<String>>) tagevec.clone();
    }

    public static boolean zweiPositionenBeiHB(String disziplin, String preisgruppe) {
        int pg = Integer.parseInt(preisgruppe) - 1;
        return !"".equals(SystemPreislisten.hmHBRegeln.get(disziplin)
                                                      .get(pg)
                                                      .get(2)
                                                      .trim())
                || !"".equals(SystemPreislisten.hmHBRegeln.get(disziplin)
                                                          .get(pg)
                                                          .get(3)
                                                          .trim());
    }

    private static boolean keineWeggebuehrBeiHB(String disziplin, String preisgruppe) {
        int pg = Integer.parseInt(preisgruppe) - 1;
        return "".equals(SystemPreislisten.hmHBRegeln.get(disziplin)
                                                     .get(pg)
                                                     .get(2)
                                                     .trim())
                && "".equals(SystemPreislisten.hmHBRegeln.get(disziplin)
                                                         .get(pg)
                                                         .get(3)
                                                         .trim());
    }

    public static String getLangtextFromID(String id, String preisgruppe, Vector<Vector<String>> vec) {
        String ret = "kein Langtext vorhanden";
        int lang = vec.size();
        if (lang == 0 || vec.get(0) == null) {
            return ret;
        }
        int idpos = vec.get(0)
                       .size()
                - 1;
        for (int i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(idpos)
                   .equals(id)) {
                ret = vec.get(i)
                         .get(0);
                break;
            }
        }
        return ret;
    }

    public static String getPreisAktFromID(String id, String preisgruppe, Vector<Vector<String>> vec) {
        int lang = vec.size(), i;
        int idpos = vec.get(0)
                       .size()
                - 1;
        String ret = "0.00";
        for (i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(idpos)
                   .equals(id)) {
                ret = vec.get(i)
                         .get(3);
                break;
            }
        }
        return ret;
    }

    public static String getPreisAltFromID(String id, String preisgruppe, Vector<Vector<String>> vec) {
        int lang = vec.size(), i;
        int idpos = vec.get(0)
                       .size()
                - 1;
        String ret = "0.00";
        for (i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(idpos)
                   .equals(id)) {
                ret = vec.get(i)
                         .get(4);
                break;
            }
        }
        return ret;
    }

    public static String getPreisAktFromPos(String pos, String preisgruppe, Vector<Vector<String>> vec) {
        try {
            int lang = vec.size(), i;
            String ret = "0.00";
            for (i = 0; i < lang; i++) {
                if (vec.get(i)
                       .get(2)
                       .equals(pos)) {
                    ret = vec.get(i)
                             .get(3);
                    break;
                }
            }
            return ret;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "0.00";
        }
    }

    public static String getPreisAltFromPos(String pos, String preisgruppe, Vector<Vector<String>> vec) {
        int lang = vec.size(), i;
        String ret = "0.00";
        for (i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(1)
                   .equals(pos)) {
                ret = vec.get(i)
                         .get(4);
                break;
            }
        }
        return ret;
    }

    public static String getPreisAltFromPosNeu(String pos, String preisgruppe, Vector<Vector<String>> vec) {
        int lang = vec.size(), i;
        String ret = "0.00";
        for (i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(2)
                   .equals(pos)) {
                ret = vec.get(i)
                         .get(4);
                break;
            }
        }
        return ret;
    }

    public static String getIDFromPos(String pos, String preisgruppe, Vector<Vector<String>> vec) {
        int lang = vec.size(), i;
        int idpos = vec.get(0)
                       .size()
                - 1;
        String ret = "-1";
        for (i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(2)
                   .equals(pos)) {
                ret = vec.get(i)
                         .get(idpos);
                break;
            }
        }
        return ret;
    }

    public static String getIDFromPosX(String pos, String preisgruppe, String disziplin) {
        Vector<Vector<String>> vec = holePreisVector(disziplin, Integer.parseInt(preisgruppe) - 1);
        int lang = vec.size(), i;
        int idpos = vec.get(0)
                       .size()
                - 1;
        String ret = "-1";
        for (i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(2)
                   .equals(pos)) {
                ret = vec.get(i)
                         .get(idpos);
                break;
            }
        }
        return ret;
    }

    public static String getPosFromID(String id, String preisgruppe, Vector<Vector<String>> vec) {
        int lang = vec.size(), i;
        int idpos = vec.get(0)
                       .size()
                - 1;
        String ret = "";
        for (i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(idpos)
                   .equals(id)) {
                ret = vec.get(i)
                         .get(2);
                break;
            }
        }
        return ret;
    }

    public static String getKurzformFromID(String id, Vector<Vector<String>> vec) {
        int lang = vec.size(), i;
        int idpos = vec.get(0)
                       .size()
                - 1;
        String ret = "";
        for (i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(idpos)
                   .equals(id)) {
                ret = vec.get(i)
                         .get(1);
                break;
            }
        }
        return ret;
    }

    public static String getKurzformFromPos(String pos, String preisgruppe, Vector<Vector<String>> vec) {
        // Parameter preisgruppe wird nicht ausgewertet
        int lang = vec.size(), i;
        String ret = "";
        try {
            for (i = 0; i < lang; i++) {
                if (vec.get(i)
                       .get(2)
                       .trim()
                       .equals(pos.trim())
                        && !"Isokin".equals(vec.get(i)
                                               .get(1))) {
                    ret = vec.get(i)
                             .get(1);
                    break;
                }
            }
        } catch (Exception ex) {
            System.out.println("Parameter pos = " + pos);
            System.out.println("Parameter preisgruppe = " + preisgruppe);
            System.out.println("Parameter vec = " + vec);
            System.out.println("Parameter ret = " + ret);
            System.out.println("Nachfolgend die Excepiton von getKurzformFromPos");
            ex.printStackTrace();
        }
        return ret;
    }

    public static Object[] getKurzformUndIDFromPos(String pos, String preisgruppe, Vector<Vector<String>> vec) {
        int lang = vec.size(), i;
        int idpos = vec.get(0)
                       .size()
                - 1;
        Object[] retobj = { "", "" };
        for (i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(2)
                   .equals(pos)
                    && !"Isokin".equals(vec.get(i)
                                           .get(1))) {
                retobj[0] = vec.get(i)
                               .get(1);
                retobj[1] = vec.get(i)
                               .get(idpos);
                break;
            }
        }
        return retobj.clone();
    }

    public static String getIDFromKurzform(String kurzform, Vector<Vector<String>> vec) {
        int lang = vec.size(), i;
        int idpos = vec.get(0)
                       .size()
                - 1;
        String ret = "";
        for (i = 0; i < lang; i++) {
            if (vec.get(i)
                   .get(1)
                   .equals(kurzform)) {
                ret = vec.get(i)
                         .get(idpos);
                break;
            }
        }
        return ret;
    }

    public static Vector<Vector<String>> holePreisVector(String disziplin, int preisgruppe) {
        try {
            if (disziplin.startsWith("KG")) {
                return SystemPreislisten.hmPreise.get("Physio")
                                                 .get(preisgruppe);
            } else if (disziplin.startsWith("MA")) {
                return SystemPreislisten.hmPreise.get("Massage")
                                                 .get(preisgruppe);
            } else if (disziplin.startsWith("ER")) {
                return SystemPreislisten.hmPreise.get("Ergo")
                                                 .get(preisgruppe);
            } else if (disziplin.startsWith("LO")) {
                return SystemPreislisten.hmPreise.get("Logo")
                                                 .get(preisgruppe);
            } else if (disziplin.startsWith("RH")) {
                return SystemPreislisten.hmPreise.get("Reha")
                                                 .get(preisgruppe);
            } else if (disziplin.startsWith("PO")) {
                return SystemPreislisten.hmPreise.get("Podo")
                                                 .get(preisgruppe);
            } else if (disziplin.startsWith("RS")) {
                return SystemPreislisten.hmPreise.get("Rsport")
                                                 .get(preisgruppe);
            } else if (disziplin.startsWith("FT")) {
                return SystemPreislisten.hmPreise.get("Ftrain")
                                                 .get(preisgruppe);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null,
                    "Fehler im Preislistenbezug!\n\nVermutete Ursache:\nSie haben eine oder mehrere Tarifgruppen gelöscht!\nSelbst Schuld, sowas sollte man nicht machen");
        }
        return null;
    }

    public static boolean neuePreisNachRezeptdatumOderStichtag(String aktDisziplin, int tarifgruppe, String rez_datum,
            boolean neuanlage, Vector<String> rezvec) {
        try {
            String datum = SystemPreislisten.hmNeuePreiseAb.get(aktDisziplin)
                                                           .get(tarifgruppe);
            int regel = SystemPreislisten.hmNeuePreiseRegel.get(aktDisziplin)
                                                           .get(tarifgruppe);
            Vector<String> tage = null;
            // Regel 1=nach Behandlungsbeginn, 2=nach Rezeptdatum, 3=irgend eine Behandlung ab Datum 
            // der Rest wird nicht ausgewertet
            if (!"".equals(datum.trim()) && regel == 2) {
                return DatFunk.TageDifferenz(datum, rez_datum) >= 0; // true: Stichtag liegt vor RezDatum
            } else if (!"".equals(datum.trim()) && regel == 1) {
                // Neuanlage
                if (neuanlage) {
                    return DatFunk.TageDifferenz(datum, DatFunk.sHeute()) >= 0;
                }
                // Tage holen
                tage = holeEinzelTermineAusRezept(null, rezvec.get(34));
                if (tage.isEmpty()) {
                    // keine Tage vorhanden und Datum heute >= Regeldatum
                    return DatFunk.TageDifferenz(datum, DatFunk.sHeute()) >= 0;
                } else if (!tage.isEmpty()) {
                    // Tage vorhanden dann Datum testen
                    return DatFunk.TageDifferenz(datum, tage.get(0)) >= 0; // true: Stichtag liegt vor Beh. Beginn
                }
            } else if (!"".equals(datum.trim()) && regel == 3) {
                // Neuanlage
                if (neuanlage) {
                    return DatFunk.TageDifferenz(datum, DatFunk.sHeute()) >= 0;
                }
                // Tage holen
                tage = holeEinzelTermineAusRezept(null, rezvec.get(34));
                if (tage.isEmpty()) {
                    // keine Tage vorhanden und Datum heute >= Regeldatum
                    return DatFunk.TageDifferenz(datum, DatFunk.sHeute()) >= 0;
                } else if (!tage.isEmpty()) {
                    // Tage vorhanden dann Datum testen ob irgend ein Datum >= Stichtag
                    for (int i = 0; i < tage.size(); i++) {
                        if (DatFunk.TageDifferenz(datum, tage.get(i)) >= 0) {
                            return true;
                        }
                    }
                    return DatFunk.TageDifferenz(datum, DatFunk.sHeute()) >= 0;
                }
            } else { // kein Stichtag oder regel ist nicht (1, 2 oder 3)
                // Neuanlage
                // Bei Regel 4
                return "".equals(datum.trim()) || regel != 4 || DatFunk.TageDifferenz(datum, DatFunk.sHeute()) >= 0;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // Bei Regel 0 oder wenn alles nicht zutrifft
        return true;
    }

    public static int testeRezGebArt(boolean testefuerbarcode, boolean hintergrund, String srez, String termine) {
        int iret = 0;
        Vector<String> vAktTermine = null;
        boolean bTermine = false;
        boolean u18Test = false;
        boolean bMitJahresWechsel = false;
        ZuzahlModell zm = new ZuzahlModell();
        Rezeptvector currVO = new Rezeptvector(Reha.instance.patpanel.vecaktrez);
        String geburtstag = DatFunk.sDatInDeutsch(Reha.instance.patpanel.patDaten.get(4));
        boolean warImVorjahrBefreit = Reha.instance.patpanel.patDaten.get(69)
                                                                     .trim()
                                                                     .equals(SystemConfig.vorJahr);
        boolean istBefreit = "T".equals(Reha.instance.patpanel.patDaten.get(30));

        // 1. Schritt haben wir bereits Termineinträge die man auswerten kann
        if (!(vAktTermine = holeEinzelTermineAusRezept("", termine)).isEmpty()) {
            // Es gibt Termine in der Tabelle
            bTermine = true;
            if (vAktTermine.get(0)
                           .substring(6)
                           .equals(SystemConfig.vorJahr)) {
                bMitJahresWechsel = true;
            }
            if (DatFunk.Unter18(vAktTermine.get(0), geburtstag)) {
                u18Test = true;
            }
        }

        for (int i = 0; i < 1; i++) {
            if (currVO.getZzRegel() <= 0) {
                // Kasse erfordert keine Zuzahlung
                zm.allefrei = true;
                iret = 0;
                break;
            }
            if (Integer.parseInt(currVO.getZzStat()) == 1) {
                // Hat bereits bezahlt normal behandeln (zzstatus == 1)
                zm.allezuzahl = true;
                iret = 2;
            }

            /* Jetzt der Ober-Scheißdreck für den Achtzehner-Test */
            if (currVO.getUnter18() || u18Test) {
                // Es ist ein unter 18 Jahre Test notwendig
                if (bTermine) {
                    int[] test = ZuzahlTools.terminNachAchtzehn(vAktTermine, geburtstag);
                    if (test[0] > 0) {
                        // muß zuzahlen

                        zm.allefrei = false;
                        if (test[1] > 0) {
                            zm.allefrei = false;
                            zm.allezuzahl = false;
                            zm.anfangfrei = true;
                            zm.teil1 = test[1];
                            zm.teil2 = maxAnzahl() - test[1];
                            // System.out.println("Splitten frei für "+test[1]+" Tage, bezahlen für
                            // "+(maxAnzahl()-test[1]));
                            iret = 1;
                        } else {
                            zm.allezuzahl = true;
                            zm.teil1 = test[1];
                            // System.out.println("Jeden Termin bezahlen insgesamt bezahlen für
                            // "+(maxAnzahl()-test[1]));
                            iret = 2;
                        }
                    } else {
                        // Voll befreit
                        zm.allefrei = true;
                        iret = 0;
                    }
                } else {
                    // Es stehen keine Termine für Analyse zur Verfügung also muß das Fenster für
                    // manuelle Eingabe geöffnet werden!!
                    String stichtag = DatFunk.sHeute()
                                             .substring(0, 6)
                            + Integer.valueOf(Integer.valueOf(SystemConfig.aktJahr) - 18);
                    if (DatFunk.TageDifferenz(geburtstag, stichtag) >= 0) {
                        // System.out.println("Normale Zuzahlung....");
                        zm.allefrei = false;
                        zm.allezuzahl = true;
                        iret = 2;
                    } else {
                        // System.out.println("Alle Frei....");
                        zm.allefrei = true;
                        zm.allezuzahl = false;
                        iret = 0;
                    }
                }
                break;
            }

            /* Keine Befreiung Aktuell und keine Vorjahr (Normalfall) */
            if (!istBefreit && !warImVorjahrBefreit) {
                iret = 2;
                break;
            }
            /* Aktuell befreit und im Vorjahr auch befreit */
            if (istBefreit && warImVorjahrBefreit) {
                iret = 0;
                break;
            }
            /* aktuell nicht frei, Vorjahr frei */
            if (!istBefreit && warImVorjahrBefreit) {
                if (!bMitJahresWechsel) {// Alle Termine aktuell
                    iret = 2;
                } else {// es gibt Termine im Vorjahr
                    Object[] obj = JahresWechsel(vAktTermine, SystemConfig.vorJahr);
                    if (!(Boolean) obj[0]) {// alle Termine waren im Vorjahr
                        if (vAktTermine.size() < maxAnzahl()) {
                            String meldung = "<html>Während der Befreiung wurden <b>" + vAktTermine.size() + "  von "
                                    + maxAnzahl() + " Behandlungen</b> durchgeführt!<br>"
                                    + "Rezeptgebühren müssen also noch für <b>" + (maxAnzahl() - vAktTermine.size())
                                    + " Behandlungen</b> entrichtet werden.<br>"
                                    + "<br><br>Ist das korrekt?<br><br></html>";
                            int anfrage = JOptionPane.showConfirmDialog(null, meldung,
                                    "Achtung wichtige Benutzeranfrage", JOptionPane.YES_NO_CANCEL_OPTION);
                            if (anfrage == JOptionPane.YES_OPTION) {
                                zm.allefrei = false;
                                zm.allezuzahl = false;
                                zm.anfangfrei = true;
                                zm.teil1 = vAktTermine.size();
                                zm.teil2 = maxAnzahl() - vAktTermine.size();
                                iret = 1;
                            } else if (anfrage == JOptionPane.CANCEL_OPTION) {
                                return -1;
                            } else {
                                Object ret = JOptionPane.showInputDialog(null,
                                        "Geben Sie bitte die Anzahl Behandlungen ein für die\nRezeptgebühren berechnet werden sollen:",
                                        Integer.toString(maxAnzahl() - vAktTermine.size()));
                                if (ret == null) {
                                    // iret = 0;
                                    return -1;
                                } else {
                                    zm.allefrei = false;
                                    zm.allezuzahl = false;
                                    zm.anfangfrei = true;
                                    zm.teil1 = maxAnzahl() - Integer.parseInt((String) ret);
                                    zm.teil2 = Integer.parseInt((String) ret);
                                    iret = 1;
                                }
                            }
                        } else {
                            iret = 0;
                        }
                    } else {// gemischte Termine
                        zm.allefrei = false;
                        zm.allezuzahl = false;
                        zm.anfangfrei = true;
                        zm.teil1 = (Integer) obj[1];
                        zm.teil2 = (Integer) obj[2];
                        iret = 1;
                    }
                }
                break;
            }
            /* Aktuelle Befreiung aber nicht im Vorjahr */
            if (istBefreit && !warImVorjahrBefreit) {
                if (!bMitJahresWechsel) {// Alle Termine aktuell
                    iret = 0;
                } else {// es gibt Termine im Vorjahr
                    Object[] obj = JahresWechsel(vAktTermine, SystemConfig.vorJahr);
                    if (!(Boolean) obj[0]) {// alle Termine waren im Vorjahr
                        iret = 2;
                    } else {// gemischte Termine
                            // System.out.println("Termine aus dem Vorjahr(Zuzahlung) = "+obj[1]+" Termine
                            // aus diesem Jahr(frei) = "+obj[2]);
                        zm.allefrei = false;
                        zm.allezuzahl = false;
                        zm.anfangfrei = false;
                        zm.teil1 = (Integer) obj[1];
                        zm.teil2 = (Integer) obj[2];
                        iret = 3;
                    }
                }
                break;
            }
        }

        zm.hausbesuch = currVO.getHausbesuch();
        zm.hbvoll = currVO.getHbVoll();
        zm.hbheim = "T".equals(Reha.instance.patpanel.patDaten.get(44));
        zm.km = StringTools.ZahlTest(Reha.instance.patpanel.patDaten.get(48));
        zm.preisgruppe = currVO.getPreisgruppe();
        zm.gesamtZahl = currVO.getAnzHB();
        if (iret == 0) {
            if (testefuerbarcode) {
                constructGanzFreiRezHMap(zm);
                constructNormalRezHMap(zm, false);
                SystemConfig.hmAdrRDaten.put("<Rendbetrag>", "0,00");
            } else {
                constructGanzFreiRezHMap(zm);
            }
        }
        if (iret == 1) {
            constructAnfangFreiRezHMap(zm, true);
        }
        if (iret == 2) {
            constructNormalRezHMap(zm, false);
        }
        if (iret == 3) {
            constructEndeFreiRezHMap(zm, false);
        }
        return iret;
    }

    private static void constructNormalRezHMap(ZuzahlModell zm, boolean unregelmaessig) {
        // System.out.println("*****In Normal HMap*********");
        Double rezgeb;
        BigDecimal[] preise = { null, null, null, null };
        BigDecimal xrezgeb = BigDecimal.valueOf(Double.valueOf(0.000));

        //// System.out.println("nach nullzuweisung " +xrezgeb.toString());
        int[] anzahl = { 0, 0, 0, 0 };
        int[] artdbeh = { 0, 0, 0, 0 };
        int i;

        BigDecimal einzelpreis = null;
        BigDecimal poswert = null;
        BigDecimal rezwert = BigDecimal.valueOf(Double.valueOf(0.000));
        BigDecimal preistest = null;
        String stmt = null;
        String meldung = null;
        DecimalFormat dfx = new DecimalFormat("0.00");
        Rezeptvector currVO = new Rezeptvector(Reha.instance.patpanel.vecaktrez);
        String xdiszi = getDisziplinFromRezNr(currVO.getRezNb());
        int xpreisgr = (currVO.getPreisgruppe()) - 1;
        String xrezdatum = DatFunk.sDatInDeutsch(currVO.getRezeptDatum());

        SystemConfig.hmAdrRDaten.put("<Rid>", currVO.getId());
        SystemConfig.hmAdrRDaten.put("<Rnummer>", currVO.getRezNb());
        SystemConfig.hmAdrRDaten.put("<Rdatum>", xrezdatum);
        boolean neuerpreis = neuePreisNachRezeptdatumOderStichtag(xdiszi, xpreisgr, xrezdatum, false,
                currVO.getVec_rez());
        // System.out.println("Neuer Preis = "+neuerpreis+"\n");

        boolean preisRegelIsSplit = (4 == SystemPreislisten.hmNeuePreiseRegel.get(xdiszi)
                                                                             .get(xpreisgr));
        boolean splitNeccessary = false;
        int anzBehVorSplit = 0;
        int anzBehNachSplit = 0;
        if (preisRegelIsSplit) {
            String stichtag = SystemPreislisten.hmNeuePreiseAb.get(xdiszi)
                                                              .get(xpreisgr);
            Vector<String> tage = RezTools.holeEinzelTermineAusRezept(null, currVO.getTermine());
            if(DatFunk.TageDifferenz(xrezdatum, stichtag) >= 0) {
                // Rezept wurde vor Stichtag ausgestellt
                anzBehVorSplit = anzBehandeltVorSplit(tage,stichtag);
                if ((anzBehVorSplit > 0)  && (anzBehVorSplit < currVO.getAnzVorrBeh())) {
                    splitNeccessary = true;
                }
            }
            anzBehNachSplit = anzBehandeltNachSplit(tage,stichtag);
            logger.info("Splitting: " + splitNeccessary + "  PastSplit: " + anzBehNachSplit + "  Anz. davor: " + anzBehVorSplit);
        }
        for (i = 0; i < 4; i++) {
            int idx = i + 1;
            anzahl[i] = currVO.getAnzBeh(idx);
            artdbeh[i] = Integer.parseInt(currVO.getArtDBehandlS(idx));
            if (!neuerpreis) {
                if (artdbeh[i] > 0) {
                    preise[i] = BigDecimal.valueOf(Double.valueOf(getPreisAltFromID(Integer.toString(artdbeh[i]),
                            Integer.toString(xpreisgr), SystemPreislisten.hmPreise.get(xdiszi)
                                                                                  .get(xpreisgr))));
                } else {
                    preise[i] = BigDecimal.valueOf(Double.valueOf("0.00"));
                }
            } else {
                try {
                    if (artdbeh[i] > 0) {
                        preistest = BigDecimal.valueOf(Double.valueOf(currVO.getPreis(idx)));
                        preise[i] = BigDecimal.valueOf(Double.valueOf(getPreisAktFromID(Integer.toString(artdbeh[i]),
                                Integer.toString(xpreisgr), SystemPreislisten.hmPreise.get(xdiszi)
                                                                                      .get(xpreisgr))));
                        if (preistest.compareTo(preise[i]) != 0) {
                            String thisPos = currVO.getHmPos(idx);
                            String preisNeu = dfx.format(preise[i])
                                                 .replace(",", ".");
                            if (preisRegelIsSplit && splitNeccessary) {
                                // TODO: 
                                // Befreiung, u18 beruecksichtigen, 
                                // Daten fuer detaillierte RGR/Quittung in HM + Anpassung Vorlagen 
                                // ==> erstmal nur Verweis auf §302-Fenster
                                if (keineSplitZuzahlung("Achtung: Unterschiedliche Preise für Position " + thisPos + "!!!")){
                                    throwNoSplit();
                                    return;
                                // 24-01: wahlweise alte Funktion (= fehlerhafte Quittung/Rechnung erstellen)
                                } else {
                                    splitNeccessary = false;
                                    meldung = "Berechung erfolgt mit dem Preis aus der Preisliste, Rezept wird aktualisiert!";
                                    switchToPreisNeu(meldung,idx,preisNeu,currVO);
                                }
                            } else {
                                meldung = "Achtung: Unterschiedliche Preise!!!\n\n"  
                                        + "Im Rezept gespeicherter Preis für Position "
                                        + thisPos + " = " + dfx.format(preistest)
                                        + "\n" + "In der Preisliste gespeicherter Preis für Position "
                                        + thisPos + " = " + dfx.format(preise[i]) + "\n\n"
                                        + "Vermutete Ursache: Die Preisliste wurde nach der Rezeptanlage aktualisiert\n"
                                        + "Berechung erfolgt mit dem Preis aus der Preisliste, Rezept wird aktualisiert!";
                                switchToPreisNeu(meldung,idx,preisNeu,currVO);
                            }
                        }
                    } else {
                        preise[i] = BigDecimal.valueOf(Double.valueOf("0.00"));
                    }
                } catch (IllegalStateException e) {
                    logger.error("constructNormalRezHMap Error: " + e.getLocalizedMessage());
//                    throw new IllegalStateException("missing termin(e)");
                    throw new IllegalStateException("split termine");
                } catch (Exception ex) {
                    preise[i] = BigDecimal.valueOf(Double.valueOf(Reha.instance.patpanel.vecaktrez.get(i + 18)));   // old
                    preise[i] = BigDecimal.valueOf(Double.valueOf(currVO.getPreis(idx)));
                }
            }
        }
        xrezgeb = xrezgeb.add(BigDecimal.valueOf(Double.valueOf(10.00)));
        rezgeb = 10.00;
        //// System.out.println("nach 10.00 zuweisung " +rezgeb.toString());

        BigDecimal endpos;
//        SystemConfig.hmAdrRDaten.put("<Rnummer>", currVO.getRezNb()); // nochmal? (doppelt)
        SystemConfig.hmAdrRDaten.put("<Rpatid>", currVO.getPatIntern());
//        SystemConfig.hmAdrRDaten.put("<Rdatum>", xrezdatum);  // nochmal? (doppelt)
        
        SystemConfig.hmAdrRDaten.put("<Rpauschale>", dfx.format(rezgeb));

        for (i = 0; i < 4; i++) {
            int idx = i + 1;
            //// System.out.println(Integer.valueOf(anzahl[i]).toString()+" / "+
             //// Integer.valueOf(artdbeh[i]).toString()+" / "+ preise[i].toString() );
            if (artdbeh[i] > 0) {
                SystemConfig.hmAdrRDaten.put("<Rposition" + (idx) + ">", currVO.getHmPos(idx));

                String id = currVO.getArtDBehandlS(idx);
                SystemConfig.hmAdrRDaten.put("<Rlangtext" + (idx) + ">",
                        RezTools.getLangtextFromID(id, "", SystemPreislisten.hmPreise.get(xdiszi)
                                                                                     .get(xpreisgr)));

                SystemConfig.hmAdrRDaten.put("<Rkuerzel" + (idx) + ">",
                        RezTools.getKurzformFromID(id, SystemPreislisten.hmPreise.get(xdiszi)
                                                                                 .get(xpreisgr)));
                
/*****************/
                if (preisRegelIsSplit && splitNeccessary) {
                    if (keineSplitZuzahlung("Wechsel der Preisliste erkannt.")){
                        throwNoSplit();
                        return;
                    }else{
                        // 24-01: wahlweise alte Funktion (= fehlerhafte Quittung/Rechnung erstellen)
                        splitNeccessary = false;
                    }                    
                }

                if (!preisRegelIsSplit || !splitNeccessary) {
                    SystemConfig.hmAdrRDaten.put("<Rpreis" + (idx) + ">", dfx.format(preise[i]));   // TODO: split

                    einzelpreis = preise[i].divide(BigDecimal.valueOf(Double.valueOf(10.000)));     // split (Zuzahlung f. 1 Behandlg.)

                    poswert = preise[i].multiply(BigDecimal.valueOf(Double.valueOf(anzahl[i])));    // split (Gesamtwert der HM-Pos)
                    rezwert = rezwert.add(poswert);                                                 // split (Gesamtwert der VO aufsummieren)
                    //// System.out.println("Einzelpreis "+i+" = "+einzelpreis);
                    BigDecimal testpr = einzelpreis.setScale(2, BigDecimal.ROUND_HALF_UP);          // split (Zuzahlung f. 1 Behandlg. gerundet)
                    //// System.out.println("test->Einzelpreis "+i+" = "+testpr);

                    SystemConfig.hmAdrRDaten.put("<Rproz" + (idx) + ">", dfx.format(testpr));
                    SystemConfig.hmAdrRDaten.put("<Ranzahl" + (idx) + ">", Integer.valueOf(anzahl[i])// split Anz. Beh. fuer diese HM-Pos.
                                                                                    .toString());

                    endpos = testpr.multiply(BigDecimal.valueOf(Double.valueOf(anzahl[i])));        // split ZZgesamt fuer diese HM-Pos.
                    SystemConfig.hmAdrRDaten.put("<Rgesamt" + (idx) + ">", dfx.format(endpos));
                    rezgeb = rezgeb + endpos.doubleValue();                                         // split (Gesamtwert der ZZ aufsummieren)
                    //// System.out.println(rezgeb.toString());
                } else {
//                  String dummy = getZuZahlValueSplit(new Rezeptvector(currVO.getVec_rez()), idx);
                    // currVO.getAnzVorrBeh()
                }
/*****************/
            } else {
                SystemConfig.hmAdrRDaten.put("<Rposition" + (idx) + ">", "----");
                SystemConfig.hmAdrRDaten.put("<Rpreis" + (idx) + ">", "0,00");
                SystemConfig.hmAdrRDaten.put("<Rproz" + (idx) + ">", "0,00");
                SystemConfig.hmAdrRDaten.put("<Rgesamt" + (idx) + ">", "0,00");
                SystemConfig.hmAdrRDaten.put("<Ranzahl" + (idx) + ">", "----");
                SystemConfig.hmAdrRDaten.put("<Rkuerzel" + (idx) + ">", "");
            }
        }
        sortHmAdrRdaten();
        /*****************************************************/
        if (zm.hausbesuch) { // Hausbesuch
            Object[] obi = hbNormal(zm, rezwert, rezgeb, currVO.getAnzHB(), neuerpreis);
            rezwert = (BigDecimal) obi[0];
            rezgeb = (Double) obi[1];
        }

        Double drezwert = rezwert.doubleValue();
        SystemConfig.hmAdrRDaten.put("<Rendbetrag>", dfx.format(rezgeb));
        SystemConfig.hmAdrRDaten.put("<Rwert>", dfx.format(drezwert));
        DecimalFormat df = new DecimalFormat("0.00");
        df.format(rezgeb);
        //// System.out.println("----------------------------------------------------");
        //// System.out.println("Endgültige und geparste Rezeptgebühr = "+s+" EUR");
        //// System.out.println(SystemConfig.hmAdrRDaten);

        // Hier muß noch Hausbesuchshandling eingebaut werden
        // Ebenso das Wegegeldhandling
    }

    private static void switchToPreisNeu(String meldung, int idx, String neuerPreis, Rezeptvector currVO) {
        JOptionPane.showMessageDialog(null, meldung);   // ? besser Abfrage, ob alte oder neue Preise?
        String stmt = "update verordn set preise" + (idx) + "='" + neuerPreis + "' where id='" + currVO.getId()
                + "' LIMIT 1";
        System.out.println("switchToPreisNeu: " + stmt);
        SqlInfo.sqlAusfuehren(stmt);
    }

    private static void throwNoSplit() {
        // Aktuellen StackTrace holen u. aufrufende Klasse/Methode ermitteln
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String callingClass = stackTrace[4].getClassName();
        String callingMethod = stackTrace[4].getMethodName();
        logger.info("throwNoSplit  aufruf aus: " + callingClass + ":" + callingMethod);
        if ("doRezeptgebuehrRechnung".equals(callingMethod) || "doRezeptGebuehr".equals(callingMethod)) {
            throw new IllegalStateException("split termine");  // Abbruch, da Splitting nicht programmiert (s. ToDo)
        } else {
            logger.error("throwNoSplit  unerwarteter aufruf aus: " + callingClass + ":" + callingMethod);            
        }
    }

    private static boolean keineSplitZuzahlung(String reason) {
        String meldung;
        meldung = reason 
                + "\n\nEs ist Splitting gefordert.\n"
//                                            + "Für eine korrekte Berechnung muss mind. die "
//                                            + "1. Behandlung nach dem Split-Datum eingetragen sein!";
                + "Die korrekte Berechnung der Zuzahlung ist erst nach Abschluß des Rezeptes "
                + "im Abrechnungsfenster möglich.";
//        String keep = (String) UIManager.get("OptionPane.okButtonText");
//        UIManager.put("OptionPane.okButtonText", "Abbruch");
//        JOptionPane.showMessageDialog(null, meldung,null, JOptionPane.ERROR_MESSAGE);
//        UIManager.put("OptionPane.okButtonText", keep);
        Object[] options = {"<html><b>OK, verstanden<br>(Bearbeitung abbechen)</b></html>",
                "<html><b>Trotzdem kassieren<br><font color='#ff0000'>(wahrscheinl. spätere Korrekur nötig)</font></b></html>"}; 
        int anfrage = JOptionPane.showOptionDialog(null, meldung, "Warnung", JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE, null, options, options[0]);
        if (anfrage != JOptionPane.NO_OPTION) {
            return true;
        }
        return false;
    }

    public static void constructRawHMap() { // Quittung-1
        new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                try {
                    DecimalFormat df = new DecimalFormat("0.00");
                    Rezeptvector currVO = new Rezeptvector(Reha.instance.patpanel.vecaktrez);
                    String diszi = getDisziplinFromRezNr(currVO.getRezNb());

                     int pg = currVO.getPreisgruppe() - 1;
                    String id = "";
//                    SystemConfig.hmAdrRDaten.clear();// tst only
                    SystemConfig.hmAdrRDaten.put("<Rid>", currVO.getId());
                    SystemConfig.hmAdrRDaten.put("<Rnummer>", currVO.getRezNb());
                    SystemConfig.hmAdrRDaten.put("<Rdatum>", DatFunk.sDatInDeutsch(currVO.getRezeptDatum()));
                    SystemConfig.hmAdrRDaten.put("<Rdiagnose>", currVO.getDiagn());

                    BigDecimal dummyproz = null;
                    BigDecimal roundproz = null;
                    for (int i = 0; i < 4; i++) {
                        int idx = i + 1;
                        id = currVO.getArtDBehandlS(idx);
                        SystemConfig.hmAdrRDaten.put("<Rposition" + (idx) + ">", currVO.getHmPos(idx));
                        SystemConfig.hmAdrRDaten.put("<Rpreis" + (idx) + ">", currVO.getPreis(idx)
                                                                                    .replace(".", ","));
                        SystemConfig.hmAdrRDaten.put("<Ranzahl" + (idx) + ">", currVO.getAnzBehS(idx));
                        dummyproz = BigDecimal.valueOf(Double.valueOf(currVO.getPreis(idx)))
                                              .divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                        roundproz = dummyproz.setScale(2, BigDecimal.ROUND_HALF_UP);
                        SystemConfig.hmAdrRDaten.put("<Rgesamt" + (idx) + ">",
                                df.format(roundproz.multiply(BigDecimal.valueOf(
                                        Double.valueOf(SystemConfig.hmAdrRDaten.get("<Ranzahl" + (idx) + ">"))))
                                                   .doubleValue()));
                        if (!"0".equals(id)) {
                            SystemConfig.hmAdrRDaten.put("<Rkuerzel" + (idx) + ">",
                                    getKurzformFromID(id, SystemPreislisten.hmPreise.get(diszi)
                                                                                    .get(pg)));
                            SystemConfig.hmAdrRDaten.put("<Rlangtext" + (idx) + ">",
                                    getLangtextFromID(id, "", SystemPreislisten.hmPreise.get(diszi)
                                                                                        .get(pg)));
                        } else {
                            SystemConfig.hmAdrRDaten.put("<Rkuerzel" + (idx) + ">", "");
                            SystemConfig.hmAdrRDaten.put("<Rlangtext" + (idx) + ">", "");
                        }
                    }
                    sortHmAdrRdaten();
                    // Hausbesuche
                    if ("T".equals(Reha.instance.patpanel.vecaktrez.get(43))) {   // old
                        SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", Reha.instance.patpanel.vecaktrez.get(64));  // old-ok
                    }
                    if (currVO.getHausbesuch()) {
                        SystemConfig.hmAdrRDaten.put("<Rhbpos>", SystemPreislisten.hmHBRegeln.get(diszi)
                                                                                             .get(pg)
                                                                                             .get(0));
                        SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", Reha.instance.patpanel.vecaktrez.get(64));  // old-ok
                        SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", currVO.getAnzHBS());
                        SystemConfig.hmAdrRDaten.put("<Rhbpreis>",
                                getPreisAktFromPos(SystemConfig.hmAdrRDaten.get("<Rhbpos>"), "",
                                        SystemPreislisten.hmPreise.get(diszi)
                                                                  .get(pg)).replace(".", ","));
                        SystemConfig.hmAdrRDaten.put("<Rwegpos>", SystemPreislisten.hmHBRegeln.get(diszi)
                                                                                              .get(pg)
                                                                                              .get(2));
                        SystemConfig.hmAdrRDaten.put("<Rweganzahl>", Reha.instance.patpanel.vecaktrez.get(7));  // old-ok
                        SystemConfig.hmAdrRDaten.put("<Rweganzahl>", currVO.getKm());
                        SystemConfig.hmAdrRDaten.put("<Rwegpreis>",
                                getPreisAktFromPos(SystemConfig.hmAdrRDaten.get("<Rwegpos>"), "",
                                        SystemPreislisten.hmPreise.get(diszi)
                                                                  .get(pg)).replace(".", ","));
                    } else {
                        SystemConfig.hmAdrRDaten.put("<Rhbpos>", "");
                        SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", "");
                        SystemConfig.hmAdrRDaten.put("<Rhbpreis>", "");
                        SystemConfig.hmAdrRDaten.put("<Rwegpos>", "");
                        SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "");
                        SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                // System.out.println(SystemConfig.hmAdrRDaten);
                return null;
            }
        }.execute();
    }

    private static void constructGanzFreiRezHMap(ZuzahlModell zm) {
        Rezeptvector currVO = new Rezeptvector(Reha.instance.patpanel.vecaktrez);
//        SystemConfig.hmAdrRDaten.clear();// tst only
        SystemConfig.hmAdrRDaten.put("<Rid>", currVO.getId());
        SystemConfig.hmAdrRDaten.put("<Rnummer>", currVO.getRezNb());
        SystemConfig.hmAdrRDaten.put("<Rdatum>", DatFunk.sDatInDeutsch(currVO.getRezeptDatum()));
        SystemConfig.hmAdrRDaten.put("<Rpatid>", currVO.getPatIntern());
        SystemConfig.hmAdrRDaten.put("<Rpauschale>", "0,00");
        for (int i = 0; i < 5; i++) {
            int idx = i + 1;
            SystemConfig.hmAdrRDaten.put("<Rposition" + (idx) + ">", "----");
            SystemConfig.hmAdrRDaten.put("<Rpreis" + (idx) + ">", "0,00");
            SystemConfig.hmAdrRDaten.put("<Rproz" + (idx) + ">", "0,00");
            SystemConfig.hmAdrRDaten.put("<Rgesamt" + (idx) + ">", "0,00");
            SystemConfig.hmAdrRDaten.put("<Ranzahl" + (idx) + ">", "----");
            SystemConfig.hmAdrRDaten.put("<Rkuerzel" + (idx) + ">", "");
        }
        SystemConfig.hmAdrRDaten.put("<Rhbpos>", "----");
        SystemConfig.hmAdrRDaten.put("<Rhbpreis>", "0,00");
        SystemConfig.hmAdrRDaten.put("<Rhbproz>", "0,00");
        SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", "0,00");
        SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
        SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
        SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
        SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
        SystemConfig.hmAdrRDaten.put("<Rendbetrag>", "0,00");
        SystemConfig.hmAdrRDaten.put("<Rwert>", "0,00");
    }

    private static void constructAnfangFreiRezHMap(ZuzahlModell zm, boolean anfang) {   // Quittung-2, RGR-1
        try {
            // System.out.println("*****In Anfang-frei*********");
            if (anfang) {
                zm.gesamtZahl = zm.teil2;
                // System.out.println("Restliche Behandlungen berechnen = "+zm.gesamtZahl);
            } else {
                zm.gesamtZahl = zm.teil1;
                // System.out.println("Beginn der Behandlung berechnen = "+zm.gesamtZahl);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Double rezgeb = 0.000;
        BigDecimal[] preise = { null, null, null, null };
        BigDecimal xrezgeb = BigDecimal.valueOf(Double.valueOf(0.000));
        Rezeptvector currVO = new Rezeptvector(Reha.instance.patpanel.vecaktrez);

//        SystemConfig.hmAdrRDaten.clear();// tst only
        String xdiszi = getDisziplinFromRezNr(currVO.getRezNb());
        int xpreisgr = currVO.getPreisgruppe() - 1;
        String xrezdatum = DatFunk.sDatInDeutsch(currVO.getRezeptDatum());
        boolean neuerpreis = neuePreisNachRezeptdatumOderStichtag(xdiszi, xpreisgr, xrezdatum, false, currVO.getVec_rez());

        //// System.out.println("nach nullzuweisung " +xrezgeb.toString());
        int[] anzahl = { 0, 0, 0, 0 };
        int[] artdbeh = { 0, 0, 0, 0 };

        // Einbauen für Barcode
        int[] gesanzahl = { 0, 0, 0, 0 };
        int i;
        BigDecimal einzelpreis = null;
        BigDecimal poswert = null;
        BigDecimal rezwert = BigDecimal.valueOf(Double.valueOf(0.000));
        SystemConfig.hmAdrRDaten.put("<Rid>", currVO.getId());
        SystemConfig.hmAdrRDaten.put("<Rnummer>", currVO.getRezNb());
        SystemConfig.hmAdrRDaten.put("<Rdatum>", currVO.getRezeptDatum());
        for (i = 0; i < 4; i++) {
            int idx = i + 1;
            gesanzahl[i] = currVO.getAnzBeh(idx);
            anzahl[i] = gesanzahl[i];
            if (anzahl[i] >= zm.gesamtZahl) {
                anzahl[i] = zm.gesamtZahl;
            }
            artdbeh[i] = Integer.parseInt(currVO.getArtDBehandlS(idx));
            if (!neuerpreis) {
                if (artdbeh[i] > 0) {
                    preise[i] = BigDecimal.valueOf(Double.valueOf(getPreisAltFromID(Integer.toString(artdbeh[i]),
                            Integer.toString(xpreisgr), SystemPreislisten.hmPreise.get(xdiszi)
                                                                                  .get(xpreisgr))));
                } else {
                    preise[i] = BigDecimal.valueOf(Double.valueOf("0.00"));
                }
            } else {
                preise[i] = BigDecimal.valueOf(Double.valueOf(currVO.getPreis(idx)));
            }
        }
        xrezgeb = xrezgeb.add(BigDecimal.valueOf(Double.valueOf(10.00)));
        if (anfang) {
            rezgeb = 00.00;
        } else {
            rezgeb = 10.00;
        }

        //// System.out.println("nach 10.00 zuweisung " +rezgeb.toString());
        DecimalFormat dfx = new DecimalFormat("0.00");
        BigDecimal endpos;
        SystemConfig.hmAdrRDaten.put("<Rnummer>", currVO.getRezNb());
        SystemConfig.hmAdrRDaten.put("<Rpatid>", currVO.getPatIntern());
        SystemConfig.hmAdrRDaten.put("<Rdatum>", DatFunk.sDatInDeutsch(currVO.getRezeptDatum()));
        SystemConfig.hmAdrRDaten.put("<Rpauschale>", dfx.format(rezgeb));

        for (i = 0; i < 4; i++) {
            int idx = i + 1;
             ////System.out.println(Integer.valueOf(anzahl[i]).toString()+" / "+
             //// Integer.valueOf(artdbeh[i]).toString()+" / "+ preise[i].toString() );
            if (artdbeh[i] > 0) {
                SystemConfig.hmAdrRDaten.put("<Rposition" + (idx) + ">", currVO.getHmPos(idx));
                SystemConfig.hmAdrRDaten.put("<Rpreis" + (idx) + ">", dfx.format(preise[i]));

                einzelpreis = preise[i].divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                // ***********vorher nur anzahl[]*****************/
                poswert = preise[i].multiply(BigDecimal.valueOf(Double.valueOf(gesanzahl[i]))); //poswert = preise[i].multiply(BigDecimal.valueOf(gesanzahl[i])); (?)

                rezwert = rezwert.add(poswert);
                //// System.out.println("Einzelpreis "+i+" = "+einzelpreis);
                BigDecimal testpr = einzelpreis.setScale(2, BigDecimal.ROUND_HALF_UP);
                //// System.out.println("test->Einzelpreis "+i+" = "+testpr);

                SystemConfig.hmAdrRDaten.put("<Rproz" + (idx) + ">", dfx.format(testpr));
                SystemConfig.hmAdrRDaten.put("<Ranzahl" + (idx) + ">", Integer.toString(anzahl[i]));

                endpos = testpr.multiply(BigDecimal.valueOf(Double.valueOf(anzahl[i])));    // endpos = testpr.multiply(BigDecimal.valueOf(anzahl[i])); (?)
                SystemConfig.hmAdrRDaten.put("<Rgesamt" + (idx) + ">", dfx.format(endpos));
                rezgeb = rezgeb + endpos.doubleValue();
                //// System.out.println(rezgeb.toString());
            } else {
                SystemConfig.hmAdrRDaten.put("<Rposition" + (idx) + ">", "----");
                SystemConfig.hmAdrRDaten.put("<Rpreis" + (idx) + ">", "0,00");
                SystemConfig.hmAdrRDaten.put("<Rproz" + (idx) + ">", "0,00");
                SystemConfig.hmAdrRDaten.put("<Rgesamt" + (idx) + ">", "0,00");
                SystemConfig.hmAdrRDaten.put("<Ranzahl" + (idx) + ">", "----");
            }
        }
        sortHmAdrRdaten();
        if (zm.hausbesuch) { // Hausbesuch
            if (zm.gesamtZahl > Integer.valueOf(Reha.instance.patpanel.vecaktrez.get(64))) { // old-ok
                zm.gesamtZahl = Integer.parseInt(Reha.instance.patpanel.vecaktrez.get(64)); // old-ok
            }
            if (zm.gesamtZahl > currVO.getAnzHB()) {
                zm.gesamtZahl = currVO.getAnzHB();
            }
            Object[] obi = hbNormal(zm, rezwert, rezgeb, Integer.valueOf(Reha.instance.patpanel.vecaktrez.get(64)), // old-ok
                    neuerpreis);
            obi = hbNormal(zm, rezwert, rezgeb, currVO.getAnzHB(), neuerpreis);
            rezwert = (BigDecimal) obi[0];
            rezgeb = (Double) obi[1];
        }
        Double drezwert = rezwert.doubleValue();
        SystemConfig.hmAdrRDaten.put("<Rendbetrag>", dfx.format(rezgeb));
        SystemConfig.hmAdrRDaten.put("<Rwert>", dfx.format(drezwert));
        DecimalFormat df = new DecimalFormat("0.00");
        df.format(rezgeb);
        // System.out.println("----------------------------------------------------");
        // System.out.println("Endgültige und geparste Rezeptgebühr = "+s+" EUR");
        //// System.out.println(SystemConfig.hmAdrRDaten);
    }

    private static void sortHmAdrRdaten() {
        int onceAgain = 0;
        HashMap<String, String> voData = SystemConfig.hmAdrRDaten;
        
        do {
            onceAgain = 0;
            for (int i = 1; i < 4; i++) {
                String sCurr = SystemConfig.hmAdrRDaten.get("<Rposition" + (i) + ">");
                String sNext = SystemConfig.hmAdrRDaten.get("<Rposition" + (i + 1) + ">");
                if (sCurr.equals("----") && !sNext.equals("----")) {
                    // akt. Platz ist leer; Nachfolger nicht -> Plaetze tauschen
                    String currIdx = String.valueOf(i);
                    String nextIdx = String.valueOf(1 + i);
                    String sTmp = SystemConfig.hmAdrRDaten.get("<Rposition" + currIdx + ">");
                    String sTmp2 = SystemConfig.hmAdrRDaten.get("<Rposition" + nextIdx + ">");
                    SystemConfig.hmAdrRDaten.put("<Rposition" + currIdx + ">", sTmp2);
                    SystemConfig.hmAdrRDaten.put("<Rposition" + nextIdx + ">", sTmp);

                    sTmp = SystemConfig.hmAdrRDaten.get("<Rpreis" + currIdx + ">");
                    sTmp2 = SystemConfig.hmAdrRDaten.get("<Rpreis" + nextIdx + ">");
                    SystemConfig.hmAdrRDaten.put("<Rpreis" + currIdx + ">", sTmp2);
                    SystemConfig.hmAdrRDaten.put("<Rpreis" + nextIdx + ">", sTmp);

                    sTmp = SystemConfig.hmAdrRDaten.get("<Rproz" + currIdx + ">");
                    sTmp2 = SystemConfig.hmAdrRDaten.get("<Rproz" + nextIdx + ">");
                    SystemConfig.hmAdrRDaten.put("<Rproz" + currIdx + ">", sTmp2);
                    SystemConfig.hmAdrRDaten.put("<Rproz" + nextIdx + ">", sTmp);

                    sTmp = SystemConfig.hmAdrRDaten.get("<Rgesamt" + currIdx + ">");
                    sTmp2 = SystemConfig.hmAdrRDaten.get("<Rgesamt" + nextIdx + ">");
                    SystemConfig.hmAdrRDaten.put("<Rgesamt" + currIdx + ">", sTmp2);
                    SystemConfig.hmAdrRDaten.put("<Rgesamt" + nextIdx + ">", sTmp);

                    sTmp = SystemConfig.hmAdrRDaten.get("<Ranzahl" + currIdx + ">");
                    sTmp2 = SystemConfig.hmAdrRDaten.get("<Ranzahl" + nextIdx + ">");
                    SystemConfig.hmAdrRDaten.put("<Ranzahl" + currIdx + ">", sTmp2);
                    SystemConfig.hmAdrRDaten.put("<Ranzahl" + nextIdx + ">", sTmp);

                    sTmp = SystemConfig.hmAdrRDaten.get("<Rkuerzel" + currIdx + ">");
                    sTmp2 = SystemConfig.hmAdrRDaten.get("<Rkuerzel" + nextIdx + ">");
                    SystemConfig.hmAdrRDaten.put("<Rkuerzel" + currIdx + ">", sTmp2);
                    SystemConfig.hmAdrRDaten.put("<Rkuerzel" + nextIdx + ">", sTmp);

                    sTmp = SystemConfig.hmAdrRDaten.get("<Rlangtext" + currIdx + ">");
                    sTmp2 = SystemConfig.hmAdrRDaten.get("<Rlangtext" + nextIdx + ">");
                    SystemConfig.hmAdrRDaten.put("<Rlangtext" + currIdx + ">", sTmp2);
                    SystemConfig.hmAdrRDaten.put("<Rlangtext" + nextIdx + ">", sTmp);
                    
                    onceAgain++;
                }
            }
        } while (onceAgain > 0);
    }

    public static void constructEndeFreiRezHMap(ZuzahlModell zm, boolean anfang) {
        // System.out.println("*****Über Ende Frei*********");
        constructAnfangFreiRezHMap(zm, anfang);
    }

    public static Vector<Vector<String>> splitteTermine(String terms) {
        Vector<Vector<String>> termine = new Vector<>();
        String[] tlines = terms.split("\n");
        int lines = tlines.length;
        //// System.out.println("Anzahl Termine = "+lines);
        Vector<String> tvec = new Vector<>();
        String[] terdat = null;
        for (int i = 0; i < lines; i++) {
            terdat = tlines[i].split("@");
            int ieinzel = terdat.length;
            if (ieinzel <= 1) {
                return (Vector<Vector<String>>) termine.clone();
            }
            //// System.out.println("Anzahl Splits = "+ieinzel);
            tvec.clear();
            for (int y = 0; y < ieinzel; y++) {
                tvec.add("".equals(terdat[y].trim()) ? "  .  .    " : terdat[y]);
            }
            termine.add((Vector<String>) tvec.clone());
        }
        return (Vector<Vector<String>>) termine.clone();
    }

    public static Object[] JahrEnthalten(Vector<String> vtage, String jahr) {
        Object[] ret = { Boolean.FALSE, -1 };
        for (int i = 0; i < vtage.size(); i++) {
            if (vtage.get(i)
                     .equals(jahr)) {
                ret[0] = true;
                ret[1] = Integer.valueOf(i);
                break;
            }
        }
        return ret;
    }

    private static Object[] JahresWechsel(Vector<String> vtage, String jahr) {
        Object[] ret = { Boolean.FALSE, -1, -1 };
        for (int i = 0; i < vtage.size(); i++) {
            if (!vtage.get(i)
                      .substring(6)
                      .equals(jahr)) {
                ret[0] = true;
                ret[1] = Integer.valueOf(i);
                ret[2] = maxAnzahl() - (Integer) ret[1];
                return ret;
            }
        }
        return ret;
    }

    private static int maxAnzahl() {
        Rezeptvector currVO = new Rezeptvector(Reha.instance.patpanel.vecaktrez);
        int ret = -1;
        int test;
        for (int i = 1; i < 5; i++) {
            test = currVO.getAnzBeh(i);
            if (test > ret) {
                ret = test;
            }
        }
        return ret;
    }

    private static String PreisUeberPosition(String position, int preisgruppe, String disziplin, boolean neu) {
        String ret = null;
        Vector<?> preisvec;
        preisvec = SystemPreislisten.hmPreise.get(getDisziplinFromRezNr(disziplin))
                                             .get(preisgruppe - 1);
        for (int i = 0; i < preisvec.size(); i++) {
            if (((String) ((Vector<?>) preisvec.get(i)).get(2)).equals(position)) {
                ////System.out.println("Der Preis von "+position+" = "+ret);
                return (String) ((Vector<?>) preisvec.get(i)).get(3 + (neu ? 0 : 1));
            }
        }
        //// System.out.println("Der Preis von "+position+" wurde nicht gefunden!!");
        return ret;
    }

    public static boolean isPhysio (String reznr) {
        return (reznr.startsWith("KG") ? true : false);
    }
    public static boolean isMassage (String reznr) {
        return (reznr.startsWith("MA") ? true : false);
    }
    public static boolean isErgo (String reznr) {
        return (reznr.startsWith("ER") ? true : false);
    }
    public static boolean isLogo (String reznr) {
        return (reznr.startsWith("LO") ? true : false);
    }
    public static boolean isPodo (String reznr) {
        return (reznr.startsWith("PO") ? true : false);
    }
    public static boolean isReha (String reznr) {
        return (reznr.startsWith("RH") ? true : false);
    }
    public static boolean isRSport (String reznr) {
        return (reznr.startsWith("RS") ? true : false);
    }
    public static boolean isFTrain (String reznr) {
        return (reznr.startsWith("FT") ? true : false);
    }
    
    public static String getDisziplinFromRezNr(String reznr) {
        if (isPhysio (reznr)) {
            return "Physio";
        } else if (isMassage (reznr)) {
            return "Massage";
        } else if (isErgo (reznr)) {
            return "Ergo";
        } else if (isLogo (reznr)) {
            return "Logo";
        } else if (isPodo (reznr)) {
            return "Podo";
        } else if (isReha (reznr)) {
            return "Reha";
        } else if (isRSport (reznr)) {
            return "Rsport";
        } else if (isFTrain (reznr)) {
            return "Ftrain";
        }
        return "Physio";
    }

    public static Object[] ermittleRezeptwert(Vector<String> vec) {
        Object[] retobj = { null, null, null };
        return retobj;
    }

    public static Object[] ermittleHBwert(Vector<String> vec) {
        Object[] retobj = { null, null, null };
        String disziplin = getDisziplinFromRezNr(vec.get(1));
        String pos = "";
        Double preis = 0.00;
        Double wgkm = "".equals(vec.get(7)) ? 0.00 : Double.parseDouble(vec.get(7));

        String pospauschale = "";
        Double preispauschale = 0.00;
        // erst testen ob HB-Einzeln oder HB-Mehrere
        int anzahl = Integer.parseInt(vec.get(64));
        int preisgruppe = Integer.parseInt(vec.get(41));
        if ("T".equals(vec.get(61))) {
            // Einzelhausbesuch
            pos = SystemPreislisten.hmHBRegeln.get(disziplin)
                                              .get(preisgruppe - 1)
                                              .get(0);
            preis = Double.parseDouble(getPreisAktFromPos(pos, Integer.toString(preisgruppe),
                    SystemPreislisten.hmPreise.get(disziplin)
                                              .get(preisgruppe - 1)));
            retobj[0] = BigDecimal.valueOf(preis)
                                  .multiply(BigDecimal.valueOf(Double.parseDouble(Integer.toString(anzahl))))
                                  .doubleValue();
            // testen ob Fahrtgeld überhaupt gezahlt wird;
            if (keineWeggebuehrBeiHB(disziplin, Integer.toString(preisgruppe))) {
                return retobj;
            }
            if (zweiPositionenBeiHB(disziplin, Integer.toString(preisgruppe))) {
                // Weggebühr und pauschale
                pos = SystemPreislisten.hmHBRegeln.get(disziplin)
                                                  .get(preisgruppe - 1)
                                                  .get(2);
                preis = Double.parseDouble(getPreisAktFromPos(pos, Integer.toString(preisgruppe),
                        SystemPreislisten.hmPreise.get(disziplin)
                                                  .get(preisgruppe - 1)));
                BigDecimal kms = BigDecimal.valueOf(preis)
                                           .multiply(BigDecimal.valueOf(Double.parseDouble(Integer.toString(anzahl))));
                kms = kms.multiply(BigDecimal.valueOf(wgkm));

                pospauschale = SystemPreislisten.hmHBRegeln.get(disziplin)
                                                           .get(preisgruppe - 1)
                                                           .get(3);
                preispauschale = Double.parseDouble(getPreisAktFromPos(pospauschale, Integer.toString(preisgruppe),
                        SystemPreislisten.hmPreise.get(disziplin)
                                                  .get(preisgruppe - 1)));
                // System.out.println("kms="+kms);
                // System.out.println(BigDecimal.valueOf(preispauschale).multiply(BigDecimal.valueOf(Double.parseDouble(Integer.toString(anzahl)))));
                // System.out.println("pospauschale="+pospauschale);
                // System.out.println("preispauschale="+preispauschale);
                if (kms.doubleValue() > BigDecimal.valueOf(preispauschale)
                                                  .multiply(BigDecimal.valueOf(
                                                          Double.parseDouble(Integer.toString(anzahl))))
                                                  .doubleValue()) {
                    retobj[1] = kms.doubleValue();
                } else {
                    retobj[1] = BigDecimal.valueOf(preispauschale)
                                          .multiply(BigDecimal.valueOf(Double.parseDouble(Integer.toString(anzahl))))
                                          .doubleValue();
                }
                return retobj;
            }
        } else {
            // Mehrere Hausbesuch
            pos = SystemPreislisten.hmHBRegeln.get(disziplin)
                                              .get(preisgruppe - 1)
                                              .get(1);
            preis = Double.parseDouble(getPreisAktFromPos(pos, Integer.toString(preisgruppe),
                    SystemPreislisten.hmPreise.get(disziplin)
                                              .get(preisgruppe - 1)));
            retobj[0] = BigDecimal.valueOf(preis)
                                  .multiply(BigDecimal.valueOf(Double.parseDouble(Integer.toString(anzahl))))
                                  .doubleValue();
        }

        return retobj;
    }

    public static Vector<String> macheUmsatzZeile(Vector<Vector<String>> vec, String tag, String kaluser) {
        Vector<String> retvec = new Vector<>();
        for (int i = 0; i < 13; i++) {
            retvec.add("");
        }

        String disziplin = getDisziplinFromRezNr(vec.get(0)
                                                    .get(1));
        String pos = "";
        String preis = "";
        String pospauschale = "";

        String preispauschale = "";
        int preisgruppe = Integer.parseInt(vec.get(0)
                                              .get(41));
        String termine = vec.get(0)
                            .get(34);
        boolean rezept = false;
        Double wgkm;
        int fehlerstufe = 0;
        int ipos = 0;
        String kform = "";
        String[] posbestaetigt = null;
        Object[][] preisobj = { { null, null, null, null }, { null, null, null, null } };
        // 1. Termine aus Rezept holen
        String bestaetigte = vec.get(0)
                                .get(34);
        // 2. Testen ob der Tag erfaßt wenn nicht weiter mit der vollen Packung +
        // Fehlerstufe 1
        if (!termine.contains(tag)) {
            fehlerstufe = 1;
        } else {
            // 3. Sofern der Tagin der Termintabelle vorhanden ist,
            // die Positionen ermitteln
            // wenn keine Positionen vorhanden, weiter mit voller Packung + Fehlerstufe 2
            posbestaetigt = bestaetigte.substring(bestaetigte.indexOf(tag))
                                       .split("@")[3].split(",");
            if ("".equals(posbestaetigt[0].trim())) {
                fehlerstufe = 2;
            }
        }
        // 4. Überprüfen ob die Positionen in der Tarifgruppe existieren,
        // sofern nicht, Preise und Positionen aus Rezept entnehmen, also volle Packung
        // + Fehlerstufe 3
        if (fehlerstufe == 0) {
            for (int j = 0; j < posbestaetigt.length; j++) {
                if (!"".equals(posbestaetigt[j].trim())) {
                    if ("".equals(kform = getKurzformFromPos(posbestaetigt[j].trim(), Integer.toString(preisgruppe),
                            SystemPreislisten.hmPreise.get(disziplin)
                                                      .get(preisgruppe == 0 ? 0 : preisgruppe - 1)))) {
                        fehlerstufe = 3;
                        break;
                    }
                    preisobj[0][j] = kform;
                    preisobj[1][j] = getPreisAktFromPos(posbestaetigt[j], Integer.toString(preisgruppe),
                            SystemPreislisten.hmPreise.get(disziplin)
                                                      .get(preisgruppe == 0 ? 0 : preisgruppe - 1));
                }
            }
        }

        if (fehlerstufe == 0) {
            // 5. Wenn hier angekommen die Preise und Positionen aus der Preisliste
            // entnehmen
            for (int j = 0; j < 4; j++) {
                retvec.set(j, String.valueOf(preisobj[0][j] != null ? preisobj[0][j] : "-----"));
                retvec.set(j + 6, String.valueOf(preisobj[1][j] != null ? preisobj[1][j] : "0.00"));
            }
            retvec.set(12, "0");
        } else {
            for (int i = 0; i < 4; i++) {
                if (!"0".equals(vec.get(0)
                                   .get(i + 8)
                                   .trim())) {
                    pos = getKurzformFromID(vec.get(0)
                                               .get(i + 8)
                                               .trim(),
                            SystemPreislisten.hmPreise.get(disziplin)
                                                      .get(preisgruppe == 0 ? 0 : preisgruppe - 1));
                    if ("".equals(pos.trim())) {
                        pos = getKurzformFromPos(vec.get(0)
                                                    .get(i + 48)
                                                    .trim(),
                                Integer.toString(preisgruppe), SystemPreislisten.hmPreise.get(disziplin)
                                                                                         .get(preisgruppe == 0 ? 0
                                                                                                 : preisgruppe - 1));
                    }
                    retvec.set(i, pos);
                    retvec.set(i + 6, vec.get(0)
                                         .get(i + 18)
                                         .trim());
                } else {
                    retvec.set(i, "-----");
                    retvec.set(i + 6, "0.00");
                }
            }
            retvec.set(12, Integer.toString(fehlerstufe));
        }
        // mit Hausbesuch?
        if ("T".equals(vec.get(0)
                          .get(43))) {
            // Hausbesuch einzeln?
            if ("T".equals(vec.get(0)
                              .get(61))) {
                pos = SystemPreislisten.hmHBRegeln.get(disziplin)
                                                  .get(preisgruppe == 0 ? 0 : preisgruppe - 1)
                                                  .get(0);
                preis = getPreisAktFromPos(pos, Integer.toString(preisgruppe), SystemPreislisten.hmPreise.get(disziplin)
                                                                                                         .get(preisgruppe == 0
                                                                                                                 ? 0
                                                                                                                 : preisgruppe
                                                                                                                         - 1));
                retvec.set(4, pos);
                retvec.set(10, preis);

                if (!keineWeggebuehrBeiHB(disziplin, Integer.toString(preisgruppe == 0 ? 1 : preisgruppe))) {
                    //// System.out.println("Kasse kennt Weggebühr...");
                    if (zweiPositionenBeiHB(disziplin, Integer.toString(preisgruppe == 0 ? 1 : preisgruppe))) {
                        // Weggebühr und pauschale
                        //// System.out.println("Kasse kennt km und Pauschale...");
                        pos = SystemPreislisten.hmHBRegeln.get(disziplin)
                                                          .get(preisgruppe == 0 ? 0 : preisgruppe - 1)
                                                          .get(2);
                        pospauschale = SystemPreislisten.hmHBRegeln.get(disziplin)
                                                                   .get(preisgruppe == 0 ? 0 : preisgruppe - 1)
                                                                   .get(3);
                        wgkm = Double.parseDouble(vec.get(0)
                                                     .get(7));

                        if (kmBesserAlsPauschale(pospauschale, pos, wgkm, preisgruppe, disziplin)) {
                            // Kilometer verwenden
                            //// System.out.println("Kilometer verwenden...");
                            pos = SystemPreislisten.hmHBRegeln.get(disziplin)
                                                              .get(preisgruppe == 0 ? 0 : preisgruppe - 1)
                                                              .get(2);
                            preis = getPreisAktFromPos(pos, Integer.toString(preisgruppe),
                                    SystemPreislisten.hmPreise.get(disziplin)
                                                              .get(preisgruppe == 0 ? 0 : preisgruppe - 1));
                            BigDecimal kms = BigDecimal.valueOf(Double.parseDouble(preis))
                                                       .multiply(BigDecimal.valueOf(wgkm));
                            retvec.set(5, pos);
                            retvec.set(11, Double.toString(kms.doubleValue()));
                            //// System.out.println("Pos = "+pos);
                            //// System.out.println("Preis = "+preis);
                        } else {
                            // Pauschale verwenden
                            //// System.out.println("Pauschale verwenden....");
                            pospauschale = SystemPreislisten.hmHBRegeln.get(disziplin)
                                                                       .get(preisgruppe == 0 ? 0 : preisgruppe - 1)
                                                                       .get(3);
                            preis = getPreisAktFromPos(pospauschale, Integer.toString(preisgruppe),
                                    SystemPreislisten.hmPreise.get(disziplin)
                                                              .get(preisgruppe == 0 ? 0 : preisgruppe - 1));
                            // System.out.println("Pos = "+pos);
                            // System.out.println("Preis = "+preis);
                            retvec.set(5, pospauschale);
                            retvec.set(11, preis);
                        }
                    }
                } else {
                    // System.out.println("Kasse kennt keine Weggebühr....");
                    retvec.set(5, "-----");
                    retvec.set(11, "0.00");
                }
            } else {
                // Hausbesuch mit
                pos = SystemPreislisten.hmHBRegeln.get(disziplin)
                                                  .get(preisgruppe == 0 ? 0 : preisgruppe - 1)
                                                  .get(1);
                preis = getPreisAktFromPos(pos, Integer.toString(preisgruppe), SystemPreislisten.hmPreise.get(disziplin)
                                                                                                         .get(preisgruppe == 0
                                                                                                                 ? 0
                                                                                                                 : preisgruppe
                                                                                                                         - 1));
                retvec.set(4, pos);
                retvec.set(10, preis);
                retvec.set(5, "-----");
                retvec.set(11, "0.00");
            }
        } else {
            retvec.set(4, "-----");
            retvec.set(10, "0.00");
            retvec.set(5, "-----");
            retvec.set(11, "0.00");
        }
        return retvec;
    }

    public static boolean kmBesserAlsPauschale(String pospauschal, String poskm, Double anzahlkm, int preisgruppe,
            String disziplin) {
        String meldung = "";
        try {
            String preiskm;
            String preispauschal;
            meldung = " Pospauschal = " + pospauschal + "\n" + "PosKilometer = " + poskm + "\n" + "   Anzahl km = "
                    + anzahlkm + "\n" + " Preisgruppe = " + preisgruppe + "\n" + "   Disziplin = " + disziplin;
            preiskm = getPreisAktFromPos(poskm, Integer.toString(preisgruppe), SystemPreislisten.hmPreise.get(disziplin)
                                                                                                         .get(preisgruppe == 0
                                                                                                                 ? 0
                                                                                                                 : preisgruppe
                                                                                                                         - 1));
            BigDecimal kms = BigDecimal.valueOf(Double.parseDouble(preiskm))
                                       .multiply(BigDecimal.valueOf(anzahlkm));
            preispauschal = getPreisAktFromPos(pospauschal, Integer.toString(preisgruppe),
                    SystemPreislisten.hmPreise.get(disziplin)
                                              .get(preisgruppe == 0 ? 0 : preisgruppe - 1));
            BigDecimal pauschal = BigDecimal.valueOf(Double.parseDouble(preispauschal));
            return kms.doubleValue() > pauschal.doubleValue();
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null,
                    "Fehler in der Ermittlung km-Abrechnung besser als Pauschale\n" + meldung);
        }
        return false;
    }

    private static Object[] hbNormal(ZuzahlModell zm, BigDecimal rezwert, Double rezgeb, int realhbAnz,
            boolean neuerpreis) {
        Object[] retobj = { rezwert, rezgeb };
        // System.out.println("Die tatsächlich HB-Anzahl = "+realhbAnz);
        // System.out.println("Der Rezeptwert zu Beginn = "+retobj[0]);
        if (zm.hausbesuch) { // Hausbesuch
            // System.out.println("Hausbesuch ist angesagt");
            // String[] praefix = {"1","2","5","3","MA","KG","ER","LO"};
            String rezid = SystemConfig.hmAdrRDaten.get("<Rnummer>")
                                                   .substring(0, 2);
            String zz = SystemPreislisten.hmHBRegeln.get(getDisziplinFromRezNr(rezid))
                                                    .get(zm.preisgruppe - 1)
                                                    .get(4);
            String kmgeld = SystemPreislisten.hmHBRegeln.get(getDisziplinFromRezNr(rezid))
                                                        .get(zm.preisgruppe - 1)
                                                        .get(2);
            String kmpausch = SystemPreislisten.hmHBRegeln.get(getDisziplinFromRezNr(rezid))
                                                          .get(zm.preisgruppe - 1)
                                                          .get(3);
            String hbpos = SystemPreislisten.hmHBRegeln.get(getDisziplinFromRezNr(rezid))
                                                       .get(zm.preisgruppe - 1)
                                                       .get(0);
            String hbmit = SystemPreislisten.hmHBRegeln.get(getDisziplinFromRezNr(rezid))
                                                       .get(zm.preisgruppe - 1)
                                                       .get(1);

            String preis = "";
            BigDecimal bdrezgeb;
            BigDecimal bdposwert;
            BigDecimal bdpreis;
            BigDecimal bdendrezgeb;
            BigDecimal testpr;
            SystemConfig.hmAdrRDaten.put("<Rwegkm>", Integer.toString(zm.km));
            SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", Integer.toString(zm.gesamtZahl));
            DecimalFormat dfx = new DecimalFormat("0.00");

            if (zm.hbheim) { // und zwar im Heim
                // System.out.println("Der HB ist im Heim");
                if (zm.hbvoll) {// Volle Ziffer abrechnen?
                    // System.out.println("Es kann der volle Hausbesuch abgerechnet werden");
                    SystemConfig.hmAdrRDaten.put("<Rhbpos>", hbpos);
                    preis = PreisUeberPosition(SystemConfig.hmAdrRDaten.get("<Rhbpos>"), zm.preisgruppe,
                            SystemConfig.hmAdrRDaten.get("<Rnummer>")
                                                    .substring(0, 2),
                            neuerpreis);
                    bdpreis = new BigDecimal(Double.valueOf(preis));
                    bdposwert = bdpreis.multiply(BigDecimal.valueOf(Double.valueOf(realhbAnz)));
                    retobj[0] = ((BigDecimal) retobj[0]).add(BigDecimal.valueOf(bdposwert.doubleValue()));

                    if ("1".equals(zz)) {// Zuzahlungspflichtig
                        SystemConfig.hmAdrRDaten.put("<Rhbpreis>", preis);
                        bdrezgeb = bdpreis.divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                        testpr = bdrezgeb.setScale(2, BigDecimal.ROUND_HALF_UP);
                        bdendrezgeb = testpr.multiply(BigDecimal.valueOf(Double.valueOf(zm.gesamtZahl)));
                        SystemConfig.hmAdrRDaten.put("<Rhbproz>", dfx.format(testpr.doubleValue()));
                        SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", dfx.format(bdendrezgeb.doubleValue()));
                        retobj[1] = (Double) retobj[1] + bdendrezgeb.doubleValue();
                    } else {
                        SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", "----");
                        SystemConfig.hmAdrRDaten.put("<Rhbpos>", "----");
                        SystemConfig.hmAdrRDaten.put("<Rhbpreis>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rhbproz>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                        SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                        SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
                    }
                    if (!"".equals(kmgeld)) {// Wenn Kilometer abgerechnet werden können
                        // System.out.println("Es könnten Kilometer abgerechnet werden");
                        if (kmBesserAlsPauschale(kmpausch, kmgeld, Double.parseDouble(Integer.toString(zm.km)),
                                zm.preisgruppe, getDisziplinFromRezNr(SystemConfig.hmAdrRDaten.get("<Rnummer>")))) {
                            // Mit Kilometerabrechnung verdient man mehr
                            preis = PreisUeberPosition(kmgeld, zm.preisgruppe, SystemConfig.hmAdrRDaten.get("<Rnummer>")
                                                                                                       .substring(0, 2),
                                    neuerpreis);
                            SystemConfig.hmAdrRDaten.put("<Rwegpos>", "" + zm.km + "km*" + preis);
                            bdpreis = new BigDecimal(Double.valueOf(preis)).multiply(
                                    new BigDecimal(Double.valueOf(zm.km)));
                            bdposwert = bdpreis.multiply(BigDecimal.valueOf(Double.valueOf(realhbAnz)));
                            retobj[0] = ((BigDecimal) retobj[0]).add(BigDecimal.valueOf(bdposwert.doubleValue()));
                            SystemConfig.hmAdrRDaten.put("<Rwegpreis>", dfx.format(bdpreis.doubleValue()));
                            // System.out.println("Zuzahlungsmodus =
                            // "+SystemPreislisten.hmZuzahlModus.get(putRezNrGetDisziplin(rezid)).get(zm.preisgruppe-1));
                            if ("1".equals(zz)) {// Zuzahlungspflichtig
                                if (SystemPreislisten.hmZuzahlModus.get(getDisziplinFromRezNr(rezid))
                                                                   .get(zm.preisgruppe - 1) == 1) {
                                    bdrezgeb = bdpreis.divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                                    testpr = bdrezgeb.setScale(2, BigDecimal.ROUND_HALF_UP);
                                    bdendrezgeb = testpr.multiply(BigDecimal.valueOf(Double.valueOf(zm.gesamtZahl)));
                                    SystemConfig.hmAdrRDaten.put("<Rwegproz>", dfx.format(testpr.doubleValue()));
                                } else { // bayrische Variante
                                    bdrezgeb = BigDecimal.valueOf(Double.valueOf(preis))
                                                         .divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                                    testpr = bdrezgeb.setScale(2, BigDecimal.ROUND_HALF_UP);
                                    bdendrezgeb = testpr.multiply(BigDecimal.valueOf(Double.valueOf(zm.gesamtZahl)))
                                                        .multiply(new BigDecimal(Double.valueOf(zm.km)));
                                    SystemConfig.hmAdrRDaten.put("<Rwegproz>",
                                            dfx.format(testpr.doubleValue()) + "(*" + zm.km + "km)");
                                }
                                SystemConfig.hmAdrRDaten.put("<Rweggesamt>", dfx.format(bdendrezgeb.doubleValue()));
                                SystemConfig.hmAdrRDaten.put("<Rweganzahl>", Integer.valueOf(zm.gesamtZahl)
                                                                                    .toString());
                                retobj[1] = (Double) retobj[1] + bdendrezgeb.doubleValue();
                            } else {
                                SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rhbpos>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rhbpreis>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rhbproz>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
                            }

                            // hier zuerst die kilometer ermitteln mal Kilometerpreis = der Endpreis
                        } else // System.out.println("Es wurden keine Kilometer angegeben also wird nach
                               // Ortspauschale abgerechnet");
                        if (!"".equals(kmpausch)) {// Wenn die Kasse keine Pauschale zur Verfügung stellt
                            SystemConfig.hmAdrRDaten.put("<Rwegpos>", kmpausch);
                            preis = PreisUeberPosition(SystemConfig.hmAdrRDaten.get("<Rwegpos>"), zm.preisgruppe,
                                    SystemConfig.hmAdrRDaten.get("<Rnummer>")
                                                            .substring(0, 2),
                                    neuerpreis);
                            bdpreis = new BigDecimal(Double.valueOf(preis));
                            bdposwert = bdpreis.multiply(BigDecimal.valueOf(Double.valueOf(realhbAnz)));
                            retobj[0] = ((BigDecimal) retobj[0]).add(BigDecimal.valueOf(bdposwert.doubleValue()));
                            SystemConfig.hmAdrRDaten.put("<Rwegpreis>", dfx.format(bdpreis.doubleValue()));
                            if ("1".equals(zz)) {// Zuzahlungspflichtig
                                bdrezgeb = bdpreis.divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                                testpr = bdrezgeb.setScale(2, BigDecimal.ROUND_HALF_UP);
                                bdendrezgeb = testpr.multiply(BigDecimal.valueOf(Double.valueOf(zm.gesamtZahl)));
                                SystemConfig.hmAdrRDaten.put("<Rwegproz>", dfx.format(testpr.doubleValue()));
                                SystemConfig.hmAdrRDaten.put("<Rweggesamt>", dfx.format(bdendrezgeb.doubleValue()));
                                SystemConfig.hmAdrRDaten.put("<Rweganzahl>", Integer.valueOf(zm.gesamtZahl)
                                                                                    .toString());
                                retobj[1] = (Double) retobj[1] + bdendrezgeb.doubleValue();
                            } else {
                                SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rhbpos>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rhbpreis>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rhbproz>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
                            }
                        } else {
                            JOptionPane.showMessageDialog(null,
                                    "Dieser Kostenträger kennt keine Weg-Pauschale, geben Sie im Patientenstamm die Anzahl Kilometer an");
                            SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                            SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                            SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
                            SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
                            SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
                        }
                    } else {// es können keine Kilometer abgerechnet werden
                            // System.out.println("Zuzahlungsmodus =
                            // "+SystemPreislisten.hmZuzahlModus.get(putRezNrGetDisziplin(rezid)).get(zm.preisgruppe-1));
                        SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                        preis = PreisUeberPosition(SystemConfig.hmAdrRDaten.get("<Rwegpos>"), zm.preisgruppe,
                                SystemConfig.hmAdrRDaten.get("<Rnummer>")
                                                        .substring(0, 2),
                                neuerpreis);
                        if (preis != null) {
                            bdpreis = new BigDecimal(Double.valueOf(preis));
                            bdposwert = bdpreis.multiply(BigDecimal.valueOf(Double.valueOf(realhbAnz)));
                            retobj[0] = ((BigDecimal) retobj[0]).add(BigDecimal.valueOf(bdposwert.doubleValue()));
                            SystemConfig.hmAdrRDaten.put("<Rwegpreis>", dfx.format(bdpreis.doubleValue()));
                            if ("1".equals(zz)) {// Zuzahlungspflichtig
                                bdrezgeb = bdpreis.divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                                testpr = bdrezgeb.setScale(2, BigDecimal.ROUND_HALF_UP);
                                bdendrezgeb = testpr.multiply(BigDecimal.valueOf(Double.valueOf(zm.gesamtZahl)));
                                SystemConfig.hmAdrRDaten.put("<Rwegproz>", dfx.format(testpr.doubleValue()));
                                SystemConfig.hmAdrRDaten.put("<Rweganzahl>", Integer.valueOf(zm.gesamtZahl)
                                                                                    .toString());
                                SystemConfig.hmAdrRDaten.put("<Rweggesamt>", dfx.format(bdendrezgeb.doubleValue()));
                                retobj[1] = (Double) retobj[1] + bdendrezgeb.doubleValue();
                            } else {
                                SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rhbpos>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rhbpreis>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rhbproz>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                                SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
                                SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
                            }
                        } else {
                            SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                            SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                            SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
                            SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
                            SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
                        }
                    }
                } else {// nur Mit-Hausbesuch
                    SystemConfig.hmAdrRDaten.put("<Rhbpos>", hbmit);
                    preis = PreisUeberPosition(SystemConfig.hmAdrRDaten.get("<Rhbpos>"), zm.preisgruppe,
                            SystemConfig.hmAdrRDaten.get("<Rnummer>")
                                                    .substring(0, 2),
                            neuerpreis);
                    if (preis != null) {
                        bdpreis = new BigDecimal(Double.valueOf(preis));
                        bdposwert = bdpreis.multiply(BigDecimal.valueOf(Double.valueOf(realhbAnz)));
                        retobj[0] = ((BigDecimal) retobj[0]).add(BigDecimal.valueOf(bdposwert.doubleValue()));
                        if ("1".equals(zz)) {// Zuzahlungspflichtig
                            SystemConfig.hmAdrRDaten.put("<Rhbpreis>", preis);
                            bdrezgeb = bdpreis.divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                            testpr = bdrezgeb.setScale(2, BigDecimal.ROUND_HALF_UP);
                            bdendrezgeb = testpr.multiply(BigDecimal.valueOf(Double.valueOf(zm.gesamtZahl)));
                            SystemConfig.hmAdrRDaten.put("<Rhbproz>", dfx.format(testpr.doubleValue()));
                            SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", dfx.format(bdendrezgeb.doubleValue()));
                            // SystemConfig.hmAdrRDaten.put("<Rweganzahl>",Integer.valueOf(zm.gesamtZahl).toString()
                            // );
                            SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                            SystemConfig.hmAdrRDaten.put("<Rwpreis>", "0,00");
                            SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                            retobj[1] = (Double) retobj[1] + bdendrezgeb.doubleValue();
                        } else {
                            SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", "----");
                            SystemConfig.hmAdrRDaten.put("<Rhbpos>", "----");
                            SystemConfig.hmAdrRDaten.put("<Rhbpreis>", "0,00");
                            SystemConfig.hmAdrRDaten.put("<Rhbproz>", "0,00");
                            SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", "0,00");
                            SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                            SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                            SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
                            SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
                            SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
                        }
                    } else {
                        SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", "----");
                        SystemConfig.hmAdrRDaten.put("<Rhbpos>", "----");
                        SystemConfig.hmAdrRDaten.put("<Rhbpreis>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rhbproz>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                        SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                        SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
                    }
                }
            } else {// nicht im Heim
                    // System.out.println("Der Hausbesuch ist nicht in einem Heim");
                SystemConfig.hmAdrRDaten.put("<Rhbpos>", hbpos);
                preis = PreisUeberPosition(SystemConfig.hmAdrRDaten.get("<Rhbpos>"), zm.preisgruppe,
                        SystemConfig.hmAdrRDaten.get("<Rnummer>")
                                                .substring(0, 2),
                        neuerpreis);
                bdpreis = new BigDecimal(Double.valueOf(preis));
                bdposwert = bdpreis.multiply(BigDecimal.valueOf(Double.valueOf(realhbAnz)));
                retobj[0] = ((BigDecimal) retobj[0]).add(BigDecimal.valueOf(bdposwert.doubleValue()));
                SystemConfig.hmAdrRDaten.put("<Rhbpreis>", preis);

                bdrezgeb = bdpreis.divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                testpr = bdrezgeb.setScale(2, BigDecimal.ROUND_HALF_UP);
                bdendrezgeb = testpr.multiply(BigDecimal.valueOf(Double.valueOf(zm.gesamtZahl)));
                SystemConfig.hmAdrRDaten.put("<Rhbproz>", dfx.format(testpr.doubleValue()));
                SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", dfx.format(bdendrezgeb.doubleValue()));
                retobj[1] = (Double) retobj[1] + bdendrezgeb.doubleValue();

                if (!"".equals(kmgeld)) {// Wenn Kilometer abgerechnet werden k�nnen
                    // System.out.println("Es könnten Kilometer abgerechnet werden");
                    if (kmBesserAlsPauschale(kmpausch, kmgeld, Double.parseDouble(Integer.toString(zm.km)),
                            zm.preisgruppe, getDisziplinFromRezNr(SystemConfig.hmAdrRDaten.get("<Rnummer>")))) {
                        // Kilometerabrechnung besser als Pauschale
                        preis = PreisUeberPosition(kmgeld, zm.preisgruppe, SystemConfig.hmAdrRDaten.get("<Rnummer>")
                                                                                                   .substring(0, 2),
                                neuerpreis);
                        SystemConfig.hmAdrRDaten.put("<Rwegpos>", "" + zm.km + "km*" + preis);

                        bdpreis = new BigDecimal(Double.valueOf(preis)).multiply(new BigDecimal(Double.valueOf(zm.km)));
                        bdposwert = bdpreis.multiply(BigDecimal.valueOf(Double.valueOf(realhbAnz)));
                        retobj[0] = ((BigDecimal) retobj[0]).add(BigDecimal.valueOf(bdposwert.doubleValue()));
                        SystemConfig.hmAdrRDaten.put("<Rwegpreis>", dfx.format(bdpreis.doubleValue()));
                        if (SystemPreislisten.hmZuzahlModus.get(getDisziplinFromRezNr(rezid))
                                                           .get(zm.preisgruppe - 1) == 1) {
                            bdrezgeb = bdpreis.divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                            testpr = bdrezgeb.setScale(2, BigDecimal.ROUND_HALF_UP);
                            bdendrezgeb = testpr.multiply(BigDecimal.valueOf(Double.valueOf(zm.gesamtZahl)));
                            SystemConfig.hmAdrRDaten.put("<Rwegproz>", dfx.format(testpr.doubleValue()));
                        } else {
                            bdrezgeb = BigDecimal.valueOf(Double.valueOf(preis))
                                                 .divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                            testpr = bdrezgeb.setScale(2, BigDecimal.ROUND_HALF_UP);
                            bdendrezgeb = testpr.multiply(BigDecimal.valueOf(Double.valueOf(zm.gesamtZahl)))
                                                .multiply(new BigDecimal(Double.valueOf(zm.km)));
                            SystemConfig.hmAdrRDaten.put("<Rwegproz>",
                                    dfx.format(testpr.doubleValue()) + "(*" + zm.km + "km)");
                        }
                        SystemConfig.hmAdrRDaten.put("<Rweggesamt>", dfx.format(bdendrezgeb.doubleValue()));
                        SystemConfig.hmAdrRDaten.put("<Rweganzahl>", Integer.valueOf(zm.gesamtZahl)
                                                                            .toString());
                        retobj[1] = (Double) retobj[1] + bdendrezgeb.doubleValue();
                    } else // System.out.println("Es wurden keine Kilometer angegeben also wird nach
                    // Ortspauschale abgerechnet");
                    if (!"".equals(kmpausch)) {// Wenn die Kasse keine Pauschale zur Verfügung stellt
                        SystemConfig.hmAdrRDaten.put("<Rwegpos>", kmpausch);
                        preis = PreisUeberPosition(SystemConfig.hmAdrRDaten.get("<Rwegpos>"), zm.preisgruppe,
                                SystemConfig.hmAdrRDaten.get("<Rnummer>")
                                                        .substring(0, 2),
                                neuerpreis);
                        bdpreis = new BigDecimal(Double.valueOf(preis));
                        bdposwert = bdpreis.multiply(BigDecimal.valueOf(Double.valueOf(realhbAnz)));
                        retobj[0] = ((BigDecimal) retobj[0]).add(BigDecimal.valueOf(bdposwert.doubleValue()));
                        SystemConfig.hmAdrRDaten.put("<Rwegpreis>", dfx.format(bdpreis.doubleValue()));

                        bdrezgeb = bdpreis.divide(BigDecimal.valueOf(Double.valueOf(10.000)));
                        testpr = bdrezgeb.setScale(2, BigDecimal.ROUND_HALF_UP);
                        bdendrezgeb = testpr.multiply(BigDecimal.valueOf(Double.valueOf(zm.gesamtZahl)));
                        SystemConfig.hmAdrRDaten.put("<Rwegproz>", dfx.format(testpr.doubleValue()));
                        SystemConfig.hmAdrRDaten.put("<Rweggesamt>", dfx.format(bdendrezgeb.doubleValue()));
                        SystemConfig.hmAdrRDaten.put("<Rweganzahl>", Integer.valueOf(zm.gesamtZahl)
                                                                            .toString());
                        retobj[1] = (Double) retobj[1] + bdendrezgeb.doubleValue();
                    } else {
                        JOptionPane.showMessageDialog(null,
                                "Dieser Kostenträger kennt keine Weg-Pauschale, geben Sie im Patientenstamm die Anzahl Kilometer an");
                        SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                        SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                        SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
                        SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
                    }
                } else {// es können keine Kilometer abgerechnet werden
                    SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
                    SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
                    SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
                    SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
                    SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
                }
            }
        } else {
            SystemConfig.hmAdrRDaten.put("<Rhbanzahl>", "----");
            SystemConfig.hmAdrRDaten.put("<Rhbpos>", "----");
            SystemConfig.hmAdrRDaten.put("<Rhbpreis>", "0,00");
            SystemConfig.hmAdrRDaten.put("<Rhbproz>", "0,00");
            SystemConfig.hmAdrRDaten.put("<Rhbgesamt>", "0,00");
            SystemConfig.hmAdrRDaten.put("<Rweganzahl>", "----");
            SystemConfig.hmAdrRDaten.put("<Rwegpos>", "----");
            SystemConfig.hmAdrRDaten.put("<Rwegpreis>", "0,00");
            SystemConfig.hmAdrRDaten.put("<Rwegproz>", "0,00");
            SystemConfig.hmAdrRDaten.put("<Rweggesamt>", "0,00");
        }
        // System.out.println("Der Rezeptwert = "+retobj[0]);
        return retobj;
    }

    public static void constructVirginHMap() {  // nach Verlassen Rezeptmaske
        Rezeptvector currVO = new Rezeptvector(Reha.instance.patpanel.vecaktrez);

        try {
//            SystemConfig.hmAdrRDaten.clear();// tst only
            SystemConfig.hmAdrRDaten.put("<Rid>", currVO.getId());
            SystemConfig.hmAdrRDaten.put("<Rnummer>", currVO.getRezNb());
            SystemConfig.hmAdrRDaten.put("<Rdatum>", DatFunk.sDatInDeutsch(currVO.getRezeptDatum()));
            SystemConfig.hmAdrRDaten.put("<Rdiagnose>", currVO.getDiagn());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /** * Funktionen für Abrechnung nach §302. */
    public static Object[] unter18Check(Vector<Vector<Object>> behandlungsfall, String geburtstag) {
        // unter 18 ab Vector x über 18 gesplittet
        Object[] ret = { Boolean.TRUE, behandlungsfall.size(), Boolean.FALSE };
        String tag1 = (String) behandlungsfall.get(behandlungsfall.size() - 1)
                                              .get(0);
        String tag2 = (String) behandlungsfall.get(0)
                                              .get(0);
        if (DatFunk.Unter18(tag1, geburtstag)) {
            return ret;
        }
        if (!DatFunk.Unter18(tag1, geburtstag) && !DatFunk.Unter18(tag2, geburtstag)) {
            ret[0] = false;
            ret[1] = -1;
            ret[2] = false;
            return ret;
        }

        int i;
        for (i = 0; i < behandlungsfall.size(); i++) {
            tag1 = (String) behandlungsfall.get(i)
                                           .get(0);
            if (!DatFunk.Unter18(tag1, geburtstag)) {
                break;
            }
        }
        ret[0] = true;
        ret[1] = i;
        ret[2] = true;
        return ret;
    }

    public static Object[] jahresWechselCheck(Vector<Vector<Object>> behandlungsfall, boolean unter18) {
        // Jahreswechsel ab Position vollständig im alten Jahr
        // unter18 wird hier nicht mehr ausgewertet, als Parameter aber noch belassen
        Object[] ret = { Boolean.FALSE, -1, Boolean.FALSE };
        if (((String) behandlungsfall.get(0)
                                     .get(0)).endsWith(SystemConfig.aktJahr)) {
            return ret;
        }
        for (int i = 0; i < behandlungsfall.size(); i++) {
            if (!((String) behandlungsfall.get(i)
                                          .get(0)).endsWith(SystemConfig.aktJahr)) {
                ret[0] = true;
                ret[2] = true;
            } else {
                ret[0] = true;
                ret[1] = i;
                ret[2] = false;
                break;
            }
        }
        return ret;
    }

    public static void loescheRezAusVolleTabelle(final String reznr) {
        new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                SqlInfo.sqlAusfuehren("delete from volle where rez_nr='" + reznr + "'");
                return null;
            }

        }.execute();
    }

    public static void fuelleVolleTabelle(final String reznr, final String rezbehandler) {
        new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                try {
                    if (SqlInfo.gibtsSchon("select rez_nr from volle where rez_nr ='" + reznr + "' LIMIT 1")) {
                        return null;
                    }
                    Vector<Vector<String>> vec = SqlInfo.holeFelder(
                            "select pat_intern,rez_datum,id from verordn where rez_nr = '" + reznr + "' LIMIT 1");
                    String cmd = "insert into volle set rez_nr='" + reznr + "', " + "pat_intern='" + vec.get(0)
                                                                                                        .get(0)
                            + "', behandler='" + rezbehandler + "', " + "fertigam='"
                            + DatFunk.sDatInSQL(DatFunk.sHeute()) + "', " + "rez_datum='" + vec.get(0)
                                                                                               .get(1)
                            + "', rezid='" + vec.get(0)
                                                .get(2)
                            + "'";
                    SqlInfo.sqlAusfuehren(cmd);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Fehler bei der Ausführung 'fuelleVolleTabelle'");
                }
                return null;
            }
        }.execute();
    }

    public static void RezGebSignal(String rez_num) {
        final String xrez_num = rez_num;
        if (!SystemConfig.RezGebWarnung) {
            return;
        }
        new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                try {
                    if ("2".equals(SqlInfo.holeEinzelFeld( // ZZStat.ZUZAHLNICHTOK.ordinal()
                            "select zzstatus from verordn where rez_nr = '" + xrez_num + "' LIMIT 1"))) {
                        new AePlayWave(Path.Instance.getProghome() + "sounds/" + "doorbell.wav").start();
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

        }.execute();
    }

    /**
     * @param stichtag *********************************************************************************/

    private static int anzBehandeltVorSplit(Vector<String> tage, String neuerPreisAbDatum) {
        int count = 0;
        Enumeration<String> elements = tage.elements();
        System.out.println("behandeltPreSplit Tage: " + tage);
        while(elements.hasMoreElements()){
            long diff = DatFunk.TageDifferenz (neuerPreisAbDatum, elements.nextElement());
            if (diff < 0) {
                count++;
            } else {
                break; // while
            }
        }
        return count;
    }

    public static int anzBehandeltNachSplit(Vector<String> tage, String neuerPreisAbDatum) {
        int anz = tage.size();
        return anz - anzBehandeltVorSplit(tage, neuerPreisAbDatum);
    }

    public static String getZuZahlValueSplit(Rezeptvector currVO) throws Exception {
        // das wird ein ziemlicher Aufriss!
        // bei vorhandenen Terminen und mehreren vorr. HM muss für jeden Termin separat berechnet
        // und zugleich die verbleibende Menge ermittelt werden ...
        // ... und was wird mit den Ausgaben für Quittung und detaillierte RGR?
        // Das würde zusätzl. HM-Einträge bedeuten, mit allem Drum&Dran (Vorlagen ...)
        // erstmal muss korrekte Zuzahlung reichen!
        
        // Hier sollte die komplette Zuzahlung 'rauskommen'.
        String splitVal = null;

        return splitVal;
    }

    public static String getZuZahlValueSplit(Rezeptvector currVO, int idxHmPos) throws Exception {
        String splitVal = null;
        String disziplin = RezTools.getDisziplinFromRezNr(currVO.getRezNb());
        int priceGroup = Integer.parseInt(currVO.getPreisgruppeS()) - 1;
        String rezDatum = DatFunk.sDatInDeutsch(currVO.getRezeptDatum());
        boolean neuerpreis = RezTools.neuePreisNachRezeptdatumOderStichtag(disziplin, priceGroup, rezDatum, false,
                Reha.instance.patpanel.vecaktrez);
        boolean preisRegelIsSplit = (4 == SystemPreislisten.hmNeuePreiseRegel.get(disziplin)
                                                                             .get(priceGroup));
        if (preisRegelIsSplit) {
            Vector<Vector<String>> preisvec = SystemPreislisten.hmPreise.get(disziplin)
                    .get(priceGroup);
            boolean checkok = new HMRCheck2020(currVO, disziplin, preisvec).check(null);    // HM sind korrekt eingetragen?
            if (checkok) {
                String neuerPreisAbDatum = SystemPreislisten.hmNeuePreiseAb.get(disziplin)
                                                                           .get(priceGroup);
                String dummy = currVO.getTermine();
                // 20.04.2021@Fr. Günther@@21201,21501@2021-04-20
                // 27.04.2021@Fr. Günther@@21201,21501@2021-04-27
                // 18.05.2021@Fr. Günther@@21201,21501@2021-05-18
                Vector<String> tage = RezTools.holeEinzelTermineAusRezept(null, currVO.getTermine());
                // [20.04.2021, 27.04.2021, 18.05.2021]
                Vector<ArrayList<?>> termine = RezTools.holePosUndAnzahlAusTerminen(currVO.getRezNb());
                // [[21201, 21501], [3, 3], [true, false], [true, false], [[Ljava.lang.Object;@1094f08, [Ljava.lang.Object;@1dec8f0]]   <- eher nicht (wie stichtag handhaben?)
                // Arrays innerhalb dem Vector termine
                // termine.get(0) = Positionen (String)
                // termine.get(1) = Anzahlen (Integer)
                // termine.get(2) = Vorrangiges Heilmittel (Boolean)
                // termine.get(3) = als Standalone ergänzendes Heilmittel erlaubt (Boolean)
                // termine.get(4) = Object[]
                // {doppelbehandlung(Boolean),erstepos(Integer),letztepos(Integer)}

                ListIterator<String> days = tage.listIterator();
                while (days.hasNext()) {
                    String tag = days.next();
                    long diff = DatFunk.TageDifferenz (neuerPreisAbDatum, tag);
                    System.out.println(tag + ": " + diff);
                    // liste mit Preisen für Behandlungen aus Terminliste machen
                    // ergänzen mit akt. Preisen für fehlende Behandlungen
                }
            } else {
                throw new Exception("HMRCheck2020 failed");
            }
        }
        
        return splitVal;
    }

    /***********************************************************************************/

    public static Object[] BehandlungenAnalysieren(String swreznum, boolean doppeltOk, boolean xforceDlg,
            boolean alletermine, Vector<String> vecx, Point pt, String xkollege, String datum) {
        int i, j, count = 0;
        boolean dlgZeigen = false; // unterdrückt die Anzeige des TeminBestätigenAuswahlFensters
        boolean jetztVoll = false;
        boolean nochOffenGleich = true;

        Vector<BestaetigungsDaten> hMPos = new Vector<>();
        hMPos.add(new BestaetigungsDaten(false, "./.", 0, 0, false, false));
        hMPos.add(new BestaetigungsDaten(false, "./.", 0, 0, false, false));
        hMPos.add(new BestaetigungsDaten(false, "./.", 0, 0, false, false));
        hMPos.add(new BestaetigungsDaten(false, "./.", 0, 0, false, false));
        Vector<String> vec = null;

        StringBuffer termbuf = new StringBuffer();

        Object[] retObj = { null, null, null };
        try {
            // die anzahlen 1-4 werden jetzt zusammenhängend ab index 11 abgerufen
            if (vecx != null) {
                vec = vecx;
            } else {
                vec = SqlInfo.holeSatz("verordn",
                        "termine,pos1,pos2,pos3,pos4,hausbes,unter18,jahrfrei,pat_intern,preisgruppe,zzregel,anzahl1,anzahl2,anzahl3,anzahl4,preisgruppe",  
                        "rez_nr='" + swreznum + "'", Arrays.asList(new String[] {}));
            }
            if (!vec.isEmpty()) {
                termbuf = new StringBuffer();
                if (alletermine) {
                    termbuf.append(vec.get(0));
                }

                if (vec.get(0).isEmpty()) {
                    checkIfStartIsTooLate(swreznum, datum);
                }

                Vector<ArrayList<?>> termine = holePosUndAnzahlAusTerminen(swreznum);
                // System.out.println(termine);
                if (termine.isEmpty()) {
                    return null;
                }
                // Arrays innerhalb dem Vector termine
                // termine.get(0) = Positionen (String)
                // termine.get(1) = Anzahlen (Integer)
                // termine.get(2) = Vorrangiges Heilmittel (Boolean)
                // termine.get(3) = als Standalone ergänzendes Heilmittel erlaubt (Boolean)
                // termine.get(4) = Object[]
                // {doppelbehandlung(Boolean),erstepos(Integer),letztepos(Integer)}

                int termineIdx = 0;
                for (i = 0; i <= 3; i++) {  // pos1..pos4
                    if ("".equals(vec.get(1 + i)
                            .trim())) {
                        hMPos.get(i).hMPosNr = "./.";
                        hMPos.get(i).vOMenge = 0;
                        hMPos.get(i).anzBBT = 0;
                    } else {
                        hMPos.get(i).hMPosNr = String.valueOf(vec.get(1 + i));
                        hMPos.get(i).vOMenge = Integer.parseInt(vec.get(i + 11));
                        hMPos.get(i).vorrangig = (Boolean) ((ArrayList<?>) ((Vector<?>) termine).get(2)).get(termineIdx);
                        hMPos.get(i).invOBelegt = true;
                        hMPos.get(i).anzBBT = (Integer) ((ArrayList<?>) ((Vector<?>) termine).get(1)).get(termineIdx);
                        termineIdx++;
                    }
                    hMPos.get(i)
                         .gehtNochEiner();
                }
                // Jetzt alle Objekte die unbelegt sind löschen
                for (i = 3; i >= 0; i--) {
                    if (!hMPos.get(i).invOBelegt) {
                        hMPos.remove(i);
                    }
                }
                hMPos.trimToSize();
                // Die Variable j erhält jetzt den Wert der Anzahl der verbliebenen Objekte
                j = hMPos.size();

                // prüfen welche Behandlungsformen 'noch einen vertragen können'
                int hMmitOffenenBehandlungen = 0;
                int anzOffeneVorrHM = 0;
                int ianzahl = hMPos.get(0).vOMenge;
                int ioffen = hMPos.get(0).vORestMenge;
                for (i = 0; i < j; i++) {
                    BestaetigungsDaten currHmPos = hMPos.get(i);
                    currHmPos.danachVoll();
                    // wenn eine Behandlung noch frei ist 
                    if (currHmPos.einerOk ) {
                        hMmitOffenenBehandlungen++;
                        if (currHmPos.vorrangig) {
                            anzOffeneVorrHM++;
                        }
                    }
                    if (ioffen != currHmPos.vORestMenge) {
                        nochOffenGleich = false;
                    }
                }
                // Keine Postition mehr frei
                if (hMmitOffenenBehandlungen == 0) {
                    retObj[0] = String.valueOf(termbuf.toString());
                    retObj[1] = Integer.valueOf(RezTools.REZEPT_IST_BEREITS_VOLL);
                    // if(debug){System.out.println("Rezept war bereits voll");}
                    return retObj;
                }

                // Nur wenn nach HMR-geprüft werden muß
                boolean hmrtest = checkIsGKV(swreznum);
                if (hmrtest) {
                    // 1. erst Prüfen ob das Rezept bereits voll ist
                    for (i = 0; i < j; i++) {
                        if (!hMPos.get(i).einerOk && hMPos.get(i).vorrangig) {
                            // ein vorrangiges Heilmittel ist voll
                            // testen ob es sich um eine Doppelposition dreht (ermittelt in holePosUndAnzahlAusTerminen())
                            if (((Object[]) ((ArrayList<?>) ((Vector<?>) termine).get(4)).get(i))[0] == Boolean.valueOf(
                                    true)) {
                                // testen ob es die 1-te Pos der Doppelbehandlung ist
                                if ((Integer) ((Object[]) ((ArrayList<?>) ((Vector<?>) termine).get(4)).get(
                                        i))[1] < (Integer) ((Object[]) ((ArrayList<?>) ((Vector<?>) termine).get(
                                                4)).get(i))[2]) {
                                    // Es ist die 1-te Position die voll ist also Ende-Gelände
                                    retObj[0] = String.valueOf(termbuf.toString());
                                    retObj[1] = Integer.valueOf(REZEPT_IST_BEREITS_VOLL);
                                    // if(debug){System.out.println("erste Position = voll + Doppelbehandlung");}
                                    // if(debug){System.out.println(hMPos.get(i).hMPosNr+"-"+hMPos.get(i).vOMenge+"-"+hMPos.get(i).anzBBT);}
                                    return retObj;
                                }
                            } else {
                                // nein keine Doppelposition
                                boolean isHMR2021 = checkIsHMR2021 (swreznum);
                                if ((isHMR2021) && (anzOffeneVorrHM > 0)) {
                                    // HMR2020: kann noch weiter gehen (bis 3 vorr. HM sind möglich)
                                } else {
                                    // also Ende-Gelände      
                                    retObj[0] = String.valueOf(termbuf.toString());
                                    retObj[1] = Integer.valueOf(RezTools.REZEPT_IST_BEREITS_VOLL);
                                    // if(debug){System.out.println("erste Position = voll und keine Doppelbehandlung");}
                                    // if(debug){System.out.println(hMPos.get(i).hMPosNr+"-"+hMPos.get(i).vOMenge+"-"+hMPos.get(i).anzBBT);}
                                    return retObj;
                                }
                            }
                        } else if (!hMPos.get(i).einerOk && !hMPos.get(i).vorrangig && j == 1) {
                            // Falls eines der wenigen ergänzenden Heilmittel solo verordnet wurde
                            // z.B. Ultraschall oder Elektrotherapie
                            retObj[0] = String.valueOf(termbuf.toString());
                            retObj[1] = Integer.valueOf(REZEPT_IST_BEREITS_VOLL);
                            // if(debug){System.out.println("es geht kein zusätzlicher");}
                            // if(debug){System.out.println(hMPos.get(i).hMPosNr+"-"+hMPos.get(i).vOMenge+"-"+hMPos.get(i).anzBBT);}
                            return retObj;
                        } else if (!hMPos.get(i).vorrangig && j == 1
                                && (Boolean) ((ArrayList<?>) ((Vector<?>) termine).get(2)).get(i)) {
                            // Ein ergänzendes Heilmittel wurde separat verordent das nicht zulässig ist
                            // könnte man auswerten, dann verbaut man sich aber die Möglichkeit
                            // bei PrivatPat. abzurechnen was geht....
                            // if(debug){System.out.println("unerlaubtes Ergänzendes Heilmittel solo
                            // verordnet");}
                            // if(debug){System.out.println(hMPos.get(i).hMPosNr+"-"+hMPos.get(i).vOMenge+"-"+hMPos.get(i).anzBBT);}
                        }
                        // if(debug){System.out.println("Position kann bestätigt werden");}
                        // if(debug){System.out.println(hMPos.get(i).hMPosNr+"-"+hMPos.get(i).vOMenge+"-"+hMPos.get(i).anzBBT);}
                    }
                    // Ende nur wenn Tarifgruppe HMR-Gruppe ist
                }

                // Nur Wenn mehrere Behandlungen im Rezept vermerkt
                boolean mustSelect = false;
                if (j > 1) {
                    // Wenn mehrere noch offen sind aber ungleiche noch Offen
                    if ((hMmitOffenenBehandlungen > 1) && (!nochOffenGleich)) {
                        dlgZeigen = true;
                    }
                    if (anzOffeneVorrHM > 1) { // HMR2020: vorr. HM einzeln behandeln!
                        mustSelect = true;
                    }
                    if (isPodoL60(swreznum) ) {
                        // erg. HM ist optional 
                        mustSelect = true;
                    }
                }
                // 3. Dann Dialog zeigen
                // TerminBestätigenAuswahlFenster anzeigen oder überspringen
                // Evtl. noch Einbauen ob bei unterschiedlichen Anzahlen
                // (System-Initialisierung) immer geöffnet wird.
                if (xforceDlg || mustSelect || (dlgZeigen && (Boolean) SystemConfig.hmTerminBestaetigen.get("dlgzeigen"))) {

                    TerminBestaetigenAuswahlFenster termBestAusw = new TerminBestaetigenAuswahlFenster(
                            Reha.getThisFrame(), null, hMPos, swreznum, Integer.parseInt(vec.get(15)));
                    termBestAusw.pack();
                    if (pt != null) {
                        termBestAusw.setLocation(pt);
                    } else {
                        termBestAusw.setLocationRelativeTo(null);
                    }
                    termBestAusw.setVisible(true);
                    if (DIALOG_WERT == DIALOG_ABBRUCH) {
                        retObj[0] = String.valueOf(termbuf.toString());
                        retObj[1] = Integer.valueOf(REZEPT_ABBRUCH);
                        return retObj;
                    }
                    for (i = 0; i < j; i++) { // ToDo HMR2020: prüfen, ob nur 1 vorr. HM gewählt (oder Doppelbehandlung)
                        BestaetigungsDaten currHmPos = hMPos.get(i);
                        if (currHmPos.best) {
                            currHmPos.anzBBT += 1;
                            // gleichzeitig prüfen ob voll
                            if (currHmPos.jetztVoll() && currHmPos.vorrangig) {
                                if (--anzOffeneVorrHM == 0) {
                                    jetztVoll = true;                                    
                                }
                            } else if (currHmPos.jetztVoll() && (!currHmPos.vorrangig) && j == 1) {
                                jetztVoll = true;
                            }
                        }
                    }
                } else {
                    /*
                     * Der Nutzer wünscht kein Auswahlfenster: bestätige alle noch offenen
                     * Heilmittel
                     */
                    for (i = 0; i < j; i++) {
                        BestaetigungsDaten currHmPos = hMPos.get(i);
                        currHmPos.best = Boolean.valueOf(currHmPos.einerOk);
                        if (currHmPos.einerOk) {
                            currHmPos.anzBBT += 1;
                            currHmPos.best = true;
                            if (currHmPos.jetztVoll() && currHmPos.vorrangig) {
                                jetztVoll = true;
                            } else if (currHmPos.jetztVoll() && (!currHmPos.vorrangig) && j == 1) {
                                jetztVoll = true;
                            }
                        }
                    }
                }
                String[] params = { null, null, null, null };
                count = 0;
                for (i = 0; i < j; i++) {
                    BestaetigungsDaten currHmPos = hMPos.get(i);
                    if (currHmPos.best) {
                        params[i] = currHmPos.hMPosNr;
                        count++;
                    }
                }
                if (count == 0) {
                    jetztVoll = true;
                }
                termbuf.append(TermineErfassen.macheNeuTermin2(params[0] != null ? params[0] : "",
                        params[1] != null ? params[1] : "", params[2] != null ? params[2] : "",
                        params[3] != null ? params[3] : "", xkollege, datum));

                retObj[0] = String.valueOf(termbuf.toString());
                retObj[1] = jetztVoll ? Integer.valueOf(REZEPT_IST_JETZ_VOLL) : Integer.valueOf(REZEPT_HAT_LUFT);
            }
            return retObj;
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            hMPos = null;
        }
        return retObj;
    }

    private static boolean isPodoL60(String reznum) {
        if (isPodo(reznum) ) {
            Vector<Vector<String>> vec1 = SqlInfo.holeFelder(
                    "select ICD10,ICD10_2 from verordn where rez_nr = '" + reznum + "' LIMIT 1");
            String icd10_1 = vec1.get(0).get(0);
            String icd10_2 = vec1.get(0).get(1);
            if ("L60.0".equals(icd10_1) || "L60.0".equals(icd10_2)) {
                // erg. HM ist optional 
                return true;
            }
        }
        return false;
    }

    public static boolean checkIsGKV(String rezNum) {
        boolean isGKV = true;
        String disziplin = getDisziplinFromRezNr(rezNum);
        String pGrpS = SqlInfo.holeEinzelFeld("select preisgruppe from verordn where rez_nr='" + rezNum + "' LIMIT 1");
        int pGrp = Integer.parseInt(pGrpS);
        isGKV = SystemPreislisten.hmHMRAbrechnung.get(disziplin)
                                                 .get(pGrp - 1) == 1;

        return isGKV;
    }

    public static boolean checkIsHMR2021(String rezNum) {
        boolean isHMR2021 = true;
        String test = SqlInfo.holeEinzelFeld("select hmr2021 from verordn where rez_nr='" + rezNum + "' LIMIT 1");
        if ("F".equals(test)) {
            isHMR2021 = false;
        }
        return isHMR2021;
    }

    private static int welcheIstMaxInt(int i1, int i2) {
        if (i1 > i2) {
            return 1;
        }
        if (i1 == i2) {
            return 0;
        }
        return 2;
    }

    /**
     * Pruefen, ob Rezept dringend abgerechnet werden sollte (Frist: 9 Monate
     * nach letzter Behandlung).
     */
    public static boolean isLate(String thisRezNr) {
        int tageBisWarnung = 220;
        String cmd = "select termine from verordn where rez_nr='" + thisRezNr + "' LIMIT 1";
        String termineDB = SqlInfo.holeEinzelFeld(cmd);
        String[] behDat = termineDB.split("\n");
        String letzteBeh = behDat[behDat.length - 1].split("@")[0];
        return DatFunk.TageDifferenz(letzteBeh, DatFunk.sHeute()) > tageBisWarnung;
    }

    /**
     * Pruefen, ob erste Behandlung innerhalb des Zeitraumes für spätesten
     * Behandlungsbeginn erfolgt
     */
    private static void checkIfStartIsTooLate(String swreznum, String datum) {
        String spaetBeginn = SqlInfo.holeEinzelFeld("select lastdate from verordn where rez_nr='" + swreznum + "' LIMIT 1");
        spaetBeginn = DatFunk.sDatInDeutsch(spaetBeginn);
        long diff = DatFunk.TageDifferenz(spaetBeginn, datum);
        if (diff > 0) {
            JOptionPane.showMessageDialog(null, "Spätester Behandlungsbeginn ist der " + spaetBeginn, "Warnung", WARNING_MESSAGE);
        }
    }

    /**
     * Pruefen, ob Gültigkeit der VO abzulaufen droht
     */
    public static void warnVoLaeuftAb(String rezNum, FristenRegelung ablaufTag) {
        long diff = DatFunk.TageDifferenz(DatFunk.sHeute(),ablaufTag.getVerfallDatum());
        
        JOptionPane.showMessageDialog(null,
                "<html>Verordnung <b>" + rezNum + (diff > 0 ? "</b> wird" : "</b> wurde") + " zum <b>"
                        + ablaufTag.getVerfallDatum() + "</b> ungültig.\n"
                        + "(" + ablaufTag.getVerfallGrund() + ")\n"
                        + "Bis dahin nicht erbrachte Behandlungen verfallen.",
                "Warnung", WARNING_MESSAGE);
    }
    
    public static void checkVoLaeuftAb(Rezeptvector rezept) {
        String rezNum = rezept.getRezNb();
        boolean hmrtest = checkIsGKV(rezNum);
        FristenRegelung ablaufTag = new FristenRegelung(rezept);
        int warnTageVorUltimo = 14;
        if (hmrtest) { // + nur wenn 'noch nicht voll'!
            String alleTermine = rezept.getTermine();

            if (isPhysio(rezNum) || isMassage(rezNum) || isErgo(rezNum) || isLogo(rezNum)) {
                warnTageVorUltimo = 14;  // 2 Wochen vor Ablauf warnen
            }
            if (isPodo(rezNum)) {
                warnTageVorUltimo = 28;  // 4 Wochen vor Ablauf warnen
            }
            
            long diff = DatFunk.TageDifferenz(DatFunk.sHeute(),ablaufTag.getVerfallDatum());
            if (diff <= warnTageVorUltimo) {
                warnVoLaeuftAb(rezNum, ablaufTag);
            }
        }
    }

    public static void checkVoLaeuftAb(String rezNum) {
      Rezeptvector myRezept = new Rezeptvector();
      myRezept.init(rezNum);
      checkVoLaeuftAb(myRezept);
    }


    public static boolean hasTermineNachAblauf (Rezeptvector rezept) {
        String rezNum = rezept.getRezNb();
        boolean hmrtest = checkIsGKV(rezNum);
        FristenRegelung ablaufTag = new FristenRegelung(rezept);
        if (hmrtest) {
            String alleTermine = rezept.getTermine();
            String letzterTermin = holeLetztenTermin(rezNum, alleTermine);
            long diff = DatFunk.TageDifferenz(letzterTermin,ablaufTag.getVerfallDatum());
            if (diff < 0) {
                warnVoLaeuftAb(rezNum, ablaufTag);
                return true;
            }
        }
        return false;
    }

    public static void updateParentVO(String rezNb) {
        int anzChildren = 0;
        String parent = rezNb.substring(0, rezNb.indexOf("-"));
        System.out.println("rez: " + rezNb + "    parent: " + parent);
        if(!rezNb.equals(parent)) {
            Rezeptvector myRezept = new Rezeptvector();
            myRezept.init(parent);

            try {
                Vector<Vector<String>> vec = SqlInfo.holeFelder(
                        "select rez_nr from verordn where rez_nr like '" + parent + "-%'");
                anzChildren = vec.size();
            } finally {}
            if (anzChildren != myRezept.getNbOfChildren()) {
                myRezept.setNbOfChildren(anzChildren);
                if (anzChildren == 0) {
                    myRezept.setHasChildren(false);
                }
                myRezept.writeRez2DB();
            }
        }
    }
        
    public static void warnRezGebNotPossible() {
        String meldung;
        meldung = "Für Nagelspangenbehandlung ist die korrekte Berechnung des Eigenanteils\n"
                + "erst nach Abschluß des Rezeptes im Abrechnungsfenster möglich.";
        String keep = (String) UIManager.get("OptionPane.okButtonText");
        UIManager.put("OptionPane.okButtonText", "Abbruch");
        JOptionPane.showMessageDialog(null, meldung,null, JOptionPane.ERROR_MESSAGE);
        UIManager.put("OptionPane.okButtonText", keep);
    }
    
    public static boolean isPodoNagelspange(Rezeptvector currVO) {
        if (isPodo(currVO.getRezNb()) && "L60.0".equals(currVO.getICD10())) {
            warnRezGebNotPossible();
            return true;
        }
        return false;
    }

}
class ZuzahlModell {
    public int gesamtZahl;
    public boolean allefrei;
    public boolean allezuzahl;
    public boolean anfangfrei;
    public int teil1;
    public int teil2;
    public int preisgruppe;
    public boolean hausbesuch;
    boolean hbvoll;
    boolean hbheim;
    int km;

    public ZuzahlModell() {
    }
}
