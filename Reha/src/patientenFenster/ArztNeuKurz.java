package patientenFenster;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.jdesktop.swingx.JXPanel;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import CommonTools.JCompTools;
import CommonTools.JRtaComboBox;
import CommonTools.JRtaTextField;
import CommonTools.SqlInfo;
import arztFenster.CheckDocDouble;
import commonData.ArztVec;
import systemEinstellungen.SystemConfig;

public class ArztNeuKurz extends JXPanel implements ActionListener, KeyListener, FocusListener {
    /**
     *
     */
    private static final long serialVersionUID = -8412450784730057738L;
    public JRtaTextField tfs[] = { null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null };
    private static final int IDX_TF_ANREDE = 0;
    private static final int IDX_TF_TITEL = 1;
    private static final int IDX_TF_NACHNAME = 2;
    private static final int IDX_TF_VORNAME = 3;
    private static final int IDX_TF_STRASSE = 4;
    private static final int IDX_TF_PLZ = 5;
    private static final int IDX_TF_ORT = 6;
    private static final int IDX_TF_LANR = 7;
    private static final int IDX_TF_BSNR = 8;
    private static final int IDX_TF_TEL = 9;
    private static final int IDX_TF_FAX = 10;
    private static final int IDX_TF_EMAIL1 = 11;
    private static final int IDX_TF_KLINIK = 12;
    private static final int IDX_TF_FACHARZT = 13;
    private static final int IDX_TF_ID = 14;

    public JButton speichern;
    public JButton abbrechen;
    public ArztAuswahl eltern;
    public JScrollPane jscr;
    public JRtaComboBox arztgruppe;

    public ArztNeuKurz(ArztAuswahl eltern) {
        super();
        this.eltern = eltern;
        setOpaque(false);
        setLayout(new BorderLayout());
        add(getFelderPanel(), BorderLayout.CENTER);
        add(getButtonPanel(), BorderLayout.SOUTH);
        validate();
    }

    public JScrollPane getFelderPanel() {
        FormLayout lay = new FormLayout(
                // 1                  2     3     4                   5     6
                "right:max(60dlu;p), 4dlu, 60dlu,right:max(60dlu;p), 4dlu, 60dlu",
                // 1  2  3   4  5   6  7   8  9  10 11  12 13  14 15  16  17   18  19  20 21  22 23   24  25   26  27  28 29 30  31
                "3dlu,p,2dlu,p,2dlu,p,2dlu,p,2dlu,p,5dlu,p,5dlu,p,2dlu,p, 5dlu, p, 5dlu,p,2dlu,p,2dlu, p, 5dlu, p, 5dlu,p,2dlu,p,10dlu");
        PanelBuilder jpan = new PanelBuilder(lay);
        jpan.setDefaultDialogBorder();
        jpan.getPanel()
            .setOpaque(false);
        jpan.getPanel()
            .addKeyListener(this);
        CellConstraints cc = new CellConstraints();

        jpan.add(new JLabel("Anrede"), cc.xy(1, 2));
        tfs[IDX_TF_ANREDE] = new JRtaTextField("NORMAL", true);
        tfs[IDX_TF_ANREDE].addKeyListener(this);
        tfs[IDX_TF_ANREDE].addFocusListener(this);
        tfs[IDX_TF_ANREDE].setName("anrede");
        jpan.add(tfs[IDX_TF_ANREDE], cc.xy(3, 2));

        jpan.add(new JLabel("Titel"), cc.xy(4, 2));
        tfs[IDX_TF_TITEL] = new JRtaTextField("NORMAL", true);
        tfs[IDX_TF_TITEL].addKeyListener(this);
        tfs[IDX_TF_TITEL].addFocusListener(this);
        tfs[IDX_TF_TITEL].setName("titel");
        jpan.add(tfs[IDX_TF_TITEL], cc.xy(6, 2));

        jpan.add(new JLabel("Nachname"), cc.xy(1, 4));
        tfs[IDX_TF_NACHNAME] = new JRtaTextField("NORMAL", true);
        tfs[IDX_TF_NACHNAME].addKeyListener(this);
        tfs[IDX_TF_NACHNAME].addFocusListener(this);
        tfs[IDX_TF_NACHNAME].setName("nachname");
        jpan.add(tfs[IDX_TF_NACHNAME], cc.xyw(3, 4, 4));

        jpan.add(new JLabel("Vorname"), cc.xy(1, 6));
        tfs[IDX_TF_VORNAME] = new JRtaTextField("NORMAL", true);
        tfs[IDX_TF_VORNAME].addKeyListener(this);
        tfs[IDX_TF_VORNAME].addFocusListener(this);
        tfs[IDX_TF_VORNAME].setName("vorname");
        jpan.add(tfs[IDX_TF_VORNAME], cc.xyw(3, 6, 4));

        jpan.add(new JLabel("Strasse"), cc.xy(1, 8));
        tfs[IDX_TF_STRASSE] = new JRtaTextField("NORMAL", true);
        tfs[IDX_TF_STRASSE].addKeyListener(this);
        tfs[IDX_TF_STRASSE].addFocusListener(this);
        tfs[IDX_TF_STRASSE].setName("strasse");
        jpan.add(tfs[IDX_TF_STRASSE], cc.xyw(3, 8, 4));

        jpan.add(new JLabel("Plz/Ort"), cc.xy(1, 10));
        tfs[IDX_TF_PLZ] = new JRtaTextField("ZAHLEN", true);
        tfs[IDX_TF_PLZ].addKeyListener(this);
        tfs[IDX_TF_PLZ].addFocusListener(this);
        tfs[IDX_TF_PLZ].setName("plz");
        jpan.add(tfs[IDX_TF_PLZ], cc.xy(3, 10));

        tfs[IDX_TF_ORT] = new JRtaTextField("NORMAL", true);
        tfs[IDX_TF_ORT].addKeyListener(this);
        tfs[IDX_TF_ORT].addFocusListener(this);
        tfs[IDX_TF_ORT].setName("ort");
        jpan.add(tfs[IDX_TF_ORT], cc.xyw(4, 10, 3));

        jpan.addSeparator("Arztidentifikation", cc.xyw(1, 12, 6));

        jpan.add(new JLabel("LANR"), cc.xy(1, 14));
        tfs[IDX_TF_LANR] = new JRtaTextField("ZAHLEN", true);
        tfs[IDX_TF_LANR].addKeyListener(this);
        tfs[IDX_TF_LANR].addFocusListener(this);
        tfs[IDX_TF_LANR].setName("arztnum");
        jpan.add(tfs[IDX_TF_LANR], cc.xyw(3, 14, 4));

        jpan.add(new JLabel("Betriebsstätte"), cc.xy(1, 16));
        tfs[IDX_TF_BSNR] = new JRtaTextField("ZAHLEN", true);
        tfs[IDX_TF_BSNR].addKeyListener(this);
        tfs[IDX_TF_BSNR].addFocusListener(this);
        tfs[IDX_TF_BSNR].setName("bsnr");
        jpan.add(tfs[IDX_TF_BSNR], cc.xyw(3, 16, 4));

        jpan.addSeparator("Kontakt", cc.xyw(1, 18, 6));

        jpan.add(new JLabel("Telefon"), cc.xy(1, 20));
        tfs[IDX_TF_TEL] = new JRtaTextField("NORMAL", true);
        tfs[IDX_TF_TEL].addKeyListener(this);
        tfs[IDX_TF_TEL].addFocusListener(this);
        tfs[IDX_TF_TEL].setName("telefon");
        jpan.add(tfs[IDX_TF_TEL], cc.xyw(3, 20, 4));

        jpan.add(new JLabel("Telefax"), cc.xy(1, 22));
        tfs[IDX_TF_FAX] = new JRtaTextField("NORMAL", true);
        tfs[IDX_TF_FAX].addKeyListener(this);
        tfs[IDX_TF_FAX].addFocusListener(this);
        tfs[IDX_TF_FAX].setName("fax");
        jpan.add(tfs[IDX_TF_FAX], cc.xyw(3, 22, 4));

        jpan.add(new JLabel("Email"), cc.xy(1, 24));
        tfs[IDX_TF_EMAIL1] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_EMAIL1].addKeyListener(this);
        tfs[IDX_TF_EMAIL1].addFocusListener(this);
        tfs[IDX_TF_EMAIL1].setName("email1");
        jpan.add(tfs[IDX_TF_EMAIL1], cc.xyw(3, 24, 4));

        jpan.addSeparator("Zusätze", cc.xyw(1, 26, 6));

        jpan.add(new JLabel("Facharzt"), cc.xy(1, 28));
        arztgruppe = new JRtaComboBox(SystemConfig.arztGruppen);
        arztgruppe.addFocusListener(this);
        jpan.add(arztgruppe, cc.xyw(3, 28, 4));

        jpan.add(new JLabel("Klinik"), cc.xy(1, 30));
        tfs[IDX_TF_KLINIK] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_KLINIK].addKeyListener(this);
        tfs[IDX_TF_KLINIK].addFocusListener(this);
        tfs[IDX_TF_KLINIK].setName("klinik");
        jpan.add(tfs[IDX_TF_KLINIK], cc.xyw(3, 30, 4));

        tfs[IDX_TF_FACHARZT] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_FACHARZT].setName("facharzt");
        tfs[IDX_TF_ID] = new JRtaTextField("NIX", true);
        tfs[IDX_TF_ID].setName("id");

        jpan.getPanel()
            .validate();
        jscr = JCompTools.getTransparentScrollPane(jpan.getPanel());
        jscr.getVerticalScrollBar()
            .setUnitIncrement(15);
        jscr.validate();
        return jscr;
    }

    /**************************************************/
    public JXPanel getButtonPanel() {
        JXPanel jpan = JCompTools.getEmptyJXPanel();
        jpan.addKeyListener(this);
        jpan.setOpaque(false);
        FormLayout lay = new FormLayout(
                // 1 2 3 4 5
                "fill:0:grow(0.33),50dlu,fill:0:grow(0.33),50dlu,fill:0:grow(0.33)",
                // 1 2 3
                "5dlu,p,5dlu");
        CellConstraints cc = new CellConstraints();
        jpan.setLayout(lay);
        speichern = new JButton("speichern");
        speichern.setActionCommand("speichern");
        speichern.addActionListener(this);
        speichern.addKeyListener(this);
        speichern.setMnemonic(KeyEvent.VK_S);
        jpan.add(speichern, cc.xy(2, 2));

        abbrechen = new JButton("abbrechen");
        abbrechen.setActionCommand("abbrechen");
        abbrechen.addActionListener(this);
        abbrechen.addKeyListener(this);
        abbrechen.setMnemonic(KeyEvent.VK_A);
        jpan.add(abbrechen, cc.xy(4, 2));

        return jpan;
    }

    public JXPanel getNeuKurzPanel() {
        return this;
    }

    public void setzteFocus() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                tfs[IDX_TF_ANREDE].requestFocus();
            }
        });
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {

        String comm = arg0.getActionCommand();
        if (comm.equals("speichern")) {
            panelWechsel(true);
        }
        if (comm.equals("abbrechen")) {
            panelWechsel(false);
        }

    }

    public void allesAufNull() {
        for (int i = 0; i < 15; i++) {
            tfs[i].setText("");
        }
    }

    public void panelWechsel(boolean uebernahme) {
        if (uebernahme) {
            String lanr = tfs[IDX_TF_LANR].getText()
                                          .trim();
            CheckDocDouble chkDbl = null;
            if (!"".equals(lanr)) {
                System.out.println("LANR " + lanr);

                chkDbl = new CheckDocDouble(lanr);
                } else {
                String nName = tfs[IDX_TF_NACHNAME].getText()
                                                   .trim();
                String vName = tfs[IDX_TF_VORNAME].getText()
                                                  .trim();
                chkDbl = new CheckDocDouble(nName, vName);
                }

                if (chkDbl.updateDouble()) {
                    // tfs neu belegen
                    ArztVec myArzt =  new ArztVec();
                    myArzt.init(chkDbl.getArztId());
                    tfs[IDX_TF_ANREDE].setText(myArzt.getAnrd());
                    tfs[IDX_TF_TITEL].setText(myArzt.getTitel());
                    tfs[IDX_TF_NACHNAME].setText(myArzt.getNName());
                    tfs[IDX_TF_VORNAME].setText(myArzt.getVName());
                    tfs[IDX_TF_STRASSE].setText(myArzt.getStr());
                    tfs[IDX_TF_PLZ].setText(myArzt.getPlz());
                    tfs[IDX_TF_ORT].setText(myArzt.getOrt());
                    tfs[IDX_TF_LANR].setText(myArzt.getLANR());
                    tfs[IDX_TF_BSNR].setText(myArzt.getBSNR());
                    tfs[IDX_TF_TEL].setText(myArzt.getTel());
                    tfs[IDX_TF_FAX].setText(myArzt.getFax());
                    tfs[IDX_TF_EMAIL1].setText(myArzt.getEMail());
                    tfs[IDX_TF_KLINIK].setText(myArzt.getKlinik());
                    tfs[IDX_TF_FACHARZT].setText(myArzt.getFach());
                    tfs[IDX_TF_ID].setText(myArzt.getIdS());
                    eltern.zurueckZurTabelle(tfs);
                } else {
                    // neuen Eintrag anlegen
                    new SwingWorker<Void, Void>() {

                        @Override
                        protected Void doInBackground() throws Exception {
                            if (tfs[IDX_TF_NACHNAME].getText()
                                                    .trim()
                                                    .equals("")) {
                                JOptionPane.showMessageDialog(null,
                                        "Also der Name des Arztes sollte wenigstens angegeben werden!");
                                return null;
                            }
                            int iid;
                            tfs[IDX_TF_FACHARZT].setText((String) arztgruppe.getSelectedItem());
                            tfs[IDX_TF_ID].setText(Integer.toString(iid = SqlInfo.holeId("arzt", "nachname")));
                            String stmt = "update arzt set ";
                            for (int i = 0; i < 14; i++) {
                                stmt = stmt + (i == 0 ? "" : ", ") + tfs[i].getName() + "='" + tfs[i].getText() + "'";
                            }
                            stmt = stmt + " where id ='" + Integer.toString(iid) + "'";
                            // System.out.println(stmt);
                            SqlInfo.sqlAusfuehren(stmt);
                            // new ExUndHop().setzeStatement(stmt);
                            eltern.zurueckZurTabelle(tfs);
                            return null;
                        }
                    }.execute();
                }
        } else {
            eltern.zurueckZurTabelle(null);
        }

    }

    @Override
    public void keyPressed(KeyEvent arg0) {

        if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
            arg0.consume();
            panelWechsel(false);
            eltern.zurueckZurTabelle(null);
            return;
        }
        try {
            if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
                arg0.consume();

                if (((JComponent) arg0.getSource()).getName()
                                                   .equals("speichern")) {
                    arg0.consume();
                    panelWechsel(true);
                } else if (((JComponent) arg0.getSource()).getName()
                                                          .equals("abbrechen")) {
                    arg0.consume();
                    panelWechsel(true);
                }
            }
        } catch (java.lang.NullPointerException ex) {
            arg0.consume();
        }

    }

    @Override
    public void keyReleased(KeyEvent arg0) {

        if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
            arg0.consume();
        }
    }

    @Override
    public void keyTyped(KeyEvent arg0) {

        if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
            arg0.consume();
        }
    }

    @Override
    public void focusGained(FocusEvent arg0) {

        Rectangle rec1 = ((JComponent) arg0.getSource()).getBounds();
        Rectangle rec2 = jscr.getViewport()
                             .getViewRect();
        JViewport vp = jscr.getViewport();
        // Rectangle rec3 = vp.getVisibleRect();
        if ((rec1.y + ((JComponent) arg0.getSource()).getHeight()) > (rec2.y + rec2.height)) {
            vp.setViewPosition(new Point(0, (rec2.y + rec2.height) - rec1.height));
        }
        if (rec1.y < (rec2.y)) {
            vp.setViewPosition(new Point(0, rec1.y));
        }

    }

    @Override
    public void focusLost(FocusEvent arg0) {

    }

}
