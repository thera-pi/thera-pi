package patientenFenster;

import java.awt.BorderLayout;
import java.awt.LinearGradientPaint;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import org.jdesktop.swingx.JXDialog;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.MattePainter;
import org.thera_pi.nebraska.gui.utils.StringTools;
import org.therapi.reha.patient.AktuelleRezepte;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import CommonTools.JCompTools;
import CommonTools.JRtaCheckBox;
import CommonTools.JRtaComboBox;
import CommonTools.JRtaTextField;
import CommonTools.SqlInfo;
import commonData.Rezeptvector;
import environment.Path;
import events.RehaTPEvent;
import events.RehaTPEventClass;
import events.RehaTPEventListener;
import hauptFenster.Reha;
import stammDatenTools.RezTools;
import systemEinstellungen.SystemPreislisten;
import systemTools.ListenerTools;

public class RezAufteilen extends JXPanel implements ActionListener, KeyListener,FocusListener,RehaTPEventListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -553992196522355066L;
	MattePainter mp = null;
	LinearGradientPaint p = null;
	private RehaTPEventClass rtp = null;
	
	public static String htmlFontSize = "4"; //könnte man auch (besser) über die SystemEinstellungen softwareweit regeln
	
	public JRtaComboBox[] jcmb_1 =  {null,null,null,null};
	public JRtaComboBox[] jcmb_2 =  {null,null,null,null};
	public JRtaComboBox[] jcmb_3 =  {null,null,null,null};
	
	public JRtaTextField[] jtf_dauer1 = {null};
	public JRtaTextField[] jtf_dauer2 = {null};
	public JRtaTextField[] jtf_dauer3 = {null};
	
	public JRtaTextField[] jtf_1 = {null,null,null,null};
	public JRtaTextField[] jtf_2 = {null,null,null,null};
	public JRtaTextField[] jtf_3 = {null,null,null,null};
	
	public JRtaCheckBox[] jcb = {null,null,null};
	
	JEditorPane htmlPane = null;
	JScrollPane scrHtml = null;
	
	public JButton abbrechen = null;
	public JButton speichern = null;
	
	public Vector<Vector<String>> preisvec = null;
	
//	public Vector<String> wrapper = null;
	public AktuelleRezepte aktrez = null;
	
	public int anzahlVorrangig;
	
	public int pKUERZEL = 1;
	public int pPOS = 2;
	public int pPREIS = 3;
	public int pID = 9;
	
	public String[] dauer = {"","10","15","20","25","30","35","40","45","50","55","60","65","70","75","80","85","90","95","100","105","110","115","120"};
	
	Rezeptvector currVO = new Rezeptvector();
	
	public RezAufteilen(){
		super();
		setName("RezeptAufteilen");
		rtp = new RehaTPEventClass();
		rtp.addRehaTPEventListener((RehaTPEventListener) this);
		
		currVO.setVec_rez(Reha.instance.patpanel.vecaktrez);
//		wrapper = Reha.instance.patpanel.vecaktrez;
//		aktrez = aktRezept;
		
		ladePreisliste();

		addKeyListener(this);
		
		setLayout(new BorderLayout());
		setOpaque(true);
		setBorder(BorderFactory.createEmptyBorder(0, 0, 5, 0));
		
		add(getDatenPanel(),BorderLayout.CENTER);
		add(getButtonPanel(),BorderLayout.SOUTH);
		setBackgroundPainter(Reha.instance.compoundPainter.get("RezNeuanlage"));

		validate();
		
		kinderEinstellen(currVO);

		SwingUtilities.invokeLater(new Runnable(){
		 	   public  void run()
		 	   {
		 			setzeFocus();		 		   
		 	   }
		});	
	}
	
	public void setzeFocus() {
		SwingUtilities.invokeLater(new Runnable(){
			  public  void run()
			  {
				  //hier das Componente benennen;
				 // jcmb[cRKLASSE].requestFocus();
			  }
		  });			
	}
	
	
	
	public JScrollPane getDatenPanel() {
		//gesamte Breite und 1/3 der Höhe
		String x = "fill:0:grow(1.0)";
		String y = "fill:0:grow(0.25),2dlu,fill:0:grow(0.75)";

		FormLayout lay = new FormLayout(x,y);
		CellConstraints cc = new CellConstraints();
		PanelBuilder jpan = new PanelBuilder(lay);
		jpan.setOpaque(false);
		
		jpan.setDefaultDialogBorder();
		jpan.getPanel().setOpaque(false);
		//JScrollPane scrHtml = null;
		jpan.add(getHTMLDaten(),cc.xy(1, 1));
		jpan.add(getKinderPanel(),cc.xy(1, 3,CellConstraints.FILL,CellConstraints.FILL));
		
		
		
		jpan.getPanel().validate();
		JScrollPane jscr = null;
		jscr = JCompTools.getTransparentScrollPane(jpan.getPanel());
		jscr.getVerticalScrollBar().setUnitIncrement(15);
		return jscr;
	}
	
	public JXPanel getButtonPanel() {
		JXPanel	jpan = JCompTools.getEmptyJXPanel();
		jpan.addKeyListener(this);
		jpan.setOpaque(false);
		FormLayout lay = new FormLayout(
		        // 1                2          3             4      5               6          7 
				"fill:0:grow(0.25),50dlu,fill:0:grow(0.25),50dlu,fill:0:grow(0.25),50dlu,fill:0:grow(0.25)",
				// 1  2  3  
				"5dlu,p,5dlu");
		CellConstraints cc = new CellConstraints();
		jpan.setLayout(lay);
		speichern = new JButton("Kindrezepte erzeugen");
		speichern.setActionCommand("erzeugen");
		speichern.addActionListener(this);
		speichern.addKeyListener(this);
		speichern.setMnemonic(KeyEvent.VK_S);
		jpan.add(speichern,cc.xyw(2,2,2));

		abbrechen = new JButton("abbrechen");
		abbrechen.setActionCommand("abbrechen");
		abbrechen.addActionListener(this);
		abbrechen.addKeyListener(this);
		abbrechen.setMnemonic(KeyEvent.VK_A);		
		jpan.add(abbrechen,cc.xy(6,2));

		return jpan;
		
	}
	
	private JScrollPane getHTMLDaten() {
		JEditorPane htmlPane = new JEditorPane();
		htmlPane.setContentType("text/html");
        htmlPane.setEditable(false);
        htmlPane.setOpaque(false);
        
        StringBuffer buf1 = new StringBuffer();
        buf1.setLength(0);
		buf1.trimToSize();
		
		buf1.append("<html><head>");
		buf1.append("<STYLE TYPE=\"text/css\">");
		buf1.append("<!--");
		buf1.append("A{text-decoration:none;background-color:transparent;border:none}");
		buf1.append("TD{font-family: Arial; font-size: 12pt; padding-left:5px;padding-right:15px}");
		buf1.append(".spalte1{color:#0000FF;}");
		buf1.append(".spalte2{color:#333333;}");
		buf1.append(".spalte2{color:#333333;}");
		buf1.append("--->");
		buf1.append("</STYLE>");
		buf1.append("</head>");
		buf1.append("<div style=margin-left:15px;>");
		buf1.append("<font face=\"Tahoma\"><style=margin-left=15px;>");
		buf1.append("<br>");
		buf1.append("<table>"); 
		buf1.append("<tr>");
		buf1.append("<th rowspan=\"5\"><a href=\"http://rezedit.de\"><img src='file:///"+Path.Instance.getProghome()+"icons/Rezept.png' border=0></a></th>");
		buf1.append("<td class=\"spalte1\" align=\"right\">");
		buf1.append("Rezeptnummer");
		buf1.append("</td><td class=\"spalte2\" align=\"left\">");
		buf1.append("<b>"+Reha.instance.patpanel.vecaktrez.get(1)+"</b>");
		buf1.append("</td>");
		buf1.append("</tr>");
		/*******/
		buf1.append("<tr>");
		buf1.append("<td class=\"spalte1\" align=\"right\">");
		buf1.append("Heilmittel 1");
		buf1.append("</td><td class=\"spalte2\" align=\"left\">");
		buf1.append(currVO.getHMkurz(1).equals("") ? "-----" : currVO.getAnzBehS(1)+" x "+getLangPos(currVO.getHMkurz(1)) );
		buf1.append("</td>");
		buf1.append("</tr>");
		/*******/
		buf1.append("<tr>");
		buf1.append("<td class=\"spalte1\" align=\"right\">");
		buf1.append("Heilmittel 2");
		buf1.append("</td><td class=\"spalte2\" align=\"left\">");
        buf1.append(currVO.getHMkurz(2).equals("") ? "-----" : currVO.getAnzBehS(2)+" x "+getLangPos(currVO.getHMkurz(2)) );
		buf1.append("</td>");
		buf1.append("</tr>");
		/*******/
		buf1.append("<tr>");
		buf1.append("<td class=\"spalte1\" align=\"right\">");
		buf1.append("Heilmittel 3");
		buf1.append("</td><td class=\"spalte2\" align=\"left\">");
        buf1.append(currVO.getHMkurz(3).equals("") ? "-----" : currVO.getAnzBehS(3)+" x "+getLangPos(currVO.getHMkurz(3)) );
		buf1.append("</td>");
		buf1.append("</tr>");
		/*******/
		buf1.append("<tr>");
		buf1.append("<td class=\"spalte1\" align=\"right\">");
		buf1.append("ergänzendes Heilmittel");
		buf1.append("</td><td class=\"spalte2\" align=\"left\">");
        buf1.append(currVO.getHMkurz(4).equals("") ? "-----" : currVO.getAnzBehS(4)+" x "+getLangPos(currVO.getHMkurz(4)) );
		buf1.append("</td>");
		buf1.append("</tr>");
		/*******/
		
		buf1.append("</table>");
		buf1.append("</html>");
		htmlPane.setText(buf1.toString());
		
		scrHtml = JCompTools.getTransparentScrollPane(htmlPane);
        scrHtml.validate();
		return scrHtml;
	}
	
	
	public JScrollPane getKinderPanel()	{
		String[] titel = {"Heilmittel 1 / Anzahl","nur bei Doppelbehandl.","ergänzendes Heilmittel  / Anzahl","Heilmittel 3 / Anzahl"};
		//                     1         2     3      4            5            6      7
		String x = "right:max(80dlu;p), 4dlu, 60dlu, 5dlu, right:max(80dlu;p), 4dlu, 60dlu";
		//                   3         5  6  7  8   9        11   12  13cb       15 16 17 18         20       22cb        24
		String y = "p, 2dlu, p, 2dlu,  p, p, p, p , p, 15dlu, p, 2dlu, p,   2dlu, p, p, p, p , 15dlu, p, 2dlu,  p,  2dlu,  p, p, p , p ,2dlu" ;
		FormLayout lay = new FormLayout(x,y);
		CellConstraints cc = new CellConstraints();
		PanelBuilder jpan = new PanelBuilder(lay);
		jpan.setOpaque(false);
		
		
		jpan.addSeparator("Kindrezept 1 - "+Reha.instance.patpanel.vecaktrez.get(1)+"-1", cc.xyw(1,1,7));
		jcb[0] = new JRtaCheckBox("Kindrezept 1 - aktiv");
		jcb[0].setActionCommand("kindrezept1");
		jpan.add(jcb[0],cc.xyw(1, 3, 5));
		int offsety = 5;
		int[] posx = {1,3,4,7};
		
		for(int i = 0; i < 4; i++) {
			if(i < 3 ) {
				jpan.addLabel(titel[i],cc.xy(posx[0], i+offsety));
				jcmb_1[i] = new JRtaComboBox();
				jcmb_1[i].setDataVectorWithStartElement(preisvec,0,9,"./.");
				jcmb_1[i].setName("jcmb_1"+Integer.toString(i));
				jcmb_1[i].setActionCommand("jcmb_1"+Integer.toString(i));
				jcmb_1[i].addActionListener(this);
				jpan.add(jcmb_1[i],cc.xyw(posx[1], i+offsety,3));
				jtf_1[i] = new JRtaTextField("ZAHLEN",true);
				jpan.add(jtf_1[i],cc.xy(posx[3], i+offsety));
			}else {
				jcmb_1[3] = new JRtaComboBox();
				jcmb_1[3].setDataVectorWithStartElement(preisvec,0,9,"./.");
				jtf_1[3] = new JRtaTextField("ZAHLEN",true);
				
				jpan.addLabel("Dauer in Min.",cc.xy(5, i+offsety));
				jtf_dauer1[0] = new JRtaTextField("ZAHLEN",true);
				jpan.add(jtf_dauer1[0],cc.xy(7, i+offsety));
			}
			
		}
		/*******************************************/
		offsety = 15;
		jpan.addSeparator("Kindrezept 2 - "+Reha.instance.patpanel.vecaktrez.get(1)+"-2", cc.xyw(1,11,7));
		jcb[1] = new JRtaCheckBox("Kindrezept 2 - aktiv");
		jcb[1].setActionCommand("kindrezept2");
		jpan.add(jcb[1],cc.xyw(1, 13, 5));
		for(int i = 0; i < 4; i++) {
			if(i < 3) {
				jpan.addLabel(titel[i],cc.xy(posx[0], i+offsety));
				jcmb_2[i] = new JRtaComboBox();
				jcmb_2[i].setDataVectorWithStartElement(preisvec,0,9,"./.");
				jcmb_2[i].setName("jcmb_2"+Integer.toString(i));
				jcmb_2[i].setActionCommand("jcmb_2"+Integer.toString(i));
				jcmb_2[i].addActionListener(this);
				jpan.add(jcmb_2[i],cc.xyw(posx[1], i+offsety,3));
				jtf_2[i] = new JRtaTextField("ZAHLEN",true);
				jpan.add(jtf_2[i],cc.xy(posx[3], i+offsety));
			}else {
				jcmb_2[3] = new JRtaComboBox();
				jcmb_2[3].setDataVectorWithStartElement(preisvec,0,9,"./.");
				jtf_2[3] = new JRtaTextField("ZAHLEN",true);
				
				jpan.addLabel("Dauer in Min.",cc.xy(5, i+offsety));
				jtf_dauer2[0] = new JRtaTextField("ZAHLEN",true);
				jpan.add(jtf_dauer2[0],cc.xy(7, i+offsety));
			}
		}
		/*******************************************/
		offsety = 24;
		jpan.addSeparator("Kindrezept 3 - "+Reha.instance.patpanel.vecaktrez.get(1)+"-3", cc.xyw(1,20,7));
		jcb[2] = new JRtaCheckBox("Kindrezept 3 - aktiv");
		jcb[2].setActionCommand("kindrezept3");
		jpan.add(jcb[2],cc.xyw(1, 22, 5));
		for(int i = 0; i < 4; i++) {
			if(i < 3) {
				jpan.addLabel(titel[i],cc.xy(posx[0], i+offsety));
				jcmb_3[i] = new JRtaComboBox();
				jcmb_3[i].setDataVectorWithStartElement(preisvec,0,9,"./.");
				jcmb_3[i].setName("jcmb_3"+Integer.toString(i));
				jcmb_3[i].setActionCommand("jcmb_3"+Integer.toString(i));
				jcmb_3[i].addActionListener(this);
				jpan.add(jcmb_3[i],cc.xyw(posx[1], i+offsety,3));
				jtf_3[i] = new JRtaTextField("ZAHLEN",true);
				jpan.add(jtf_3[i],cc.xy(posx[3], i+offsety));
			}else {
				jcmb_3[3] = new JRtaComboBox();
				jcmb_3[3].setDataVectorWithStartElement(preisvec,0,9,"./.");
				jtf_3[3] = new JRtaTextField("ZAHLEN",true);

				jpan.addLabel("Dauer in Min.",cc.xy(5, i+offsety));
				jtf_dauer3[0] = new JRtaTextField("ZAHLEN",true);
				jpan.add(jtf_dauer3[0],cc.xy(7, i+offsety));
			}
		}
		
		jpan.getPanel().validate();
		JScrollPane spane = JCompTools.getTransparentScrollPane(jpan.getPanel());
        spane.validate();
        return spane;
	}
	@Override
	public void rehaTPEventOccurred(RehaTPEvent evt) {
		try{
			if(evt.getDetails()[0] != null){
				if(evt.getDetails()[0].equals(this.getName())){
					this.setVisible(false);
					rtp.removeRehaTPEventListener((RehaTPEventListener) this);
					rtp = null;
					aufraeumen();
				}
			}
		}catch(NullPointerException ne){
			JOptionPane.showMessageDialog(null, "Fehler beim abhängen des Listeners Rezept-Neuanlage\n"+
					"Bitte informieren Sie den Administrator über diese Fehlermeldung");
		}
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void focusLost(FocusEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
    public void keyPressed(KeyEvent arg0) {
        if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
            doAbbrechen();
            return;
        }
    }

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// //System.out.println("4 - "+e.getActionCommand());
		if(e.getActionCommand().equals("abbrechen") ){
			new SwingWorker<Void,Void>(){
				@Override
				protected Void doInBackground() throws Exception {
					doAbbrechen();
					return null;
				}
			}.execute();
			return;
		}else if (e.getActionCommand().equals("erzeugen") ){
			new SwingWorker<Void,Void>(){
				@Override
				protected Void doInBackground() throws Exception {
					erzeugeKindrezepte();
					return null;
				}
			}.execute();
			return;
		}
		
	}
	
	
	private void doAbbrechen(){
		aufraeumen();
		((JXDialog)this.getParent().getParent().getParent().getParent().getParent()).dispose();		
	}
	
	public void aufraeumen(){
		new SwingWorker<Void,Void>(){
			@Override
			protected Void doInBackground() throws Exception {
				for(int i = 0; i < jtf_1.length;i++){
					ListenerTools.removeListeners(jtf_1[i]);
					ListenerTools.removeListeners(jtf_2[i]);
					ListenerTools.removeListeners(jtf_3[i]);
				}
				for(int i = 0; i < jcb.length;i++){
					ListenerTools.removeListeners(jcb[i]);
				}
				for(int i = 0; i < jcmb_1.length;i++){
					ListenerTools.removeListeners(jcmb_1[i]);
					ListenerTools.removeListeners(jcmb_2[i]);
					ListenerTools.removeListeners(jcmb_3[i]);
				}
				ListenerTools.removeListeners(getInstance());
				if(rtp != null){
					rtp.removeRehaTPEventListener((RehaTPEventListener) getInstance());
					rtp = null;
				}
				return null;
			}
		}.execute();
	}
	public RezAufteilen getInstance(){
		return this;
	}
	
	public void ladePreisliste() {
		String disziplin = RezTools.getDisziplinFromRezNr(currVO.getRezNb());
		preisvec = SystemPreislisten.hmPreise.get(disziplin).get(Integer.parseInt(Reha.instance.patpanel.vecaktrez.get(41))-1);
		//System.out.println("Anzahl Preise: "+preisvec.size());
		//System.out.println(preisvec);
	}
	public String getLangPos(String kurzPos) {
		for(Vector<String> preis : preisvec) {
			if(preis.get(1).equals(kurzPos)) {
				return preis.get(0);
			}
		}
		return "";
	}
	
public void kinderEinstellen(Rezeptvector currVO) {
		anzahlVorrangig = currVO.getAnzVorrHM();
		int doppelStart = currVO.startIdxDoppelbehandlung();
		boolean ergaenzendTeilen = false;
		int ergaenzendVerteilt = 0; 
//		if(doppelStart > 0) {   // erledigt startIdxDoppelbehandlung()
//			anzahlVorrangig--;
//		}
		String tst = currVO.getArtDBehandlS(4);
		int anzahlErgaenzend = ( (!currVO.getArtDBehandlS(4).equals("0")) ? 1 : 0);
		int einheitenErgaenzend = (anzahlErgaenzend == 1 ?  currVO.getAnzBeh(4) : 0);
		
		int[] einheitenVorrangig = new int[] {currVO.getAnzBeh(1), currVO.getAnzBeh(2), currVO.getAnzBeh(3)};
		int summeVorrangig1= einheitenVorrangig[0];
		int summeVorrangig2= einheitenVorrangig[0]+einheitenVorrangig[1];
		int summeVorrangig3= einheitenVorrangig[0]+einheitenVorrangig[1]+einheitenVorrangig[2];
		
		if(anzahlVorrangig == 2) {
			jcb[0].setSelected(true);
			jcb[1].setSelected(true);
		}else if(anzahlVorrangig == 3) {
			jcb[0].setSelected(true);
			jcb[1].setSelected(true);
			jcb[2].setSelected(true);
		}

		int test = 0;
		
		jcmb_1[0].setSelectedIndex(leistungTesten(currVO.getArtDBehandlId(1)));
		jtf_1[0].setText(currVO.getAnzBehS(1));
		jtf_dauer1[0].setText(currVO.getDauer());
		jtf_dauer2[0].setText(currVO.getDauer());
		if(doppelStart == 0) {
		    jcmb_2[0].setSelectedIndex(leistungTesten(currVO.getArtDBehandlId(2)));
		    jtf_2[0].setText(currVO.getAnzBehS(2));
		    if(anzahlVorrangig == 3) {
		        jcmb_3[0].setSelectedIndex(leistungTesten(currVO.getArtDBehandlId(3)));
		        jtf_3[0].setText(currVO.getAnzBehS(3));
		        jtf_dauer3[0].setText(currVO.getDauer());
		    }
		}else if(doppelStart == 1) {
			jcmb_1[1].setSelectedIndex(leistungTesten(currVO.getArtDBehandlId(2)));
			jtf_1[1].setText(currVO.getAnzBehS(2));
			jcmb_2[0].setSelectedIndex(leistungTesten(currVO.getArtDBehandlId(3)));
			jtf_2[0].setText(currVO.getAnzBehS(3));
		}else if(doppelStart == 2) {
			jcmb_2[0].setSelectedIndex(leistungTesten(currVO.getArtDBehandlId(2)));
			jtf_2[0].setText(currVO.getAnzBehS(2));
			jcmb_2[1].setSelectedIndex(leistungTesten(currVO.getArtDBehandlId(3)));
			jtf_2[1].setText(currVO.getAnzBehS(3));
		}

		if(anzahlErgaenzend > 0) {
		    int idxErgHM = leistungTesten(currVO.getArtDBehandlId(4));
			if(einheitenErgaenzend <= einheitenVorrangig[0]) {
				jcmb_1[2].setSelectedIndex(idxErgHM);
				jtf_1[2].setText(currVO.getAnzBehS(4));
			}else {
				ergaenzendVerteilt = einheitenVorrangig[0];
				jcmb_1[2].setSelectedIndex(idxErgHM);
				jtf_1[2].setText(Integer.toString(einheitenVorrangig[0]));
				if( ( (test=(einheitenErgaenzend - ergaenzendVerteilt))) <= einheitenVorrangig[1]) {
					jcmb_2[2].setSelectedIndex(idxErgHM);
					jtf_2[2].setText(Integer.toString(test));
				}else {
					int rest = einheitenErgaenzend - (einheitenVorrangig[0]+einheitenVorrangig[1]);
					jcmb_2[2].setSelectedIndex(idxErgHM);
					jtf_2[2].setText(Integer.toString(einheitenVorrangig[1]));
					jcmb_3[2].setSelectedIndex(idxErgHM);
					jtf_3[2].setText(Integer.toString(rest));
				}
			}
		}		
	}
	
	public int leistungTesten(int veczahl){
		int retwert = 0;
		if(veczahl==-1 || veczahl==0){
			return retwert;
		}
		if(preisvec==null){
			return 0;
		}
		String sveczahl = Integer.toString(veczahl);
			
		for(int i = 0;i<preisvec.size();i++){
			if(  preisvec.get(i).get(9).equals(sveczahl)){
				return i+1;
			}
		}
		return retwert;
	}
	
	private final static int cREZEPTNUMMER = 1;
	private final static int cHATKINDER = 28;
	private final static int cANZAHLKINDER = 30;
//	private final static int cISTKIND = 29;
	public void erzeugeKindrezepte(){
        int antwort = JOptionPane.showConfirmDialog(this,
                aktrez.htmlStart + "Wollen Sie die jetzt Kindrezepte wie dargestellt erzeugen?" + aktrez.htmlEnd,
                "Wichtige Benutzeranfrage", JOptionPane.YES_NO_CANCEL_OPTION);
        if(antwort != JOptionPane.YES_OPTION) {
			return;
		}
		
		Vector<Vector<String>> feldname = SqlInfo.holeFelder("describe verordn");
		Vector<String> original = (Vector<String>)Reha.instance.patpanel.vecaktrez.clone();
		StringBuffer buf = new StringBuffer();
		
		JRtaTextField[][] tfs = {jtf_1,jtf_2,jtf_3};
		JRtaTextField[][] tfs_dauer = {jtf_dauer1,jtf_dauer2,jtf_dauer3};
		JRtaComboBox[][] jcmb = {jcmb_1,jcmb_2,jcmb_3};
		
		//System.out.println("Größe Feldvektor: "+feldname.size());
		//System.out.println("Größe Rezeptvektor: "+original.size());
		String stest;
		int itest = 0;
		int kinder = 0;
		try {
			for(int i = 0; i < (this.anzahlVorrangig); i++) {
				buf.setLength(0);
				buf.trimToSize();
				if(jcb[i].isSelected()){
					buf.append("insert into verordn set ");
					for(int x = 0; x < feldname.size(); x++) {
						if(!(feldname.get(x).get(0).toLowerCase().equals("id"))) {
							if( (stest=feldname.get(x).get(0)).toLowerCase().startsWith("art_dbeh")) {
								itest = Integer.parseInt(stest.substring(stest.length()-1))-1;
								if(itest == 2 ) {
									buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest+1].getSelectedIndex(),pID)+"' ,");
								}else if(itest == 3) {
									buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest-1].getSelectedIndex(),pID)+"' ,");
								}else {
									buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest].getSelectedIndex(),pID)+"' ,");
								}
							}else if((stest=feldname.get(x).get(0)).toLowerCase().startsWith("pos")) {
								itest = Integer.parseInt(stest.substring(stest.length()-1))-1;
								if(itest == 2 ) {
									buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest+1].getSelectedIndex(),pPOS)+"' ,");									
								}else if(itest == 3) {
									buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest-1].getSelectedIndex(),pPOS)+"' ,");
								}else {
									buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest].getSelectedIndex(),pPOS)+"' ,");
								}
							}else if((stest=feldname.get(x).get(0)).toLowerCase().startsWith("kuerzel")) {
								itest = Integer.parseInt(stest.substring(stest.length()-1))-1;
								if(itest < 4) {
									if(itest==2) {
										buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest+1].getSelectedIndex(),pKUERZEL)+"' ,");
									}if(itest==3) {
										buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest-1].getSelectedIndex(),pKUERZEL)+"' ,");
									}else if(itest < 2) {
										buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest].getSelectedIndex(),pKUERZEL)+"' ,");										
									}
								}else {
									if(itest != 3) {
										buf.append(stest+"='"+original.get(x)+"' ,");	
									}
									
								}
							}else if((stest=feldname.get(x).get(0)).toLowerCase().startsWith("preise")) {
								itest = Integer.parseInt(stest.substring(stest.length()-1))-1;
								if(itest == 2 ) {
									buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest+1].getSelectedIndex(),pPREIS)+"' ,");	
								}else if(itest == 3) {
									buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest-1].getSelectedIndex(),pPREIS)+"' ,");
								}else {
									buf.append(stest+"='"+getFromPreisVec(jcmb[i][itest].getSelectedIndex(),pPREIS)+"' ,");
								}
								
							}else if((stest=feldname.get(x).get(0)).toLowerCase().startsWith("anzahl") && (!feldname.get(x).get(0).toLowerCase().equals("anzahlkm")) && (!feldname.get(x).get(0).toLowerCase().equals("anzahlhb"))) {
								itest = Integer.parseInt(stest.substring(stest.length()-1))-1;
								String sanzahl = "";
								if(itest == 2) {
									sanzahl = (!tfs[i][itest+1].getText().equals("") ? tfs[i][itest+1].getText() : "0" );
								}else if(itest == 3) {
									sanzahl = (!tfs[i][itest-1].getText().equals("") ? tfs[i][itest-1].getText() : "0" );
								}else {
									sanzahl = (!tfs[i][itest].getText().equals("") ? tfs[i][itest].getText() : "0" );
								}
								buf.append(stest+"='"+sanzahl+"' ,");
							}else if((stest=feldname.get(x).get(0)).toLowerCase().startsWith("rez_nr")) {
								buf.append(stest+"='"+original.get(x)+"-"+Integer.toString(i+1)+"' ,");
							}else if((stest=feldname.get(x).get(0)).toLowerCase().startsWith("logfrei2")) {
								buf.append(stest+"='T' ,");
							}else if((stest=feldname.get(x).get(0)).toLowerCase().startsWith("logfrei1")) {
								buf.append(stest+"='F' ,");
							}else if((stest=feldname.get(x).get(0)).toLowerCase().startsWith("dauer")) {
								buf.append(stest+"='"+ tfs_dauer[i][0].getText()+"'  ,");
							}else if((stest=feldname.get(x).get(0)).toLowerCase().startsWith("termine")) {
								buf.append(stest+"='' ,");
							}else if((stest=feldname.get(x).get(0)).toLowerCase().startsWith("zzstatus")) {
								buf.append(stest+"='"+original.get(x)+"' ,"); //vorher fest auf 0 gesetzt
							}else if(x == (original.size()-1)) {
								buf.append(feldname.get(x).get(0)+"='"+original.get(x)+"'");
							}else {
								buf.append(feldname.get(x).get(0)+"='"+StringTools.EscapedDouble(original.get(x))+"' ,");
							}
						}
					}
					SqlInfo.sqlAusfuehren(buf.toString());
					kinder++;
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		final String anzahlkinder = Integer.toString(kinder);
		final String reznum = original.get(cREZEPTNUMMER);
		final String feldhatkinder = feldname.get(cHATKINDER).get(0);
		final String feldanzahlkinder = feldname.get(cANZAHLKINDER).get(0);

		new SwingWorker<Void,Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				try {
					SqlInfo.sqlAusfuehren("update verordn set "+
							feldhatkinder+"='T', "+feldanzahlkinder+"='"+anzahlkinder+"' where rez_nr='"+reznum+"' LIMIT 1" );
					Reha.instance.patpanel.aktRezept.holeRezepte(Reha.instance.patpanel.patDaten.get(29),reznum);
					doAbbrechen();
				}catch(Exception ex) {
					ex.printStackTrace();
				}
				return null;
			}
		}.execute();
		
		
	}
	
	public String getFromPreisVec(int index,int element){
		String ret = (element==9 ? "0" : "");
		if(index == 0) {
			return ret;
		}
		return preisvec.get(index-1).get(element);
		
	}
}
