package org.therapi.hmrCheck;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import CommonTools.StringTools;

public class HMRlangfristBedarf {
    
    private File xml;
    private Document doc;
    private Element root;
    private Element voBedList;
    private String typeOfVoBedarf = null;
    private String zweiterIcdCode = null;
    private String geltungsbereich = null;
    private int hoechstAlter = 0;
    private int mindestAlter = 0;
    private int seitAkutEreignisZaehler = 0;
    private String seitAkutEreignisEinheit = null;
    private String hinweisTxt = null;

    
    private static String cKG = "I";    // <kapitel V="I" DN="Maßnahmen der Physiotherapie">
    private static String cPO = "II";   // <kapitel V="II" DN="Maßnahmen der Podologischen Therapie">
    private static String cLO = "III";  // <kapitel V="III" DN="Maßnahmen der Stimm-, Sprech-, Sprach- und Schlucktherapie">
    private static String cER = "IV";   // <kapitel V="IV" DN="Maßnahmen der Ergotherapie">
    private static String cEN = "V";    // <kapitel V="V" DN="Maßnahmen der Ernährungstherapie">
    static HashMap<String,String> hmKapitel = new HashMap<>();
    

    public HMRlangfristBedarf(File xml) {
        this.xml = xml;
        this.loadXML();
        hmKapitel.put( "Physio", cKG );
        hmKapitel.put( "Massage", cKG );    // Massage läuft in der HMR mit unter Physio
        hmKapitel.put( "Ergo", cER );
        hmKapitel.put( "Logo", cLO );
        hmKapitel.put( "Podo", cPO );
    }
    
    static HashMap<String,String> hmKV = new HashMap<>();
    private void kvInit () {
        hmKV.put( "01", "Schleswig-Holstein" );
        hmKV.put( "02", "Hamburg" );
        hmKV.put( "03", "Bremen" );
        hmKV.put( "17", "Niedersachsen" );
        hmKV.put( "20", "Westfalen-Lippe" );
        hmKV.put( "38", "Nordrhein" );
        hmKV.put( "46", "Hessen" );
        hmKV.put( "51", "Rheinland-Pfalz" );
        hmKV.put( "52", "Baden-Württemberg" );
        hmKV.put( "71", "Bayerns" );
        hmKV.put( "72", "Berlin" );
        hmKV.put( "73", "Saarland" );
        hmKV.put( "74", "KBV" );
        hmKV.put( "78", "Mecklenburg-Vorpommern" );
        hmKV.put( "83", "Brandenburg" );
        hmKV.put( "88", "Sachsen-Anhalt" );
        hmKV.put( "93", "Thüringen" );
        hmKV.put( "98", "Sachsen" );
    }

    
    public String getKapitelFromHMap(String disziKurz) {
        return hmKapitel.get( disziKurz );
    }

    private void loadXML() {
        try {
            this.doc = new SAXBuilder().build(this.xml);
            this.root = doc.getRootElement();
            Element body =  this.root.getChild("body", this.root.getNamespace());
            Element sdhma = body.getChild("sdhma_stammdaten", body.getNamespace("sdhma_stammdaten"));
            this.voBedList = sdhma.getChild("verordnungsbedarf_liste", sdhma.getNamespace("verordnungsbedarf_liste"));
        } catch (JDOMException | IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        kvInit ();
    }
    
    private void initVars() {
        typeOfVoBedarf = null;
        zweiterIcdCode = null;
        hoechstAlter = 0;
        mindestAlter = 0;
        seitAkutEreignisZaehler = 0;
        seitAkutEreignisEinheit = null;
        hinweisTxt = null;
        geltungsbereich  = null;
    }

    public boolean isBesVoBedOrLangfristBed(String icd10, String kap, String diagnosegruppe) {
        return isBesVoBedOrLangfristBed(icd10, null, kap, diagnosegruppe);
    }

    public boolean isBesVoBedOrLangfristBed(String ICD10, String ICD10_2, String diszi, String diagnosegruppe) {
        boolean foundMatch = false;
        Element hmlist = null;
        Element hm = null;
        Element bedarf = null;
        Element geltungsbereich_kv = null;
        initVars();
        if (ICD10 != null) {
            ICD10 = StringTools.delZusaetzeFromIcdString(ICD10);
        }
        if (ICD10_2 != null) {
            ICD10_2 = StringTools.delZusaetzeFromIcdString(ICD10_2);
        }
        for(Element voBedarf : this.voBedList.getChildren()) {
            Element currIcd = voBedarf.getChild("icd_code", voBedarf.getNamespace("icd_code"));
            if (currIcd == null) {
                continue;
            }
            geltungsbereich_kv = voBedarf.getChild("geltungsbereich_kv", voBedarf.getNamespace("geltungsbereich_kv"));
            hmlist = voBedarf.getChild("heilmittel_liste", voBedarf.getNamespace("heilmittel_liste"));
            hm = hmlist.getChild("heilmittel", hmlist.getNamespace("heilmittel"));
            bedarf = hm.getChild("anlage_heilmittelvereinbarung", hm.getNamespace("anlage_heilmittelvereinbarung"));
            Element zweitCode = hm.getChild("sekundaercode", hm.getNamespace("sekundaercode"));
            Element maxAlter = hm.getChild("obere_altersgrenze", hm.getNamespace("obere_altersgrenze"));
            Element minAlter = hm.getChild("untere_altersgrenze", hm.getNamespace("untere_altersgrenze"));
            Element hinweis = hm.getChild("hinweistext", hm.getNamespace("hinweistext"));
            Element seitAkut = hm.getChild("zeitraum_akutereignis", hm.getNamespace("zeitraum_akutereignis"));
            
            String currIcdVal = currIcd.getAttribute("V").getValue();
            if(currIcdVal.equals(ICD10) || currIcdVal.equals(ICD10 + "-") || currIcdVal.equals(ICD10 + ".-")) {
                foundMatch = true;
                zweiterIcdCode = (zweitCode == null ? null : zweitCode.getAttribute("V").getValue());
                hoechstAlter = (maxAlter == null ? 0 : Integer.parseInt(maxAlter.getAttribute("V").getValue()));
                mindestAlter = (minAlter == null ? 0 : Integer.parseInt(minAlter.getAttribute("V").getValue()));
                seitAkutEreignisZaehler = (seitAkut == null ? 0 : Integer.parseInt(seitAkut.getAttribute("V").getValue()));
                seitAkutEreignisEinheit = (seitAkut == null ? null : seitAkut.getAttribute("U").getValue());
                hinweisTxt  = (hinweis == null ? null : hinweis.getAttribute("V").getValue());
                if ((zweitCode == null) || 
                        (zweiterIcdCode.equals(ICD10_2))){
                   typeOfVoBedarf = bedarf.getAttribute("V").getValue();
                   Element kaplist = hm.getChild("kapitel_liste", hm.getNamespace("kapitel_liste"));
                   Element kapitel = null;
                   for(Element c : kaplist.getChildren()) {
                       if(c.getAttribute("V").getValue().equals(diszi)) {
                           kapitel = c;
                       }
                   }
                   if (geltungsbereich_kv != null) {
                       String key = geltungsbereich_kv.getAttribute("V").getValue();
                       geltungsbereich = hmKV.get(key);
                   }
                   if (kapitel != null) {
                       Element dglist = kapitel.getChild("diagnosegruppe_liste", kapitel.getNamespace("diagnosegruppe_liste"));
                       Element diagnosegr = null;
                       for(Element c : dglist.getChildren()) {
                           if(c.getAttribute("V").getValue().equals(diagnosegruppe)) {
                               diagnosegr = c;
                           }
                       }
                       if (diagnosegr != null) {
                           return true;
                       }
                   }
                } else {
                    // TODO Zweitcode falsch oder fehlt
                }
            } else if (foundMatch) {
                break;
            }
        }
        return false;
    }

    public String getTypeOfVoBedarf() {
        return (typeOfVoBedarf != null ? typeOfVoBedarf : "");
    }

    public int getAkutLimit() {
        return seitAkutEreignisZaehler;
    }
    public String getAkutUnit() {
        return seitAkutEreignisEinheit;
    }

    public int getUpperLimit() {
        return hoechstAlter;
    }

    public int getLowerLimit() {
        return mindestAlter;
    }

    public String getSecCode() {
        return (zweiterIcdCode != null ? zweiterIcdCode : "");
    }

    public String getHint() {
        return (hinweisTxt != null ? hinweisTxt : "");
    }
    
    public String getValid() {
        return (geltungsbereich != null ? geltungsbereich : "");
    }
    
}
