package org.therapi.hmrCheck;

import java.util.Vector;

public class Nagelspangenbehandlung {
    Vector<String> posVorr = new Vector<String>();
    Vector<Integer> anzPos  = new Vector<Integer>();

    public Nagelspangenbehandlung (Vector<String> HMposVorr, Vector<Integer> anzahl){
        posVorr = HMposVorr;
        anzPos = anzahl;
        int idxEint = -1;
        int idxMehrt = -1;

        if (posVorr.contains("78230")) {
            // einteil. Nagelkorrekturspange
            idxEint = posVorr.indexOf("78230");
            System.out.println("L60positionen: einteilig " + HMposVorr + "   idx: " + idxEint);          
        } else if (posVorr.contains("78300")) {
            // mehrteil. Nagelkorrekturspange
            idxMehrt = posVorr.indexOf("78300");
            System.out.println("L60positionen: mehrteilig " + HMposVorr + "   idx: " + idxMehrt);
        } else if (posVorr.contains("78400")) {
            // mehrteil. Nagelkorrekturspange
            idxMehrt = posVorr.indexOf("78400");
            System.out.println("L60positionen: mehrteilig " + HMposVorr + "   idx: " + idxMehrt);                        
        } else {
            System.out.println("L60positionen: unknown Type.  HM: " + HMposVorr);
        }
        int idxCommon = (idxMehrt >= 0 ? idxMehrt : idxEint);
        if (!posVorr.contains("78100")) {
            // keine Erstbefundung - ist das ein Folgerezept? (?nachfragen?)
            // insOrAdd(posVorr, idxCommon, anzahl, "78100", 1);
        }
        if (!posVorr.contains("78520")) {
            // kein Behandlungsabschluss - gibt es ein Folgerezept? (?nachfragen?)
            // insOrAdd(posVorr, idxCommon, anzahl, "78520", 1);
        }
        if (idxEint >= 0) {
            if (!posVorr.contains("78210")) {
                insOrAdd(posVorr, idxEint, anzahl, "78210", 2);
            }            
            if (!posVorr.contains("78220")) {
                insOrAdd(posVorr, idxEint, anzahl, "78220", 1);
            }            
        }
}

    public Vector<String> getAllVorr() {
        return posVorr;
    }
    
    private int dec (int anz) {
        return --anz;
    }
    
    private boolean insOrAdd (Vector<String> posVorr, int idxMaxCnt, Vector<Integer> anzahlen, String HMcode, int anz) {
        boolean noEmpty = true;
        int posExist = posVorr.size();
        for (int i = 0; i < posExist; i++) {
            if ("".equals(posVorr.get(i))) {
                posVorr.set(i, HMcode);
                anzahlen.set(i, anz);
                anzahlen.set(idxMaxCnt, anzahlen.get(idxMaxCnt) - anz);
                return true;
            }
        }
        if (posExist < 4) {
            posVorr.add(posExist, HMcode);
            anzahlen.set(posExist, anz);
            anzahlen.set(idxMaxCnt, anzahlen.get(idxMaxCnt) - anz);
            return true;
        } 
        posVorr.add(HMcode);
        anzahlen.insertElementAt(anz, posExist);
        anzahlen.set(idxMaxCnt, anzahlen.get(idxMaxCnt) - anz);
        return false;
    }
}