package org.therapi.hmrCheck;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.swing.JOptionPane;

import org.therapi.reha.patient.AktuelleRezepte;

import CommonTools.DatFunk;
import CommonTools.SqlInfo;
import CommonTools.StringTools;
import abrechnung.Disziplinen;
import commonData.Rezeptvector;
import commonData.ExcludePosList;
import commonData.HmPosList;
import commonData.VerordnungsArten;
import hauptFenster.Reha;
import hmrCheck.FehlerTxt;
import patientenFenster.RezNeuanlage2020;
import stammDatenTools.RezTools;
import systemEinstellungen.SystemConfig;

public class HMRCheck2020 {
    private Vector<Integer> anzahl = null;
    private static final int IDX_HM_ERG = 3;
    private static final int FRIST_DRINGLICH_HMR2020 = 14;
    private Vector<String> positionenVorr = null;
    private Vector<String> positionenErg = null;
    private Vector<String> positionenAll = null;
    private Vector<Vector<String>> preisvec = null;
    private String diagnosegruppe = null;
    private String diagnoseTxt = null;
    private int disziIdx;
    private String disziKurz = null;
    private String disziKapHMR = null;
    private int preisgruppe;
//    final String maxanzahl = "Die Höchstmenge pro Rezept bei ";
//    final String rotein = "<>";
    private boolean testok = true;
    private FehlerTxt fehlertext = null;
//    private int rezeptart;
    private String reznummer = null;
    private String rezdatum = null;
    private String letztbeginn = null;
    private boolean dringlich = false;
    private boolean neurezept = false;
    private boolean doppelbehandlung = false;
    private boolean unter18 = false;
    private String icd10_1 = null;
    private String icd10_2 = null;
    private static SimpleDateFormat sdDeutsch = new SimpleDateFormat("dd.MM.yyyy");
    private static SimpleDateFormat sdSql = new SimpleDateFormat("yyyy-MM-dd");
    private String geburtstag = null;
    private String patId  = null;
    private Rezeptvector rezept = null;


    VerordnungsArten voArten = new VerordnungsArten();

    int maxprorezept = 0;
    int maxproStdVO = 0;
    int maxprofall = 0;
    int behFreqMax = 0;
    Disziplinen diszis = null;
    String typeOfBedarf = null;
    int idxArtDerVO = 0;

// mal sehen, was bleibt:
// - maxProFall (aka 'orientierende Behandlungsmenge') ist uninteressant
// - es 'zählen' nur maxProVo, entweder Standard oder BVB/LHM
//   dabei berücksichtigen: Summe HM1..3; HM4 separat

    public HMRCheck2020(Rezeptvector rezeptData, String Disziplin, Vector<Vector<String>> xpreisvec) {
        disziKurz = Disziplin;
        diszis = new Disziplinen();
        diszis.setCurrDisziKurz(disziKurz);
        disziIdx = diszis.getCurrDisziIdx();
//        diszis.setCurrTypeOfVO(currTypeOfVO);
        anzahl = new Vector<Integer>();
        positionenVorr = new Vector<String>();
        positionenErg = new Vector<String>();
        positionenAll = new Vector<String>();
        rezept = rezeptData;
        diagnosegruppe = rezept.getIndiSchluessel();
        diagnoseTxt = rezept.getDiagn();
        behFreqMax = rezept.getFrequenzMax();
        patId = rezept.getPatIntern();
        int idxVorr = 0;
        int idxErg = 0;
        for (int i = 0; i < 4; i++) {
            String tmp = rezept.getHmPos(i + 1);
            if (!"".equals(tmp)) {
                anzahl.add(i, rezept.getAnzBeh(i + 1));
                if (i < 3) {
                    positionenVorr.add(idxVorr++, tmp);
                } else {
                    positionenErg.add(idxErg++, tmp);
                }
            } else {
                anzahl.add(i, 0);
            }
        }
        try {
            positionenAll.addAll(positionenVorr);
            positionenAll.addAll(positionenErg);
        } catch (Exception e) {
        }
        preisvec = xpreisvec;
//        rezeptart = rezept.getRezArt();
        reznummer = rezept.getRezNb();
        unter18 = rezept.getUnter18();
        rezdatum = DatFunk.sDatInDeutsch(rezept.getRezeptDatum());
        letztbeginn = DatFunk.sDatInDeutsch(rezept.getLastDate());
        dringlich = rezept.getDringlich();
        icd10_1 = rezept.getICD10();
        icd10_2 = rezept.getICD10_2();
        if (reznummer.equals("")) {
            neurezept = true;
        }
        fehlertext = new FehlerTxt();
        idxArtDerVO = rezept.getRezArt();
    }

    /**
     *
     * Abhängig von der Diagnosegruppe und ICD10-Codes muß geprüft werden <p>
     * 1. ist die Anzahl pro Rezept o.k.<p>
     * 2. sind die gewählten Heilmittel o.k.<p>
     * 3. ist das ergänzende Heilmittel o.k.<p>
     * @param rezNeuanlage2020 
     *
     */
    public boolean check(Object caller) {
        final int IDX_MAX_PRO_FALL = 1;
        final int IDX_MAX_PRO_VO = 2;
        final int IDX_VORR_HM = 3;
        final int IDX_MAX_VORR = 4;
        final int IDX_ERG_HM = 5;
        final int IDX_MAX_ERG = 6;
        final Object parent = caller;
        if (RezTools.isRSport(reznummer) || RezTools.isFTrain(reznummer) || RezTools.isReha(reznummer)
                || diszis.currIsRsport() || diszis.currIsFtrain()) {
            return true;
        }

        diagnosegruppe = diagnosegruppe.replace(" ", "");
        Vector<Vector<String>> tmpVec = SqlInfo.holeFelder(
                "select * from hmrcheck where indischluessel='" + diagnosegruppe + "' LIMIT 1");

        if ((tmpVec.size() <= 0 || diagnosegruppe.equals("")) && (!diagnosegruppe.equals("k.A."))) {
            JOptionPane.showMessageDialog(null,
                    "Diagnosegruppe " + diagnosegruppe + " unbekannt oder nicht angegeben!");
            return false;
        } else if (diagnosegruppe.equals("k.A.")) {
            JOptionPane.showMessageDialog(null,
                    "Diagnosegruppe " + diagnosegruppe + " (keine Angaben) wurde gewählt, HMR-Check wird abgebrochen.\n"
                            + "Bitte stellen Sie selbst sicher daß alle übrigen Pflichtangaben vorhanden sind");
            return true;
        }
        diagnoseTxt = StringTools.cleanupIcdMarker(diagnoseTxt);
        if (icd10_1.equals("") && icd10_2.equals("") && diagnoseTxt.equals("")) {
            fehlertext.add("<b>Es wurde <font color='#ff0000'>weder ein ICD10-Code noch ein Diagnosetext eingetragen</font> - so wird das nix!<br><br></b>");
            testok = false;
        }
        Vector<String> vec = tmpVec.get(0);
        maxproStdVO = Integer.parseInt(vec.get(IDX_MAX_PRO_VO));
        maxprofall = Integer.parseInt(vec.get(IDX_MAX_PRO_FALL));
        String[] vorrHM = vec.get(IDX_VORR_HM)
                                .split("@");
        String[] ergHM = vec.get(IDX_ERG_HM)
                                 .split("@");
        String prefix = diszis.getPrefix(disziIdx);
        HmPosList listeVorrangigeHM = new HmPosList(vorrHM, prefix);
        HmPosList listeErgaenzendeHM = new HmPosList(ergHM, prefix);

        // Behandlungszahlen zum HM (mainly stolen^h^h^h borrowed from org.therapi.hmrCheck2021.HMRCheck2021.jar)
        int[] erlaubte = null;
        int maxAge = 0;
        int minAge = 0;
        int akutLimit = 0;
        String akutUnit = null;
        String zweiterIcdCode = null;
        String hinweisTxt = null;
        String beschraenktAuf = null;
        boolean isLfBedarf = false;
        
        String kap = Reha.hmrXML.getKapitelFromHMap(disziKurz);
        ExcludePosList excludePos = new ExcludePosList (prefix);
        if (AktuelleRezepte.isDentist2020(this.diagnosegruppe)) {
            // aus DB gelesene Werte gelten
            maxprorezept = maxproStdVO;
        } else {
            erlaubte = Reha.hmrXML.getAnzahl(kap, this.diagnosegruppe);
            maxproStdVO = erlaubte[Reha.hmrXML.cVOMEN];
            ArrayList<String> tmpList = Reha.hmrXML.getErlaubteVorrangigeHM(kap, this.diagnosegruppe);
            if (diszis.currIsPodo() && tmpList.contains("Nagelspangenbehandlung")) {
                // aus DB gelesene Werte gelten (keine HM-Codes angegeben)
                System.out.println("Podo Nagelspangenbehandlung, vorr: " + listeVorrangigeHM + "   erg: " + listeErgaenzendeHM);
            } else {
                listeVorrangigeHM = new HmPosList(tmpList);
                listeVorrangigeHM.setPrefix(prefix);
                tmpList = Reha.hmrXML.getErlaubteErgaenzendeHM(kap, this.diagnosegruppe);;
                listeErgaenzendeHM = new HmPosList(tmpList);
                listeErgaenzendeHM.setPrefix(prefix);
            }

            // testen, ob BVB / LHM (ändert maxprorezept je nach max. Behandlungsfrequenz)
            kap = Reha.hmrLfBed.getKapitelFromHMap(disziKurz);
            isLfBedarf = checkLfBedarf( kap, this.diagnosegruppe, icd10_1, icd10_2);
            // Einschränkungen checken!
            zweiterIcdCode = Reha.hmrLfBed.getSecCode();
            maxAge = Reha.hmrLfBed.getUpperLimit();
            minAge = Reha.hmrLfBed.getLowerLimit();
            akutLimit = Reha.hmrLfBed.getAkutLimit();
            if (akutLimit > 0) {
                akutUnit = Reha.hmrLfBed.getAkutUnit();
            }
            hinweisTxt = Reha.hmrLfBed.getHint();
            beschraenktAuf = Reha.hmrLfBed.getValid();

            if (maxAge > 0 || minAge > 0) {
                geburtstag = SqlInfo.holeEinzelFeld("select geboren from pat5 where pat_intern='" + patId + "' LIMIT 1");
                geburtstag = DatFunk.sDatInDeutsch(geburtstag);
                if ((maxAge > 0) && !(DatFunk.hoechstAlter(rezdatum, geburtstag, maxAge))) {
                    isLfBedarf = false;
                }
                if ((minAge > 0) && !(DatFunk.mindestAlter(rezdatum, geburtstag, minAge))) {
                    isLfBedarf = false;
                }
            }

            if (isLfBedarf) {
                maxprorezept = behFreqMax * 12;
                String art = Reha.hmrLfBed.getTypeOfVoBedarf();
                switch (art) {
                case "BVB":
                    typeOfBedarf = VerordnungsArten.BES_VO_BEDARF;
                    break;
                case "LHM":
                    typeOfBedarf = VerordnungsArten.LANGFRIST_VO;
                    break;
                }
            } else {
                maxprorezept = maxproStdVO;
            }
        }
        System.out.println("max. Beh.-Frequenz: " + behFreqMax + "   langfrist: " + (isLfBedarf ? "ja" : "nein") + "   max/VO: " + maxprorezept);

        if (diszis.currIsPodo() && ("L60.0".equals(icd10_1) || "L60.0".equals(icd10_2))) {
            // Anpassung für > 4 HM-Pos notwendig
            // Ergänzung von 'immer da'-Positionen mit fixer Anzahl (Anpassung, Fertigung, ...) auf Kosten der Anz. des eindeutigen vorr. HM
            Nagelspangenbehandlung l60pos = new Nagelspangenbehandlung(positionenVorr, anzahl);
            positionenVorr = l60pos.getAllVorr();
        }
        // test auf erlaubte HM
        int anzVorr = positionenVorr.size();
        for (int i = 0; i < anzVorr; i++) {
            String currPos = positionenVorr.get(i);
            boolean isVorrHM = checkIsVorrHM(preisvec, currPos);
            boolean vorrangigErlaubt = listeVorrangigeHM.contains(currPos);
            boolean ergaenzendErlaubt = listeErgaenzendeHM.contains(currPos);
            boolean isoliertErlaubt = checkIsoliertErlaubt(preisvec, currPos);

            if (!vorrangigErlaubt) {
                if (isVorrHM) {
                    String[] vorrangig = (String[]) listeVorrangigeHM.toArray(new String[0]);
                    fehlertext.add(getDialogText(true, getHeilmittel(currPos), currPos, vorrangig));
                    testok = false;
                }
                if (ergaenzendErlaubt) {
                    if (isoliertErlaubt && (anzVorr == 1)) {
                        // ergänzendes HM darf isoliert verordnet werden
                        // (betrifft ET,EST,US)
                    } else {
                        fehlertext.add("<b>Das  <font color='#ff0000'>ergänzende Heilmittel</font> " + getHeilmittel(currPos)
                                + "<br>wurde  <font color='#ff0000'>als vorrangiges Heilmittel eingetragen</font>.<br><br>");
                        testok = false;
                    }
                }
            }
        }

        // mögliche Höchstmenge pro Rezept wurde überschritten?
        int anzBehVorr = 0;
        int anzBehErg = anzahl.get(IDX_HM_ERG);
        for (int i = 0; i < positionenVorr.size(); i++) {
            if (!excludePos.contains(positionenVorr.get(i))) { // Funktions-, Bedarfsanalyse etc werden nicht mitgezählt
                anzBehVorr += anzahl.get(i);
            }
        }
        String typeOfCurrVO = VerordnungsArten.getTypeOfVo(idxArtDerVO);
        if ((anzBehVorr > maxproStdVO) && (isLfBedarf)) {
            if (!typeOfCurrVO.equals(typeOfBedarf)) {
                fehlertext.add("<b>Bei Diagnosegruppe " + diagnosegruppe + " sind für eine Standard-VO"
                        + " <font color='#ff0000'>maximal " + Integer.toString(maxproStdVO)
                        + " Behandlungen</font> <br>pro Rezept erlaubt!!<br>"
                        + "Wechsel der Verordnungsart zu <font color='#ff0000'>" + typeOfBedarf
                        + "</font> ist lt. ICD-Code(s) möglich und notwendig.</b>");
                if (hinweisTxt != null && (hinweisTxt.length() > 0)) {
                    fehlertext.add("<br><br><b>Hinweis: " + hinweisTxt + "</b>");
                }
                if (beschraenktAuf != null && (beschraenktAuf.length() > 0)) {
                    fehlertext.add("<br><br><b> <font color='#ff0000'>Achtung regional beschränkt: </font></b> <br>"
                            + "<b>Nur LHM/BVB</b> wenn der ausstellende Arzt seinen Sitz <b>in " + beschraenktAuf + "</b> hat!");
                }
                fehlertext.add("<br><br>");
                testok = false;
            }
        }
        if ((anzBehErg > maxprorezept) || (anzBehVorr > maxprorezept)) {
            fehlertext.add("<b>Bei Diagnosegruppe " + diagnosegruppe + " sind für eine "
                    + (typeOfBedarf != null ? typeOfBedarf : typeOfCurrVO));
                    if (diszis.currIsPodo()) {
                        fehlertext.add(" mit einem max. Behandlungsabstand von " + Integer.toString(behFreqMax) + " Wochen");
                    } else {
                        fehlertext.add(" mit der max. Behandlungsfrequenz von " + Integer.toString(behFreqMax) + "/Woche");                        
                    }
                    fehlertext.add("<br>pro Rezept <font color='#ff0000'>maximal " + Integer.toString(maxprorezept)
                    + " Behandlungen</font> erlaubt!!<br>" + " Im Rezept angelegt: "
                    + Integer.max(anzBehErg, anzBehVorr) + " Behandlungen.<br><br></b>");
            testok = false;
        }

        if (anzBehErg > anzBehVorr) {
            fehlertext.add("<b>Es sind maximal so viele ergänzende wie "
                    + "vorrangige Behandlungen pro Rezept erlaubt!!<br><br></b>");
            testok = false;
        }

        try {
            // test auf Doppelbehandlung
            if (positionenAll.size() >= 2) {
                if (positionenAll.get(0)
                              .equals(positionenAll.get(1))) { // sind auch pos2 + pos3 mögl.?
                    doppelbehandlung = true;
                    int doppelgesamt = anzahl.get(0) + anzahl.get(1);
                    if (doppelgesamt > maxprorezept) {
                        fehlertext.add("<b>Die Doppelbehandlung bei Diagnosegruppe "    // und ICD??
                                + diagnosegruppe
                                + ", übersteigt<br>die maximal erlaubte Höchstverordnungsmenge pro Rezept von<br><font color='#ff0000'>"
                                + Integer.toString(maxprorezept) + " Behandlungen</font>!!<br><br>");
                        testok = false;
                    }
                }
            }

            // test Rezeptbeginn
            long differenz = DatFunk.TageDifferenz(rezdatum, letztbeginn);
            if (dringlich && (differenz > FRIST_DRINGLICH_HMR2020)) {
                fehlertext.add("<br><b><font color='#ff0000'>Dringlicher Behandlungsbedarf</font> angegeben. <br><br>"
                        + "Die <font color='#ff0000'>Spanne zwischen Verordnungsdatum und spätestem Behandlungsbeginn</font> <br>beträgt jedoch "
                        + "<font color='#ff0000'>" + Long.toString(differenz) + " Tag(e) </font>!!<br><br>");
                testok = false;
            }
            String cmd = "select termine from verordn where rez_nr='" + reznummer + "' LIMIT 1";
            String termine = SqlInfo.holeFeld(cmd)
                                    .get(0);
            Vector<String> vtagetest = new Vector<String>();
            if (!termine.trim()
                        .equals("")) {
                vtagetest = RezTools.holeEinzelTermineAusRezept(null, termine);
            }
            // test Entl.-Mngmnt
            if (typeOfCurrVO.equals(VerordnungsArten.ENTLASS_MNGMNT)) {
                fehlertext = new FehlerTxt();   // bisher. Tests sind hinfällig
                testok = true;
                isLfBedarf = false;
                if (differenz > VerordnungsArten.FRIST_ENTL_MNGMNT_START) {
                    fehlertext.add("<br><b><font color='#ff0000'>Entlassungsmanagement</font> angegeben. <br><br>"
                            + "Die <font color='#ff0000'>Spanne zwischen Verordnungsdatum und spätestem Behandlungsbeginn</font> <br>beträgt jedoch "
                            + "<font color='#ff0000'>" + Long.toString(differenz) + " Tag(e) </font>!!<br>"
                            + "Erlaubt sind max. " + VerordnungsArten.FRIST_ENTL_MNGMNT_START + " Tage<br><br>");
                    testok = false;
                }
                if (!vtagetest.isEmpty()) {
                    String letzteBehandlung = vtagetest.lastElement();
                    int differenzEnd = (int) DatFunk.TageDifferenz(rezdatum, letzteBehandlung);
                    if (differenzEnd > VerordnungsArten.FRIST_ENTL_MNGMNT_END) {
                        String ultimo = DatFunk.sDatPlusTage(rezdatum, VerordnungsArten.FRIST_ENTL_MNGMNT_END);
                        fehlertext.add("<br><b><font color='#ff0000'>Entlassungsmanagement</font> angegeben. <br><br>"
                                + "Die <font color='#ff0000'>Spanne zwischen Verordnungsdatum und letzter Behandlung</font> <br>beträgt jedoch "
                                + "<font color='#ff0000'>" + Long.toString(differenzEnd) + " Tag(e)</font>!!<br>"
                                + "Die Behandlungen müssen bis pätestens zum " + ultimo + " erbracht sein.<br><br>");
                        testok = false;
                    }
                }
            }
            
            // test Anzahl Termine
            if (RezTools.checkIsGKV(rezept.getRezNb())) {
                int maxTermine = anzahl.get(0) + anzahl.get(1) + anzahl.get(2);
                if (vtagetest.size() > maxTermine){
                    fehlertext.add("<b>Die Anzahl der Termine übersteigt die <br>"
                            + "<font color='#ff0000'>Höchstverordnungsmenge von " + Integer.toString(maxprorezept) + " Behandlungen</font><br>"
                            + " pro Rezept!!<br><br>");
                    testok = false;
                    Vector<String> behandlungen = RezTools.holeEinzelZiffernAusRezept(null, termine);
                    String [] behToday = null;
                    String currPos = null;
                    for (int idx=0; idx < vtagetest.size() ; idx++){
                        behToday = (behandlungen.get(idx).split(","));
                        if (behToday.length == 1) {
                            currPos = behToday[0];
                            boolean vorrangigErlaubt = listeVorrangigeHM.contains(currPos);
                            boolean ergaenzendErlaubt = listeErgaenzendeHM.contains(currPos);
                            if (ergaenzendErlaubt && !vorrangigErlaubt) {
                                fehlertext.add("<b>Das  <font color='#ff0000'>ergänzende Heilmittel</font> " + getHeilmittel(behToday[0])
                                + "<br>wurde  <font color='#ff0000'>als vorrangiges Heilmittel eingetragen</font>.<br><br>");
                                break;
                            }
                        }
                    }
                }
            }
            
            differenz = DatFunk.TageDifferenz(rezdatum, DatFunk.sHeute());
            if (neurezept) {
                if (differenz < 0) {
                    fehlertext.add("<br><b><font color='#ff0000'>Rezeptdatum ist absolut kritisch!</font><br>Spanne zwischen Behandlungsbeginn und Rezeptdatum beträgt <font color='#ff0000'>"
                            + Long.toString(differenz)
                            + " Tag(e) </font>.<br>Behandlungsbeginn ist also <font color='#ff0000'>vor</font> dem  Ausstellungsdatum!!</b><br><br>");
                    testok = false;
                }
                if ((differenz = DatFunk.TageDifferenz(letztbeginn, DatFunk.sHeute())) > 0) {
                    // System.out.println("Differenz 2 = "+differenz);
                    fehlertext.add("<br><b><font color='#ff0000'>Behandlungsbeginn ist kritisch!</font><br><br>Die Differenz zwischen <font color='#ff0000'>spätestem Behandlungsbeginn</font> und 1.Behandlung<br>beträgt <font color='#ff0000'>"
                            + Long.toString(differenz) + " Tag(e) </font><br>" + "</b><br><br>");
                    testok = false;
                }
            } else {
                // Keine Termine notiert
                if (termine.trim()
                           .equals("")) {
                    // LetzterBeginn abhandeln
                    if (differenz < 0) {
                        fehlertext.add("<br><b><font color='#ff0000'>Rezeptdatum ist absolut kritisch!</font><br>Spanne zwischen Behandlungsbeginn und Rezeptdatum beträgt <font color='#ff0000'>"
                                + Long.toString(differenz)
                                + " Tag(e) </font>.<br>Behandlungsbeginn ist also <font color='#ff0000'>vor</font> dem  Ausstellungsdatum!!</b><br><br>");
                        testok = false;
                    }
                    if ((differenz = DatFunk.TageDifferenz(letztbeginn, DatFunk.sHeute())) > 0) {
                        // System.out.println("Differenz 2 = "+differenz);
                        fehlertext.add("<br><b><font color='#ff0000'>Behandlungsbeginn ist kritisch!</font><br><br>Die Differenz zwischen <font color='#ff0000'>spätestem Behandlungsbeginn</font> und 1.Behandlung<br>beträgt <font color='#ff0000'>"
                                + Long.toString(differenz) + " Tag(e) </font><br>" + "</b><br><br>");
                        testok = false;
                    }

                } else {
                    // LetzterBeginn abhandeln
                    String erstbehandlung = RezTools.holeEinzelTermineAusRezept(null, termine)
                                                    .get(0);
                    differenz = DatFunk.TageDifferenz(rezdatum, erstbehandlung);
                    if (differenz < 0) {
                        fehlertext.add("<br><b><font color='#ff0000'>Rezeptdatum ist absolut kritisch!</font><br>Spanne zwischen Behandlungsbeginn und Rezeptdatum beträgt <font color='#ff0000'>"
                                + Long.toString(differenz)
                                + " Tag(e) </font>.<br>Behandlungsbeginn ist also <font color='#ff0000'>vor</font> dem  Ausstellungsdatum!!</b><br><br>");
                        testok = false;
                    }
                    if ((differenz = DatFunk.TageDifferenz(letztbeginn, erstbehandlung)) > 0) {
                        // System.out.println("Differenz 2 = "+differenz);
                        fehlertext.add("<br><b><font color='#ff0000'>Behandlungsbeginn ist kritisch!</font><br><br>Die Differenz zwischen <font color='#ff0000'>spätestem Behandlungsbeginn</font> und 1.Behandlung<br>beträgt <font color='#ff0000'>"
                                + Long.toString(differenz) + " Tag(e) </font><br>" + "</b><br><br>");
                        testok = false;
                    }
                    // Test auf Anregung von Michael Schütt
                    for (int i = 0; i < vtagetest.size(); i++) {
                        if ((differenz = DatFunk.TageDifferenz(vtagetest.get(i), DatFunk.sHeute())) < 0) {
                            fehlertext.add("<br><b><font color='#ff0000'>Behandlungsdatum " + vtagetest.get(i)
                                    + " ist kritisch!</font><br><br>"
                                    + "Das Behandlungsdatum liegt um <font color='#ff0000'>" + Long.toString(differenz * -1)
                                    + " Tage </font>in der Zukunft</b><br><br>");
                            testok = false;
                        }
                    }

                }
            }
            // VO abgelaufen?
            if (RezTools.hasTermineNachAblauf(this.rezept)) {
                testok = false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (!testok) {
            if (isLfBedarf 
                    && !(typeOfCurrVO.equals(typeOfBedarf)) 
                    && anzBehVorr > maxproStdVO) {  // oder doch immer umstellen? (bringt das was?)
                String calledFrom = parent.getClass().toString(); // ...AktuelleRezepte / ...RezNeuanlage2020
                if (calledFrom.contains("RezNeuanlage2020")) {
                    fehlertext.add("<br><b>Soll die Verordnungsart jetzt auf " + typeOfBedarf + " geändert werden?</b><br>");
                    int anfrage = JOptionPane.showConfirmDialog(null, fehlertext.getTxt(),
                            "Meldung", JOptionPane.YES_NO_OPTION);
                    if (anfrage == JOptionPane.YES_OPTION) {
                        // Dropbox passend einstellen
                        ((RezNeuanlage2020) caller).changeVoArt(typeOfBedarf);
                    }
                } else {
                    fehlertext.add("<br><b>Bitte stellen Sie die Verordnungsart in der Eingabemaske auf " + typeOfBedarf + " um.</b><br>");
                    JOptionPane.showMessageDialog(null, fehlertext.getTxt());
                }
            } else {    // (!isLfBedarf || currVO.equals(typeOfBedarf) || caller.equals(null))
                if (!fehlertext.getTxt().isEmpty()) {
                    JOptionPane.showMessageDialog(null, fehlertext.getTxt());                
                }
            }
        }
        return testok;
    }

    private boolean checkLfBedarf(String kap, String diagnosegruppe2, String icd10_1, String icd10_2) {
        boolean isLfBedarf = false;
        String icd1 = (icd10_1 == null ? "" : icd10_1);
        String icd2 = (icd10_2 == null ? "" : icd10_2);
        if (!("".equals(icd1))) {
            if (!("".equals(icd2))) {
                isLfBedarf = Reha.hmrLfBed.isBesVoBedOrLangfristBed(icd10_1, icd10_2, kap, this.diagnosegruppe);
                if (isLfBedarf == false) {
                    isLfBedarf = Reha.hmrLfBed.isBesVoBedOrLangfristBed(icd10_2, kap, this.diagnosegruppe);  
                    if (isLfBedarf == false) {
                        isLfBedarf = Reha.hmrLfBed.isBesVoBedOrLangfristBed(icd10_2, icd10_1, kap, this.diagnosegruppe);  
                        if (isLfBedarf == true) {
                            String art = (Reha.hmrLfBed.getTypeOfVoBedarf().equals("BVB") ? "'besonderen Verordnungsbedarf'" : "'langfristigen Heilmittelbedarf'");
                            fehlertext.add("<b>Die ICD10-Codes begründen einen " + art + ".<br>"
                                    + "Allerdings sollte <font color='#ff0000'>" + icd10_2 + "</font> als"
                                    + " <font color='#ff0000'>erster Code</font> angegeben werden!</b><br><br>");
                            testok = false;
                        }
                    }
                }
            } else {
                isLfBedarf = Reha.hmrLfBed.isBesVoBedOrLangfristBed(icd10_1, kap, this.diagnosegruppe);                            
            }
        } else if (!("".equals(icd2))) {
            isLfBedarf = Reha.hmrLfBed.isBesVoBedOrLangfristBed(icd10_2, kap, this.diagnosegruppe);                                        
        }
        return isLfBedarf;
    }

    private String[] addPrefix(String[] hmPos, String prefix) {
        String[] tmpArr = new String[hmPos.length];
        String tmpStr = null;
        for (int i = 0; i < hmPos.length; i++) {
            tmpStr = prefix + hmPos[i];
            tmpArr[i] = tmpStr;
        }
        return tmpArr;
    }

    private ArrayList<String> matchPrefix(ArrayList<String> hmPos, String prefix) {
        ArrayList<String> tmpList = new ArrayList<String>();
        String tmpStr = null;
        for (int i = 0; i < hmPos.size(); i++) {
            tmpStr = hmPos.get(i).replace("X", prefix);
            if (!tmpList.contains(tmpStr)) {
                tmpList.add(tmpStr);
            }
        }
        return tmpList;
    }

    private boolean[] checkVorrErgInPl(Vector<Vector<String>> preisvec, String currPos) {
        boolean[] vorrUisoliert = new boolean[]{false,false};
        for (int j = 0; j < preisvec.size(); j++) {
            Vector<String> currVec = preisvec.get(j);
            if (currPos.equals(currVec.get(2))) {
               vorrUisoliert = stammDatenTools.RezTools.isVorrangigAndExtra(currVec.get(1),
                        diszis.getRezClass(disziIdx));
                break;
            }
        }
        return vorrUisoliert;
    }
    
    private boolean checkIsoliertErlaubt(Vector<Vector<String>> preisvec, String currPos) {
        boolean[] vorrUisoliert = checkVorrErgInPl( preisvec, currPos );
        return vorrUisoliert[1];
    }

    private boolean checkIsVorrHM(Vector<Vector<String>> preisvec, String currPos) {
        boolean[] vorrUisoliert = checkVorrErgInPl( preisvec, currPos );
        return vorrUisoliert[0];
    }

    private String getDialogText(boolean vorrangig, String heilmittel, String hmpos, String[] positionen) {
        String meldung = "Bei der Diagnosegruppe <b><font color='#ff0000'>" + diagnosegruppe + "</font></b> ist das "
                + (vorrangig ? "vorrangige " : "ergänzende") + " Heilmittel<br><br>--> <b><font color='#ff0000'>"
                + heilmittel + "</font></b> <-- nicht erlaubt!<br><br><br>" + "Mögliche "
                + (vorrangig ? "vorrangige " : "ergänzende") + " Heilmittel sind:<br><b><font color='#ff0000'>"
                + getErlaubteHeilmittel(positionen) + "</font></b><br><br>";
        return meldung;

    }

    /************************/
    private String getErlaubteHeilmittel(String[] heilmittel) {
        StringBuffer buf = new StringBuffer();
        String hm = "";
        for (int i = 0; i < heilmittel.length; i++) {
            hm = getHeilmittel(heilmittel[i]);
            if (!hm.equals("")) {
                buf.append(getHeilmittel(heilmittel[i]) + "<br>");
            }
        }
        return (buf.toString()
                   .equals("") ? "<br>keine<br>" : buf.toString());
    }

    /************************/
    private String getHeilmittel(String heilmittel) {
        for (int i = 0; i < preisvec.size(); i++) {
            if (preisvec.get(i)
                        .get(2)
                        .equals(heilmittel)) {
                return preisvec.get(i)
                               .get(0);
            }
        }
        return "";
    }


    public static int hmrTageDifferenz(String referenzdatum, String vergleichsdatum, int differenz,
            boolean samstagistwerktag) {
        int ret = 1;
        try {
            String letztesdatum = hmrLetztesDatum(referenzdatum, differenz, samstagistwerktag);
            ret = Integer.parseInt(Long.toString(DatFunk.TageDifferenz(letztesdatum, vergleichsdatum)));
        } catch (Exception ex) {
            System.out.println("Fehler in der Ermittlung der Unterbrechungszeiträume");
            ex.printStackTrace();
        }
        return ret;
    }

    public static String hmrLetztesDatum(String startdatum, int differenz, boolean samstagistwerktag) {

        int werktage = 0;
        Date date = null;


        try {
            date = sdDeutsch.parse(startdatum);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        while (true) {
            // System.out.println("Getestetes Datum = "+sd.format(date));
            if ((!(date.getDay() % 7 == 0)) && (samstagistwerktag)) {
                if (!istFeiertag(date)) {
                    if (werktage == differenz) {
                        return sdDeutsch.format(date);
                    }
                    werktage++;
                }
            } else if ((!(date.getDay() % 7 == 0)) && (!samstagistwerktag) && (!(date.getDay() % 6 == 0))) {
                if (!istFeiertag(date)) {
                    if (werktage == differenz) {
                        return sdDeutsch.format(date);
                    }
                    werktage++;
                }
            }

            date = new Date(date.getTime() + (24 * 60 * 60 * 1000));
        }
    }


    public static boolean istFeiertag(Date date) {

        if (SystemConfig.vFeiertage.contains(sdSql.format(date))) {
            return true;
        }
        return false;
    }

    private String pauseText (String rezNb, long pause) {
        return "Therapiepause <font color='#ff0000'>vor " + rezNb + "</font> beträgt <font color='#ff0000'>"
                + pause + "</font> Tage.</b><br>";
    }

    public static int getFristDringlich() {
        return FRIST_DRINGLICH_HMR2020;
    }

}