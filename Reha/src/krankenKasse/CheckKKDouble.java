package krankenKasse;

import java.util.Vector;

import javax.swing.JOptionPane;

import CommonTools.SqlInfo;

public class CheckKKDouble {
    String kassId = null;
    String name1 = null;
    String name2 = null;
    String ikKasse = null;
    Boolean ikIstVergeben = false;

    public CheckKKDouble(String ik) {
        Vector<Vector<String>> kassDat = SqlInfo.holeFelder(
                "select id, kassen_nam1, kassen_nam2, ik_kasse from kass_adr where ik_kasse='" + ik + "' LIMIT 1");         // TODO: was ist mit mehreren Treffern?
        if (kassDat.size() > 0) {
            ikIstVergeben = true;
            extractFields(kassDat.get(0));
        }
    }

    private void extractFields (Vector<String> result) {
        kassId = result.get(0);
        name1 = result.get(1);
        name2 = result.get(2);
        ikKasse = result.get(3);
    }
    
    public boolean updateDouble() {
        if (kassId != null) {
            String tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            String end = "<br>";
            String meldung = "<html>Es existiert bereits ein Kasseneintrag mit folgenden Daten:<b>" + end
                    + tab + "</b>Name:<b>&nbsp;&nbsp;" + name1 + ", " + name2 + end
                    + tab + "</b>IK:<b>&nbsp;&nbsp;" + ikKasse + end + "</html>\n";
            if (ikIstVergeben) {
                meldung = meldung + "<html><font color=#FF0000><b>Doppelte IK sind nicht erlaubt!</b></font>" + end
                    + tab + "Die Daten der vorhandenen Kasse werden jetzt geöffnet.</html>";
                JOptionPane.showMessageDialog(null, meldung);
                return true;
            }
        }
        return false;
    }
    
    public String getKassId () {
        return kassId;
    }
}