package krankenKasse;

import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;

import org.jdesktop.swingx.JXPanel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import CommonTools.JCompTools;
import CommonTools.JRtaComboBox;
import CommonTools.JRtaTextField;
import CommonTools.SqlInfo;
import hauptFenster.AktiveFenster;
import hauptFenster.Reha;
import rehaInternalFrame.JArztInternal;
import systemEinstellungen.SystemConfig;
import systemEinstellungen.SystemPreislisten;
import rehaInternalFrame.JKasseInternal;


public class KasseNeuanlage extends JXPanel implements ActionListener, KeyListener, FocusListener {
    /**
     * 
     */
    private static final long serialVersionUID = 7499352308908305654L;
    Vector<String> kasDaten = null;
    String kassenId = "";
    JRtaComboBox tarifGruppe = null;
    ImageIcon hgicon;
    int icx, icy;
    AlphaComposite xac1 = null;
    AlphaComposite xac2 = null;
    Object eltern = null;
    boolean neuAnlage;
    boolean ohneKuerzel = false;
    public JRtaTextField[] jtf = { null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null };

    private static final int IDX_TF_KUERZEL = 0;
    private static final int IDX_TF_PG = 1;
    private static final int IDX_TF_NAME1 = 2;
    private static final int IDX_TF_NAME2 = 3;
    private static final int IDX_TF_STRASSE = 4;
    private static final int IDX_TF_PLZ = 5;
    private static final int IDX_TF_ORT = 6;
    private static final int IDX_TF_TEL = 7;
    private static final int IDX_TF_FAX = 8;
    private static final int IDX_TF_EMAIL1 = 9;
    private static final int IDX_TF_EMAIL2 = 10;
    private static final int IDX_TF_EMAIL3 = 11;
    private static final int IDX_TF_KV_NUMMER = 12;
    private static final int IDX_TF_IK_KASSE = 13;
    private static final int IDX_TF_IK_KOSTENT = 14;
    private static final int IDX_TF_IK_DATENANN= 15;
    private static final int IDX_TF_IK_NUTZER = 16;
    private static final int IDX_TF_IK_PAPIER = 17;

JButton knopf1 = null;
    JButton knopf4 = null;
    JButton knopf5 = null;
    int[] fedits = { 0, 2, 3, 4, 5, 6, 7, 8, 9, 13, 14, 15, 16, 17, 12 };
    int[] ffelder = { 0, 2, 3, 4, 5, 6, 9, 8, 20, 14, 17, 15, 16, 19, 11 };
    KassenPanel kpan;
    JLabel labKtraeger = null;
    JLabel labKuerzel = null;

    boolean mitButton = false;
    private Logger logger = LoggerFactory.getLogger(KasseNeuanlage.class);

    public KasseNeuanlage(Object eltern, KassenPanel xkpan, Vector<String> vec, String id) {
        super();
        setBackgroundPainter(Reha.instance.compoundPainter.get("KasseNeuanlage"));
        this.kasDaten = vec;
        this.kassenId = id;
        this.eltern = eltern;
        kpan = xkpan;
        if (id.equals("")) {
            this.neuAnlage = true;
        } else {
            this.neuAnlage = false;
        }

        hgicon = Reha.rehaBackImg;
        icx = hgicon.getIconWidth() / 2;
        icy = hgicon.getIconHeight() / 2;
        xac1 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.07f);
        xac2 = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f);
        this.setDoubleBuffered(true);

        this.setLayout(new BorderLayout());

        add(getDatenPanel(), BorderLayout.CENTER);
        add(getButtonPanel(), BorderLayout.SOUTH);
        if (!this.neuAnlage) {
            new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    fuelleFelder();
                    return null;
                }

            }.execute();
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        if (hgicon != null) {
            g2d.setComposite(this.xac1);
            g2d.drawImage(hgicon.getImage(), (getWidth() / 2) - icx, (getHeight() / 2) - icy, null);
            g2d.setComposite(this.xac2);
        }
    }

    public void setzeFocus() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                jtf[IDX_TF_KUERZEL].requestFocusInWindow();
            }
        });
    }

    public void fensterSchliessen() {
        ((JDialog) this.eltern).dispose();
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {

        String comm = arg0.getActionCommand();
        final String xcomm = comm;
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (xcomm.equals("speichern")) {
                    datenSpeichern();
                    tabelleAktualisieren();
                    fensterSchliessen();
                } else if (xcomm.equals("abbrechen")) {
                    fensterSchliessen();
                } else if (xcomm.equals("vergleichKT")) {
                    doVergleichKT();
                }
            }
        });
    }

    private void doVergleichKT() {
        boolean validIKNummer = false;
        String iKNummer = "";
        JRtaTextField kVNummer;
        kVNummer = new JRtaTextField("ZAHLEN", true);
        if (neuAnlage == true || jtf[IDX_TF_IK_KASSE].getText()
                                                     .trim()
                                                     .equals("")) {

            kVNummer.setText(JOptionPane.showInputDialog(null,
                    "<html>Krankenkassennummer laut Rezept<br>" + "bitte <b>7-stellige</b> Zahl eingeben</html>",
                    "KV-Nummer eingeben", JOptionPane.OK_CANCEL_OPTION));

            if (!kVNummer.getText()
                         .equals("")) {
                if ((kVNummer.getText()
                             .length() < 7)
                        && kVNummer.getText()
                                   .length() != 9) {
                    JOptionPane.showMessageDialog(null, "<html>die KV-Nummer <b>muss siebenstellig</b> sein</html>");
                } else if (kVNummer.getText()
                                   .length() == 9) {
                    iKNummer = kVNummer.getText();
                    validIKNummer = true;
                } else if (kVNummer.getText()
                                   .length() == 7) {
                    iKNummer = "10" + kVNummer.getText();
                    validIKNummer = true;
                } else {
                    JOptionPane.showMessageDialog(null, "<html>die KV-Nummer <b>muss siebenstellig</b> sein</html>");
                }
            }
        } else /* neuAnlage == false */ {
            iKNummer.equals(jtf[IDX_TF_IK_KASSE].getText());
            validIKNummer = true;
        }
        if (validIKNummer) {
            if (jtf[IDX_TF_IK_KASSE].getText()
                                    .equals("")) {
                jtf[IDX_TF_IK_KASSE].setForeground(Color.BLUE);
                jtf[IDX_TF_IK_KASSE].setText(iKNummer);
                jtf[IDX_TF_KV_NUMMER].setForeground(Color.BLUE);
                jtf[IDX_TF_KV_NUMMER].setText(kVNummer.getText());
            }
            ktraegerAuslesen();
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                jtf[IDX_TF_KUERZEL].requestFocus();
            }
        });
    }

    @Override
    public void keyPressed(KeyEvent arg0) {
        if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
            arg0.consume();
            if (((JComponent) arg0.getSource()).getName()
                                               .equals("speichern")) {
                datenSpeichern();
                tabelleAktualisieren();
                fensterSchliessen();
            }
            if (((JComponent) arg0.getSource()).getName()
                                               .equals("abbrechen")) {
                fensterSchliessen();
            }
        }
        if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
            fensterSchliessen();
        }
        if (((JComponent) arg0.getSource()).getName()
                                           .equals("KUERZEL")) {
            if (arg0.getKeyChar() == '?') {
                doVergleichKT();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent arg0) {
        if (((JComponent) arg0.getSource()).getName()
                                           .equals("KUERZEL")) {
            jtf[IDX_TF_KUERZEL].setText(jtf[IDX_TF_KUERZEL].getText()
                                                        .replace("?", ""));
        }
    }

    @Override
    public void keyTyped(KeyEvent arg0) {

    }

    @Override
    public void focusGained(FocusEvent arg0) {

        if (((JComponent) arg0.getSource()).getName()
                                           .equals("TARIFGRUPPE")) {

            if (this.neuAnlage && (!jtf[IDX_TF_KUERZEL].getText()
                                                       .trim()
                                                       .equals("-"))) {
                // System.out.println("Kürzel = "+jtf[IDX_TF_KUERZEL].getText());
                new SwingWorker<Void, Void>() {
                    @Override
                    protected Void doInBackground() throws Exception {
                        String kurz = jtf[IDX_TF_KUERZEL].getText()
                                                      .trim()
                                                      .toUpperCase();
                        String stmt = "select id from kass_adr where kuerzel='" + kurz + "'";
                        if (SqlInfo.gibtsSchon(stmt)) {
                            jtf[IDX_TF_KUERZEL].requestFocus();
                            JOptionPane.showMessageDialog(null,
                                    "Krankenkasse mit dem Kürzel --> " + kurz + " <-- bereits vorhanden");
                            jtf[IDX_TF_KUERZEL].setText("XXX-YY");
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    jtf[IDX_TF_KUERZEL].requestFocus();
                                }
                            });

                        }
                        return null;
                    }

                }.execute();
            }
        }
    }

    @Override
    public void focusLost(FocusEvent arg0) {
    }

    private void fuelleFelder() {
        List<String> nichtlesen = Arrays.asList(new String[] { "KMEMO" });
        Vector<String> felder = SqlInfo.holeSatz("kass_adr", "*", "id='" + this.kassenId + "'", nichtlesen);
        int gros = felder.size();
        int anzahlf = fedits.length;
        if (gros > 0) {
            for (int i = 0; i < anzahlf; i++) {
                jtf[fedits[i]].setText(felder.get(ffelder[i]));
            }
            int preisG = Integer.valueOf(felder.get(1)) - 1;
            // System.out.println("In Preisgruppe einstellen Preisgruppe = "+preisG);
            tarifGruppe.setSelectedIndex((preisG >= 0 ? preisG : 0));
        }
    }

    public void ktraegerAuslesen() {
        boolean emailaddyok = false;
        if (this.neuAnlage == true) {
            int iid = SqlInfo.holeId("kass_adr", "kmemo");
            if (iid == -1) {
                JOptionPane.showMessageDialog(null,
                        "Fehler beim Anlegen einer neuen Kasse, bitte erneut versuchen -> speichern");
                return;
            }
            this.kassenId = Integer.toString(iid);
        }
        List<String> nichtlesen = Arrays.asList(new String[] { "KMEMO" });
        Vector<String> felder = SqlInfo.holeSatz("kass_adr", "*", "id='" + this.kassenId + "'", nichtlesen);
        felder.setElementAt(jtf[IDX_TF_IK_KASSE].getText(), 14);
        Vector<String> felder2 = SqlInfo.holeSatz("ktraeger", "*", "ikkasse='" + felder.get(14) + "'", nichtlesen);
        if (felder2.size() <= 0) {
            JOptionPane.showMessageDialog(null,
                    "Kein Eintrag in der Kostenträgerdatei vorhanden für IK=" + felder.get(14));
            return;
        }
        // Wenn Datenannahmestelle fehlt
        if (felder2.get(3)
                   .equals("")) {
            felder2.set(3, KTraegerTools.getDatenIK(felder2.get(1)));
        }
        // Wenn logischer Empfänger (Entschlüsselungsbefugnis fehlt)
        if (felder2.get(4)
                   .equals("")) {
            felder2.set(4, KTraegerTools.getNutzerIK(felder2.get(1)));
        }
        // Wenn Papierannahmestelle fehlt
        if (felder2.get(2)
                   .equals("")) {
            felder2.set(2, KTraegerTools.getPapierIK(felder2.get(1)));
        }
        // Wenn Emailadresse fehlt
        String email = "";
        if (felder2.get(11)
                   .equals("")) {
            // zunächst beim Kostenträger nachsehen
            email = KTraegerTools.getEmailAdresse(felder2.get(1));
            // Falls keine gefunden bei der Datenannahmestelle nachsehen
        } else {
            email = felder2.get(11);
        }
        // Jetzt nachsehen ob die Datenannahmestelle eine Emailadresse hat.
        String email2 = KTraegerTools.getEmailAdresse(felder2.get(3));
        if (!email2.trim()
                   .equals("")) {
            // alles palletti
            // jetzt die Adresse aus dem Vector löschen
            felder2.set(11, "");
            emailaddyok = true;
        } else {
            // wenn in email eine Adresse steht.
            if (!email.equals("")) {
                String cmd = "update ktraeger set email='" + email + "' where ikkasse='" + felder2.get(3) + "' LIMIT 1";
                SqlInfo.sqlAusfuehren(cmd);
                emailaddyok = true;
                felder2.set(11, "");
            } else {
                felder2.set(11, "");
            }
        }
        if ((felder2.get(0)
                    .equals(""))
                || (felder2.get(1)
                           .equals(""))
                || (felder2.get(2)
                           .equals(""))
                || (felder2.get(3)
                           .equals(""))
                || (felder2.get(4)
                           .equals(""))
                || (!emailaddyok)) {
            String htmlMeldung = "<html>Achtung mit den ermittelten Daten kann eine maschinenlesbare Abrechnung<br>"
                    + "nach §302 SGB V <b>nicht durchgeführt</b>werden</html>";
            JOptionPane.showMessageDialog(null, htmlMeldung);
        }

        // i= 0 1 2 3 4 5 6 7 8 9 10
        int[] fjtf = { 13, 14, 17, 15, 16, 2, 3, 5, 6, 4, 9 };
        int[] fktraeger = { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11 };
        if (felder2.size() > 0) {
            for (int i = 0; i < fjtf.length; i++) {
                // Änderungen übertragen außer Emailadresse
                if (!jtf[fjtf[i]].getName()
                                 .equals("EMAIL1")) {
                    if (!jtf[fjtf[i]].getText()
                                     .equals(felder2.get(fktraeger[i]))) {
                        if (jtf[fjtf[i]].getText()
                                        .length() != 0) {
                            jtf[fjtf[i]].setForeground(Color.RED); // Ersetzte Daten rot markieren
                        } else {
                            jtf[fjtf[i]].setForeground(Color.BLUE); // Hinzugefügte Daten blau markieren
                        }
                    }
                    // wenn der neue Inhalt nicht leer ist ansonsten nimm den alten Inhalt
                    jtf[fjtf[i]].setText((!felder2.get(fktraeger[i])
                                                  .equals("") ? (String) felder2.get(fktraeger[i])
                                                          : jtf[fjtf[i]].getText()));
                }
            }
        }
    }

    public void datenSpeichern() {
        // int[] fedits = {0,2,3,4,5,6,7,8,9,13,14,15,16,17};
        // int[] ffelder = {0,2,3,4,5,6,9,8,20,14,17,15,16,19};
        try {
            int anzahlf = fedits.length;
            String dbid = this.kassenId;
            StringBuffer kkBuffer = new StringBuffer();
            // String stmt = "update kass_adr set ";
            kkBuffer.append("update kass_adr set ");
            if (this.neuAnlage) {
                String ikKasse = jtf[IDX_TF_IK_KASSE].getText()
                                                     .trim();
                CheckKKDouble chkDbl = null;
                if (!"".equals(ikKasse)) {
                    logger.info("ikKasse " + ikKasse);

                    chkDbl = new CheckKKDouble(ikKasse);

                    if (chkDbl.updateDouble()) {
                        logger.info("schreibeInDb: KK-Doublette gefunden - wechsle zum Original");
                        final String xkassid = chkDbl.getKassId();

                        new SwingWorker<Void, Void>() {
                            @Override
                            // auf exist. Kasseneintrag umstellen
                            protected Void doInBackground() throws Exception {
                                JComponent kasse = AktiveFenster.getFensterAlle("KrankenKasse");
                                if (kasse != null) {
                                    Reha.containerHandling(((JKasseInternal) kasse).getDesktop());
                                    ((JKasseInternal) kasse).aktiviereDiesenFrame(((JKasseInternal) kasse).getName());
                                    ((JKasseInternal) kasse).starteKasseID(xkassid);
                                    if (((JKasseInternal) kasse).isIcon()) {
                                        try {
                                            ((JKasseInternal) kasse).setIcon(false);
                                        } catch (PropertyVetoException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                                return null;
                            }

                          }.execute();
                          // TODO: re-init form with existing data (?)
                          fensterSchliessen();
                          return;
                    }
                }

                int iid = SqlInfo.holeId("kass_adr", "kmemo");
                if (iid == -1) {
                    JOptionPane.showMessageDialog(null,
                            "Fehler beim Anlegen einer neuen Kasse, bitte erneut versuchen -> speichern");
                    return;
                }
                dbid = Integer.toString(iid);
                this.kassenId = dbid;
            }
            for (int i = 0; i < anzahlf; i++) {

                kkBuffer.append(jtf[fedits[i]].getName() + "='" + jtf[fedits[i]].getText()
                                                                                .trim()
                        + "', ");

            }
            kkBuffer.append("kmemo ='', ");
            kkBuffer.append("preisgruppe ='" + Integer.toString(this.tarifGruppe.getSelectedIndex() + 1) + "', ");
            kkBuffer.append("pgkg ='" + Integer.toString(this.tarifGruppe.getSelectedIndex() + 1) + "', ");
            kkBuffer.append("pgma ='" + Integer.toString(this.tarifGruppe.getSelectedIndex() + 1) + "', ");
            kkBuffer.append("pger ='" + Integer.toString(this.tarifGruppe.getSelectedIndex() + 1) + "', ");
            kkBuffer.append("pglo ='" + Integer.toString(this.tarifGruppe.getSelectedIndex() + 1) + "', ");
            kkBuffer.append("pgrh ='" + Integer.toString(this.tarifGruppe.getSelectedIndex() + 1) + "', ");
            if (SystemConfig.mitRs) {
                kkBuffer.append("pgpo ='" + Integer.toString(this.tarifGruppe.getSelectedIndex() + 1) + "', ");
                kkBuffer.append("pgrs ='" + Integer.toString(this.tarifGruppe.getSelectedIndex() + 1) + "', ");
                kkBuffer.append("pgft ='" + Integer.toString(this.tarifGruppe.getSelectedIndex() + 1) + "' ");
            } else {
                kkBuffer.append("pgpo ='" + Integer.toString(this.tarifGruppe.getSelectedIndex() + 1) + "' ");
            }

            kkBuffer.append("where id='" + dbid + "' LIMIT 1");

            SqlInfo.sqlAusfuehren(kkBuffer.toString());
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Fehler beim Abspeichern der Kasse");
        }

    }

    public void tabelleAktualisieren() {

        List<String> list = Arrays.asList(new String[] { jtf[IDX_TF_KUERZEL].getText(), jtf[IDX_TF_NAME1].getText(), jtf[IDX_TF_NAME2].getText(),
                jtf[IDX_TF_ORT].getText(), jtf[IDX_TF_TEL].getText(), jtf[IDX_TF_FAX].getText(), jtf[IDX_TF_IK_KASSE].getText(), this.kassenId });
        if (this.neuAnlage) {
            Vector<String> vec = new Vector<String>();
            for (int i = 0; i < list.size(); i++) {
                vec.add(list.get(i));
            }
            kpan.ktblm.addRow(vec);
        } else {
            int row = kpan.kassentbl.getSelectedRow();
            int model = kpan.kassentbl.convertRowIndexToModel(row);

            for (int i = 0; i < 8; i++) {
                kpan.ktblm.setValueAt(list.get(i), model, i);
            }
            // System.out.println("Tabellenzeile aktualisiert");
        }
        kpan.kassentbl.revalidate();
        kpan.kassentbl.repaint();
    }

    private JXPanel getDatenPanel() {
        JXPanel but = new JXPanel(new BorderLayout());
        but.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        but.setOpaque(false);
        jtf[IDX_TF_KUERZEL] = new JRtaTextField("GROSS", true);
        jtf[IDX_TF_KUERZEL].setName("KUERZEL");
        jtf[IDX_TF_KUERZEL].setToolTipText("<html>Das Kürzele einer Kasse besteht aus insgesamt 6 Zeichen 'AAA-AA'<br>"
                + "Z.B. <b>AOK-RT</b> für AOK Reutlingen</html>");
        MaskFormatter uppercase = null;
        try {
            uppercase = new MaskFormatter("AAA-AA");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DefaultFormatterFactory factory = new DefaultFormatterFactory(uppercase);
        jtf[IDX_TF_KUERZEL].setFormatterFactory(factory);
        jtf[IDX_TF_KUERZEL].addFocusListener(this);
        jtf[IDX_TF_KUERZEL].addKeyListener(this);

        knopf1 = new JButton("Kostenträgerdatei");
        knopf1.setPreferredSize(new Dimension(70, 20));
        knopf1.addActionListener(this);
        knopf1.setActionCommand("vergleichKT");
        knopf1.setName("Kostenträgerdatei");
        knopf1.addKeyListener(this);
        knopf1.setMnemonic(KeyEvent.VK_K);
        if (neuAnlage == true) {
            knopf1.setToolTipText("auf Basis der Daten der Kostenträgerdatei erstellen");
        } else {
            knopf1.setToolTipText("mit Daten der Kostenträgerdatei vergleichen");
        }
        jtf[IDX_TF_PG] = new JRtaTextField("", true);
        jtf[IDX_TF_PG].setName("PREISGRUPPE");
        jtf[IDX_TF_NAME1] = new JRtaTextField("", true);
        jtf[IDX_TF_NAME1].setName("KASSEN_NAM1");
        jtf[IDX_TF_NAME2] = new JRtaTextField("", true);
        jtf[IDX_TF_NAME2].setName("KASSEN_NAM2");
        jtf[IDX_TF_STRASSE] = new JRtaTextField("", true);
        jtf[IDX_TF_STRASSE].setName("STRASSE");
        jtf[IDX_TF_PLZ] = new JRtaTextField("", true);
        jtf[IDX_TF_PLZ].setName("PLZ");
        jtf[IDX_TF_ORT] = new JRtaTextField("", true);
        jtf[IDX_TF_ORT].setName("ORT");
        jtf[IDX_TF_TEL] = new JRtaTextField("", true);
        jtf[IDX_TF_TEL].setName("TELEFON");
        jtf[IDX_TF_FAX] = new JRtaTextField("", true);
        jtf[IDX_TF_FAX].setName("FAX");
        jtf[IDX_TF_EMAIL1] = new JRtaTextField("", true);
        jtf[IDX_TF_EMAIL1].setName("EMAIL1");
        jtf[IDX_TF_EMAIL2] = new JRtaTextField("", true);
        jtf[IDX_TF_EMAIL2].setName("EMAIL2");
        jtf[IDX_TF_EMAIL3] = new JRtaTextField("", true);
        jtf[IDX_TF_EMAIL3].setName("EMAIL3");
        jtf[IDX_TF_KV_NUMMER] = new JRtaTextField("ZAHLEN", true);
        jtf[IDX_TF_KV_NUMMER].setName("KV_NUMMER");
        jtf[IDX_TF_IK_KASSE] = new JRtaTextField("ZAHLEN", true);
        jtf[IDX_TF_IK_KASSE].setName("IK_KASSE"); // aus Kostentraegerdatei/Karte einlesen?
        jtf[IDX_TF_IK_KOSTENT] = new JRtaTextField("ZAHLEN", true);
        jtf[IDX_TF_IK_KOSTENT].setName("IK_KOSTENT");
        jtf[IDX_TF_IK_DATENANN] = new JRtaTextField("ZAHLEN", true);
        jtf[IDX_TF_IK_DATENANN].setName("IK_PHYSIKA");
        jtf[IDX_TF_IK_NUTZER] = new JRtaTextField("ZAHLEN", true);
        jtf[IDX_TF_IK_NUTZER].setName("IK_NUTZER");
        jtf[IDX_TF_IK_PAPIER] = new JRtaTextField("ZAHLEN", true);
        jtf[IDX_TF_IK_PAPIER].setName("IK_PAPIER");

        //                                    1.           2.     3.       4.               5.     6.
        FormLayout lay = new FormLayout("right:max(80dlu;p), 4dlu, 60dlu,right:max(60dlu;p), 4dlu, 60dlu",
                //1.  2.  3.   4.   5.   6  7   8    9   10   11  12  13  14   15   16  17  18   19   20   21  22   23  24   25   26   27  28  29   30   31   32  33  34  
                "p, 2dlu, p, 2dlu, p, 2dlu, p, 2dlu, p, 2dlu, p, 2dlu, p, 2dlu, p, 2dlu, p, 2dlu, p, 2dlu, p, 10dlu, p, 10dlu, p, 2dlu, p, 2dlu, p, 2dlu, p, 2dlu, p, 0dlu");
        PanelBuilder builder = new PanelBuilder(lay);
        builder.setDefaultDialogBorder();
        builder.getPanel()
               .setOpaque(false);
        CellConstraints cc = new CellConstraints();

        // abhängig von boolean mitButton entweder nur das Label mit Icon oder einen
        // JButton
        labKuerzel = new JLabel("Kürzel");
        if (mitButton) {
            builder.add(knopf1, cc.xyw(4, 1, 3));
        } else {
            labKuerzel.setIcon(SystemConfig.hmSysIcons.get("kleinehilfe"));
            labKuerzel.setHorizontalTextPosition(JLabel.LEFT);
            labKuerzel.addMouseListener(new MouseAdapter() {
                @Override
                public void mousePressed(MouseEvent arg0) {
                    doVergleichKT();
                }
            });
        }
        // Ende Änderungsvorschlag

        builder.add(labKuerzel, cc.xy(1, 1, CellConstraints.DEFAULT, CellConstraints.CENTER));
        builder.add(jtf[IDX_TF_KUERZEL], cc.xy(3, 1, CellConstraints.DEFAULT, CellConstraints.CENTER));

        builder.addLabel("Tarifgruppe", cc.xy(1, 3));
        tarifGruppe = new JRtaComboBox();
        tarifGruppe.setName("TARIFGRUPPE");
        tarifGruppe.addFocusListener(this);
        builder.add(tarifGruppe, cc.xyw(3, 3, 4));
        new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                int gruppen = SystemPreislisten.hmPreisGruppen.get("Common")
                                                              .size();
                for (int i = 0; i < gruppen; i++) {
                    tarifGruppe.addItem(SystemPreislisten.hmPreisGruppen.get("Common")
                                                                        .get(i));
                }
                tarifGruppe.setSelectedIndex(0);
                return null;
            }

        }.execute();
        builder.addLabel("Name_1", cc.xy(1, 5));
        builder.add(jtf[IDX_TF_NAME1], cc.xyw(3, 5, 4));
        builder.addLabel("Name_2", cc.xy(1, 7));
        builder.add(jtf[IDX_TF_NAME2], cc.xyw(3, 7, 4));
        builder.addLabel("Strasse", cc.xy(1, 9));
        builder.add(jtf[IDX_TF_STRASSE], cc.xyw(3, 9, 4));

        builder.addLabel("PLZ/Ort", cc.xy(1, 11));
        builder.add(jtf[IDX_TF_PLZ], cc.xy(3, 11));
        builder.add(jtf[IDX_TF_ORT], cc.xyw(4, 11, 3));

        builder.addLabel("Telefon", cc.xy(1, 13));
        builder.add(jtf[IDX_TF_TEL], cc.xyw(3, 13, 4));
        builder.addLabel("Fax", cc.xy(1, 15));
        builder.add(jtf[IDX_TF_FAX], cc.xyw(3, 15, 4));
        builder.addLabel("E-Mail", cc.xy(1, 17));
        builder.add(jtf[IDX_TF_EMAIL1], cc.xyw(3, 17, 4));

        builder.addSeparator("IK-Daten für maschinenlesbare Abrechnung", cc.xyw(1, 21, 6));

        builder.addLabel("IK der Krankenkasse", cc.xy(1, 25));
        builder.add(jtf[IDX_TF_IK_KASSE], cc.xyw(3, 25, 4));

        builder.addLabel("IK des Kostenträgers", cc.xy(1, 27));
        builder.add(jtf[IDX_TF_IK_KOSTENT], cc.xyw(3, 27, 4));

        builder.addLabel("IK der Datenannahmestelle", cc.xy(1, 29));
        builder.add(jtf[IDX_TF_IK_DATENANN], cc.xyw(3, 29, 4));

        builder.addLabel("IK Nutzer/Entschlüssellung", cc.xy(1, 31));
        builder.add(jtf[IDX_TF_IK_NUTZER], cc.xyw(3, 31, 4));

        builder.addLabel("IK Papierannahmestelle", cc.xy(1, 33));
        builder.add(jtf[IDX_TF_IK_PAPIER], cc.xyw(3, 33, 4));

        JScrollPane jscr = JCompTools.getTransparentScrollPane(builder.getPanel());
        jscr.getVerticalScrollBar()
            .setUnitIncrement(15);

        // jscr.setViewportView(builder.getPanel());
        jscr.validate();
        jscr.addKeyListener(this);
        but.add(jscr, BorderLayout.CENTER);

        return but;
    }

    private JXPanel getButtonPanel() {
        JXPanel but = new JXPanel(new BorderLayout());
        but.setOpaque(false);

        knopf4 = new JButton("speichern");
        knopf4.setPreferredSize(new Dimension(70, 20));
        knopf4.addActionListener(this);
        knopf4.setActionCommand("speichern");
        knopf4.setName("speichern");
        knopf4.addKeyListener(this);
        knopf4.setMnemonic(KeyEvent.VK_S);

        knopf5 = new JButton("abbrechen");
        knopf5.setPreferredSize(new Dimension(70, 20));
        knopf5.addActionListener(this);
        knopf5.setActionCommand("abbrechen");
        knopf5.setName("abbrechen");
        knopf5.addKeyListener(this);
        knopf5.setMnemonic(KeyEvent.VK_A);

        //                                     1.             2.    3.                        4.    5.
        FormLayout lay = new FormLayout("right:max(60dlu;p), 4dlu, 60dlu,right:max(60dlu;p), 4dlu, 60dlu",
                // 1.  2. 3. 
                "0dlu, p, 5dlu");
        PanelBuilder builder = new PanelBuilder(lay);
        builder.setDefaultDialogBorder();
        builder.getPanel()
               .setOpaque(false);
        CellConstraints cc = new CellConstraints();

        builder.add(knopf4, cc.xy(3, 2));
        builder.add(knopf5, cc.xy(6, 2));

        but.add(builder.getPanel(), BorderLayout.CENTER);
        return but;
    }

}