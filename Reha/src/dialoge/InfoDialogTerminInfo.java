package dialoge;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTable;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;

import CommonTools.ButtonTools;
import CommonTools.DatFunk;
import CommonTools.JCompTools;
import CommonTools.JRtaTextField;
import CommonTools.SqlInfo;
import CommonTools.StringTools;
import commonData.Rezeptvector;
import environment.Path;
import events.RehaTPEvent;
import events.RehaTPEventClass;
import events.RehaTPEventListener;
import hauptFenster.Reha;
import stammDatenTools.FristenRegelung;
import stammDatenTools.RezTools;
import stammDatenTools.ZuzahlTools.ZZStat;
import systemEinstellungen.SystemConfig;
import systemEinstellungen.SystemPreislisten;
import systemTools.ListenerTools;
import terminKalender.KollegenListe;

public class InfoDialogTerminInfo extends InfoDialog {

    private static final long serialVersionUID = 5798435749921270687L;
    private JLabel textlab;
    private JLabel bildlab;
    private KeyListener kl;


    public InfoDialogTerminInfo(String arg1, Vector<Vector<String>> data) {
        super(arg1, data);

        activateListener();
        this.setContentPane(getTerminInfoContent());

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.addKeyListener(kl);
        this.getContentPane()
            .validate();
    }

    public JXPanel getTerminInfoContent() {
        JXPanel jpan = new JXPanel();
        jpan.addKeyListener(kl);
        jpan.setBackground(Color.WHITE);
        jpan.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        FormLayout lay = new FormLayout("5dlu,p:g,p,p:g,5dlu", "5dlu,p,5dlu,p,p,max(350dlu;p),0dlu,100dlu,5dlu");
        jpan.setLayout(lay);
        CellConstraints cc = new CellConstraints();
        bildlab = new JLabel(" ");
        bildlab.setIcon(SystemConfig.hmSysIcons.get("tporgklein"));
        jpan.add(bildlab, cc.xy(3, 2, CellConstraints.FILL, CellConstraints.DEFAULT));
        htmlPane1 = new JEditorPane(/* initialURL */);
        htmlPane1.setContentType("text/html");
        htmlPane1.setEditable(false);
        htmlPane1.setOpaque(false);
        htmlPane1.addKeyListener(kl);
        scr1 = JCompTools.getTransparentScrollPane(htmlPane1);

        scr1.validate();
        jpan.add(scr1, cc.xywh(2, 4, 3, 3, CellConstraints.FILL, CellConstraints.FILL));

        htmlPane2 = new JEditorPane(/* initialURL */);
        htmlPane2.setContentType("text/html");
        htmlPane2.setEditable(false);
        htmlPane2.setOpaque(false);
        htmlPane2.addKeyListener(kl);

        scr2 = JCompTools.getTransparentScrollPane(htmlPane2);
        scr2.validate();
        jpan.add(scr2, cc.xyw(2, 8, 3, CellConstraints.FILL, CellConstraints.FILL));

        holeTerminInfo();

        htmlPane1.addHyperlinkListener(new HyperlinkListener() {
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    if (extractFieldName(e.getURL()
                                          .toString()).equals("weiteretermine")) {
                        // System.err.println(e.getURL().toString());
                        htmlPane1.requestFocus();
                        // hier muß der Suchendialog gestartet werden
                        starteFolgeTermine();

                        return;
                    }
                }
            }

            private String extractFieldName(String url) {
                String ext = url.substring(7);
                return ext.replace(".de", "");
            }
        });

        jpan.validate();
        return jpan;
    }

    void activateListener() {
        kl = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void keyPressed(KeyEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_F1 && (!e.isControlDown()) && (!e.isShiftDown())) {
                    dispose();
                }

            }

        };
    }

    private void holeTerminInfo() {
        String rezNr = macheNummer(this.arg1);
        Rezeptvector aktRezept = new Rezeptvector();
        if ((rezNr.length() == 0) 
                || ("@FREI".equals(rezNr))
                || (SqlInfo.holeEinzelFeld("select id from verordn where rez_nr = '" + rezNr + "' LIMIT 1")
                           .equals(""))) {
            if (SqlInfo.holeEinzelFeld("select id from lza where rez_nr = '" + rezNr + "' LIMIT 1")
                    .equals("")) {
                JOptionPane.showMessageDialog(null,
                        "Dieses Rezept ist weder im aktuellen Rezeptstamm noch in der Historie - also gelöscht!"
                                + "\nRezeptnummer: " + this.arg1);                
            } else {
                JOptionPane.showMessageDialog(null, "Dieses Rezept ist bereits in der Historie - also abgerechnet!"
                        + "\nRezeptnummer: " + this.arg1);
            }
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    dispose();
                }
            });
            return;
        } else {
            aktRezept.init(rezNr);
        }

        try {
            disziplin = StringTools.getDisziplin(rezNr);
            tagebreak = (Integer) ((Vector<?>) SystemPreislisten.hmFristen.get(disziplin)
                                                                          .get(2)).get(aktRezept.getPreisGrpIdx());

        } catch (Exception ex) {
            tagebreak = 28;
            ex.printStackTrace();
        }

        fuelleHTML(aktRezept);
    }

    private void fuelleHTML(Rezeptvector aktRezept) {
        currPatient = aktRezept.getPatIntern();
        try {
            String shtml = ladehead() + getMittelTeil(aktRezept) + ladeend();
            htmlPane1.setText(shtml);
            scr1.revalidate();
            htmlPane1.validate();
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    if (scr1.isVisible()) {
                        scr1.getVerticalScrollBar()
                            .setValue(0);
                        // scr1.getViewport().scrollRectToVisible(new Rectangle(0,0));
                    }
                }
            });
            /**************/
            shtml = ladehead() + getErgebnisTeil(aktRezept);
            ladeend();
            htmlPane2.setText(shtml);
            scr2.revalidate();
            htmlPane2.validate();
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    if (scr2.isVisible()) {
                        scr2.getVerticalScrollBar()
                            .setValue(0);
                        // scr1.getViewport().scrollRectToVisible(new Rectangle(0,0));
                    }
                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private final StringBuffer bufhead = new StringBuffer();
    private final StringBuffer bufend = new StringBuffer();
    private String currPatient;
    Vector<String> tage = null;
    Vector<Vector<String>> tageplus = null;
    private FristenRegelung fristen;

    public String ladehead() {
        bufhead.append("<html>\n<head>\n");
        bufhead.append("<STYLE TYPE=\"text/css\">\n");
        bufhead.append("<!--\n");
        bufhead.append("A{text-decoration:none;background-color:transparent;border:none}\n");
        bufhead.append("A.even{text-decoration:underline;color: #000000; background-color:transparent;border:none}\n");
        bufhead.append("A.odd{text-decoration:underline;color: #FFFFFF;background-color:transparent;border:none}\n");
        bufhead.append("TD{font-family: Arial; font-size: 12pt; vertical-align: top;white-space: nowrap;}\n");
        bufhead.append(
                "TD.inhalt {font-family: Arial, Helvetica, sans-serif; font-size: 20px;background-color: #7356AC;color: #FFFFFF;}\n");
        bufhead.append(
                "TD.inhaltinfo {font-family: Arial, Helvetica, sans-serif; font-size: 20px;background-color: #DACFE7; color: #1E0F87;}\n");
        bufhead.append(
                "TD.headline1 {font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-color: #EADFF7; color: #000000;}\n");
        bufhead.append(
                "TD.headline2 {font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-color: #DACFE7; color: #000000;}\n");
        bufhead.append(
                "TD.headline3 {font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-color: #7356AC; color: #FFFFFF;}\n");
        bufhead.append(
                "TD.headline4 {font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-color: #1E0F87; color: #FFFFFF;}\n");
        bufhead.append(
                "TD.itemeven {font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-color: #E6E6E6; color: #000000;}\n");
        bufhead.append(
                "TD.itemodd {font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-color: #737373; color: #F0F0F0;}\n");
        bufhead.append(
                "TD.itemkleineven {font-family: Arial, Helvetica, sans-serif; font-size: 9px; background-color: #E6E6E6; color: #000000;}\n");
        bufhead.append(
                "TD.itemkleinodd {font-family: Arial, Helvetica, sans-serif; font-size: 9px; background-color: #737373; color: #F0F0F0;}\n");
        bufhead.append(
                "TD.header {font-family: Arial, Helvetica, sans-serif; font-size: 14px; background-color: #1E0F87; color: #FFFFFF;}\n");
        bufhead.append("UL {font-family: Arial, Helvetica, sans-serif; font-size: 9px;}\n");
        bufhead.append("UL.paeb { margin-top: 0px; margin-bottom: 0px; }\n");
        bufhead.append(
                "H1 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; background-color: #1E0F87; color: #FFFFFF;}\n");
        bufhead.append("--->\n");
        bufhead.append("</STYLE>\n");
        bufhead.append("</head>\n");
        bufhead.append("<body>\n");
        return bufhead.toString();

    }

    public String ladeend() {
        bufend.append("</body>\n</html>\n");
        return bufend.toString();
    }

    private void starteFolgeTermine() {
        FolgeTermine folgeTermine = new FolgeTermine();
        PinPanel pinPanel = new PinPanel();
        pinPanel.setName("FolgeTermine");
        pinPanel.getGruen()
                .setVisible(false);
        folgeTermine.getSmartTitledPanel()
                    .setTitle("<html><b>Folgetermine suchen</b></html>");
        folgeTermine.setSize(500, 800);
        folgeTermine.setPreferredSize(new Dimension(580 + Reha.zugabex, 350 + Reha.zugabey));
        folgeTermine.getSmartTitledPanel()
                    .setPreferredSize(new Dimension(580, 350)); // Original 630
        folgeTermine.setPinPanel(pinPanel);
        folgeTermine.getSmartTitledPanel()
                    .setContentContainer(new FolgeTermineSuchen());
        folgeTermine.getSmartTitledPanel()
                    .getContentContainer()
                    .setName("FolgeTermine");
        folgeTermine.setName("FolgeTermine");
        folgeTermine.setModal(true);
        // folgeTermine.setLocationRelativeTo(this);
        folgeTermine.setLocation(this.getLocation().x + 150, this.getLocation().y + 250);
        folgeTermine.pack();
        folgeTermine.setVisible(true);
        folgeTermine.dispose();
        muststop = true;
        Robot rob;
        try {
            rob = new Robot();
            rob.keyRelease(KeyEvent.VK_F1);
        } catch (AWTException e) {
            e.printStackTrace();
        }

    }

    /**
     * @param aktRezept *************************************************/
    public String getErgebnisTeil(Rezeptvector aktRezept) {
        StringBuffer ergebnis = new StringBuffer();
        String latestStartDE = DatFunk.sDatInDeutsch(aktRezept.getLastDate());
        int anzBehMax = aktRezept.getAnzVorrBeh();
        int anzBehDone = tage.size();
        if (anzBehDone == anzBehMax) {
            ergebnis.append("<span " + getSpanStyle("12", "#ff0000") + "<b>Das Rezept ist voll</b></span>\n");
        } else if (anzBehDone > 0 && anzBehDone < anzBehMax) {
            String slastdate = DatFunk.sDatPlusTage(tage.get(anzBehDone - 1), tagebreak);
            String lastdate = DatFunk.WochenTag(slastdate) + " " + slastdate;
            long diff = 0;
            if ((diff = DatFunk.TageDifferenz(slastdate, DatFunk.sHeute())) > 0) {
                ergebnis.append("<span " + getSpanStyle("14", "#ff0000") + "<b>" + lastdate + "</b></span><br>"
                        + "<span " + getSpanStyle("10", "") + "Maximale Unterbrechung verpasst " + lastdate + "!! ("
                        + Integer.toString(tagebreak) + " Tage)</span>\n");
            } else {
                ergebnis.append("<span " + getSpanStyle("14", "#008000") + "<b>" + lastdate + "</b></span><br>"
                        + "<span " + getSpanStyle("10", "") + "nächster Termin spätestens am " + lastdate + " ("
                        + Integer.toString(tagebreak) + " Tage)</span>\n");
            }

        } else if (anzBehDone == 0) {
            long diff = 0;
            if ((diff = DatFunk.TageDifferenz(latestStartDE, DatFunk.sHeute())) > 0) {
                ergebnis.append("<span " + getSpanStyle("14", "#ff0000") + "<b>" + latestStartDE + "</b></span><br>"
                        + "<span " + getSpanStyle("10", "")
                        + "Achtung Behandlungsbeginn heute überschreitet<br>den spätesten Behandlungsbeginn<br>um "
                        + Long.toString(diff) + " Tage!!</span>\n");
            } else {
                ergebnis.append("<span " + getSpanStyle("14", "#008000") + "<b>" + latestStartDE + "</b></span><br>"
                        + "<span " + getSpanStyle("10", "")
                        + "Behandlung noch nicht begonnen stand heute<br>spätester Behandlungsbeginn<br>in "
                        + Long.toString(diff) + " Tage(n)!!</span>\n");
            }
        }
        if (anzBehDone > 0) {   // Physio-VOs sind 3/6 Monate gueltig, Logo 7/9 + Ablaufregelung Podo
            String lastdate = fristen.getLastChance();
            long diff = DatFunk.TageDifferenz(lastdate, DatFunk.sHeute());
            if (diff > 0) {
                ergebnis.append("<span " + getSpanStyle("14", "#ff0000") + "<br><br><b>" + DatFunk.WochenTag(lastdate)
                        + " " + lastdate + "</b></span><br>" + "<span " + getSpanStyle("10", "")
                        + "letzter Behandlungstermin spätestens am " + lastdate + " <br>(" + fristen.getVerfallGrund() + ")</span>\n");
            } else {
                ergebnis.append("<span " + getSpanStyle("14", "#008000") + "<br><br><b>" + DatFunk.WochenTag(lastdate)
                        + " " + lastdate + "</b></span><br>" + "<span " + getSpanStyle("10", "")
                        + "letzter Behandlungstermin spätestens am " + lastdate + " <br>(" + fristen.getVerfallGrund() + ")</span>\n");
            }
        }
        return ergebnis.toString();
    }

    /***************************************************/
    /**
     * @param aktRezept 
     **/
    public String getMittelTeil(Rezeptvector aktRezept) {
        StringBuffer mitte = new StringBuffer();
        boolean zuzahl = (aktRezept.getZzStat().equals("0") ? false : true);
        String termine = aktRezept.getTermine();
        tage = RezTools.holeEinzelTermineAusRezept("", termine);
        tageplus = RezTools.holeTermineUndBehandlerAusRezept("", termine);
        int geleistet = aktRezept.getAnzVorrBehGeleistet(); 
        String patInt = aktRezept.getPatIntern();
        String patName = SqlInfo.holeEinzelFeld("select concat(n_name, ', ', v_name) from pat5 where pat_intern = '" + patInt + "' LIMIT 1");
        mitte.append("<span " + getSpanStyle("12", "") + StringTools.EGross(patName)
                + "<br>Rezeptnummer: " + this.arg1 + "<br>" + StringTools.EGross(aktRezept.getKtrName())
                + "</span><br><br>\n");
        mitte.append("<span " + getSpanStyle("10", "") + getPositionen(aktRezept) + "</span><br><br>\n");

        mitte.append("<span " + getSpanStyle("10", "") + "Rezeptgebühr: </span>");
        if (!zuzahl) {
            mitte.append("<span " + getSpanStyle("10", "#008000") + "<b>nicht erforderlich oder befreit</b>" + "</span><br>\n");
        } else {
            int zzStat = Integer.parseInt(aktRezept.getZzStat());
            if (zzStat == ZZStat.ZUZAHLOK.ordinal()) {
                mitte.append("<span " + getSpanStyle("10", "#008000") + "<b>bezahlt</b>" + "</span><br>\n");
            } else if (zzStat == ZZStat.ZUZAHLNICHTOK.ordinal()) {
                mitte.append("<span " + getSpanStyle("10", "#ff0000") + "<b>nicht bezahlt</b>" + "</span><br>\n");
            }

        }
        if (aktRezept.getArztbericht()) {       // ok?
            mitte.append("<span " + getSpanStyle("10", "") + "Arztbericht: </span>");
            if ("".equals(aktRezept.getArztBerichtID())) {
                mitte.append("<span " + getSpanStyle("10", "#ff0000") + "<b>noch nicht erstellt</b>" + "</span><br>\n");
            } else {
                mitte.append("<span " + getSpanStyle("10", "#008000") + "<b>bereits angelegt</b>" + "</span><br>\n");                
            }
        }
        String unEqual = (geleistet != tageplus.size()) ?  " (an " + Integer.toString(tageplus.size()) + " Tagen)" : "";
        mitte.append("<span " + getSpanStyle("10", "") + "Bislang durchgeführt: <b>" + geleistet
                + " von " + aktRezept.getAnzVorrBeh() + unEqual + "</b></span><br>\n"); 
        String rezDatDE = DatFunk.sDatInDeutsch(aktRezept.getRezeptDatum());
        String latestStartDE = DatFunk.sDatInDeutsch(aktRezept.getLastDate());
        mitte.append("<span " + getSpanStyle("10", "") + "Rezeptdatum: <b>" + rezDatDE + "</b></span><br>\n");
        mitte.append("<span " + getSpanStyle("10", "") + "Spätester Behandlungsbeginn: <b>" + latestStartDE + "</b></span><br><br>\n");

        int pghmr = aktRezept.getPreisGrpIdx();
        String disziplin = StringTools.getDisziplin(this.arg1);

        if (SystemPreislisten.hmHMRAbrechnung.get(disziplin)
                                             .get(pghmr) < 1) {
            mitte.append("<span " + getSpanStyle("14", "#ff0000") + "<b>Keine HMR-Prüfung erforderlich</b>" + "</span>"
                    + "<br><br>\n");
        }
        mitte.append("<table width='100%'>\n");
        /***********************************************************************************/
        long diff = 0;

        fristen = new FristenRegelung(aktRezept);
        int behandlungen = tageplus.size();
        if (behandlungen > 0) {
            mitte.append("<tr>\n");
            mitte.append("<td class='itemkleinodd'>" + "#" + "</td>\n");
            mitte.append("<td class='itemkleinodd' align=\"center\">" + "Beh.-Datum" + "</td>\n");
            mitte.append("<td class='itemkleinodd'>" + "spät.Beginn / Pause" + "</td>\n");
            mitte.append("<td class='itemkleinodd'>" + "VO gültig" + "</td>\n");
            mitte.append("<td class='itemkleinodd'>" + "behandelt von" + "</td>\n");
            mitte.append("</tr>\n");
        }
        for (int i = 0; i < behandlungen/* tage.size() */; i++) {
            String currDate = tageplus.get(i)
                                     .get(0);
            String currBehandl = tageplus.get(i)
                                         .get(1);
            mitte.append("<tr>\n");
            mitte.append("<td class='itemkleinodd'>" + Integer.toString(i + 1) + "</td>\n");
            mitte.append("<td class='itemkleineven' align=\"center\">" + currDate + "</td>\n");

            if (i == 0) {
                // zuerst testen ob vor dem Rezeptdatum begonnen wurde
                if ((diff = DatFunk.TageDifferenz(rezDatDE, currDate)) < 0) {
                    mitte.append("<td><img src='file:///" + Path.Instance.getProghome() + "icons/nichtok.gif" + "'>"
                            + " < Rezeptdatum<br>" + Long.toString(diff) + " Tage" + "</td>\n");
                } else if ((diff = DatFunk.TageDifferenz(latestStartDE, currDate)) > 0) {
                    // der spaeteste Beginn wurde ueberschritten
                    mitte.append("<td><img src='file:///" + Path.Instance.getProghome() + "icons/nichtok.gif" + "'>"
                            + " > " + Long.toString(diff) + " Tage" + "</td>\n");
                } else {    // DatFunk.TageDifferenz(latestStartDE, currDay)) <= 0)
                    // der spaeteste Beginn wurde eingehalten
                    mitte.append("<td><img src='file:///" + Path.Instance.getProghome() + "icons/ok.gif" + "'>"
                            + " <= " + Long.toString(diff) + " Tage" + "</td>\n");
                }
                mitte.append("<td>&nbsp;</td>\n");  // kein Abstand zw. Beh.
                mitte.append("<td>" + currBehandl + "</td>\n");
            } else if (i > 0) {
                String previousDate = tageplus.get(i - 1)
                                              .get(0);
                if ((diff = DatFunk.TageDifferenz(previousDate, currDate)) > tagebreak) {   // TODO: Begruendung checken
                    mitte.append("<td><img src='file:///" + Path.Instance.getProghome() + "icons/nichtok.gif" + "'>"
                            + " > " + Long.toString(diff) + " Tage" + "</td>\n");
                } else {
                    mitte.append("<td><img src='file:///" + Path.Instance.getProghome() + "icons/ok.gif" + "'>" + " <= "
                            + Long.toString(diff) + " Tage" + "</td>\n");
                }
                if (fristen.dateIsValid(currDate)) {
                    mitte.append("<td><img src='file:///" + Path.Instance.getProghome() + "icons/ok.gif" + "'>"
                            + fristen.getCheckTxt() + "</td>\n");
                } else {
                    mitte.append("<td><img src='file:///" + Path.Instance.getProghome() + "icons/nichtok.gif" + "'>"
                            + fristen.getCheckTxt() + "</td>\n");
                }

                mitte.append("<td style='white-space: nowrap;'>" + currBehandl + "</td>\n");
            }

            mitte.append("</tr>\n");
        }
        /***********************************************************************************/
        mitte.append("<tr><td>&nbsp;</td>\n");
        mitte.append("<td align=\"center\">" + makeLink("fragezeichen.png", "weiteretermine") + "</td></tr>");
        mitte.append("</table>\n");

        return mitte.toString();
    }

    /***************************************************/

    private String getPositionen(Rezeptvector aktRezept) {
        StringBuffer positionen = new StringBuffer();
        for (int i = 1; i < 5; i++) {
            String currHM = aktRezept.getHMkurz(i);
            if (!"".equals(currHM)) {
                positionen.append((positionen.length() > 0) ? ", " : "");
                positionen.append(aktRezept.getAnzBehS(i) + " x " + currHM);
            }
        }
        return positionen.toString();
    }

    private String makeLink(String ico, String url) {
        String linktext = "<img src='file:///" + Path.Instance.getProghome() + "icons/" + ico + "'  border=0>";
        linktext = "<a href=\"http://" + url + ".de\">" + linktext + "</a>\n";
        return linktext;
    }

    /***************************************/
    private class FolgeTermine extends RehaSmartDialog implements RehaTPEventListener, WindowListener {

        /**
         *
         */
        private static final long serialVersionUID = 1L;
        private RehaTPEventClass rtp = null;

        public FolgeTermine() {
            super(null, "FolgeTermine");
            this.setName("FolgeTermine");
            rtp = new RehaTPEventClass();
            rtp.addRehaTPEventListener((RehaTPEventListener) this);

        }

        public void rehaTPEventOccurred(RehaTPEvent evt) {

            try {

                if (evt.getDetails()[0] != null) {
                    if (evt.getDetails()[0].equals(this.getName())) {
                        this.setVisible(false);
                        this.dispose();
                        rtp.removeRehaTPEventListener((RehaTPEventListener) this);
                        rtp = null;
                        ListenerTools.removeListeners(this);
                        muststop = true;
                        super.dispose();
                    }
                } else {
                    // System.out.println("Details == null");
                }
            } catch (NullPointerException ne) {
                ne.printStackTrace();
                // System.out.println("In FolgeTermine " +evt);
            } catch (Exception ex) {

            }
        }

        public void windowClosed(WindowEvent arg0) {
            if (rtp != null) {
                this.setVisible(false);
                rtp.removeRehaTPEventListener((RehaTPEventListener) this);
                rtp = null;
                dispose();
                ListenerTools.removeListeners(this);
                super.dispose();
                // System.out.println("****************Rezept Neu/ändern -> Listener entfernt
                // (Closed)**********");
            }
        }
    }

    /**************************************************/
    private class FolgeTermineSuchen extends JXPanel implements  RehaTPEventListener {
        /**
         *
         */
        private static final long serialVersionUID = 1L;
        private RehaTPEventClass rtp = null;
        private MyFolgeTermineTableModel mod = new MyFolgeTermineTableModel();
        private JXTable tab;
        private JRtaTextField tfstart;
        private JRtaTextField tfsuche;
        private JLabel lbaktuell;

        private String suchkrit;
        private String startdate;

        private ActionListener al;

        public FolgeTermineSuchen() {
            super();
            rtp = new RehaTPEventClass();
            rtp.addRehaTPEventListener((RehaTPEventListener) this);
            al = new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    String action = e.getActionCommand();
                    if (action.equals("start")) {
                        muststop = false;
                        buts[0].setEnabled(false);
                        buts[1].setEnabled(true);
                        lbaktuell.setText(tfstart.getText());
                        startdate = tfstart.getText();
                        mod.setRowCount(0);
                        tab.validate();
                        starteSuche();
                    } else if (action.equals("stop")) {
                        muststop = true;
                        buts[1].setEnabled(false);
                        buts[0].setEnabled(true);
                    } else if (action.equals("close")) {
                        muststop = true;
                        dispose();
                    }

                }

            };
            // 1 2 3 4 5 6 7 8 9 10 11 12
            String x = "5dlu,p,2dlu,80dlu,5dlu,p,2dlu,80dlu,5dlu,p,2dlu,80dlu,0dlu:g,5dlu";
            // 1 2 3 4 5 6
            String y = "5dlu,p,5dlu,p,5dlu,30dlu:g,5dlu";
            FormLayout lay = new FormLayout(x, y);
            CellConstraints cc = new CellConstraints();

            setLayout(lay);
            add(new JLabel("suche nach"), cc.xy(2, 2));
            add((tfsuche = new JRtaTextField("GROSS", true)), cc.xy(4, 2));
            add(new JLabel("starte bei"), cc.xy(6, 2));
            add((tfstart = new JRtaTextField("DATUM", true)), cc.xy(8, 2));
            add(new JLabel("aktuell bei"), cc.xy(10, 2));
            add((lbaktuell = new JLabel("")), cc.xy(12, 2));
            lbaktuell.setForeground(Color.RED);
            String[] column = { "lfd.", "Tag", "Datum", "Uhrzeit", "Name", "Rez.Nummer", "Therapeut", "Tage diff." };
            mod.setColumnIdentifiers(column);

            tab = new JXTable(mod);
            tab.getColumn(0)
               .setMaxWidth(20);
            tab.getColumn(7)
               .setMaxWidth(30);
            JScrollPane scr = JCompTools.getTransparentScrollPane(tab);
            scr.validate();
            add(scr, cc.xyw(2, 6, 12));
            add(buts[0] = ButtonTools.macheButton("start", "start", al), cc.xy(8, 4));
            add(buts[1] = ButtonTools.macheButton("stop", "stop", al), cc.xy(4, 4));
            buts[0].setEnabled(false);
            // add( ButtonTools.macheBut("schliessen","close", al),cc.xy(8,4));
            validate();
            if (tageplus.size() <= 0) {
                startdate = DatFunk.sHeute();
            } else {
                startdate = DatFunk.sDatPlusTage(tageplus.get(tageplus.size() - 1)
                                                         .get(0),
                        1);
            }
            tfstart.setText(startdate);
            suchkrit = SqlInfo.holeEinzelFeld("select n_name from pat5 where pat_intern = '" + currPatient + "' LIMIT 1")
                    + " " + arg1;
            tfsuche.setText(suchkrit);
            lbaktuell.setText(startdate);
            starteSuche();

        }

        @Override
        public void rehaTPEventOccurred(RehaTPEvent evt) {
            try {
                if (evt.getDetails()[0] != null) {
                    if (evt.getDetails()[0].equals(this.getName())) {
                        this.setVisible(false);
                        rtp.removeRehaTPEventListener((RehaTPEventListener) this);
                        rtp = null;
                        muststop = true;
                        // aufraeumen();
                    }
                }
            } catch (NullPointerException ne) {
                JOptionPane.showMessageDialog(null, "Fehler beim abhängen des Listeners Rezept-Neuanlage\n"
                        + "Bitte informieren Sie den Administrator über diese Fehlermeldung");
            }

        }



        private void starteSuche() {
            muststop = false;
            new SuchenInTagen().setzeStatement(lbaktuell, startdate, tfsuche.getText()
                                                                            .split(" "),
                    mod);
        }

    }

    /*****************************************************/
    private class MyFolgeTermineTableModel extends DefaultTableModel {
        /**
        *
        */
        private static final long serialVersionUID = 1L;

        public Class<?> getColumnClass(int columnIndex) {
            if (columnIndex == 0) {
                return String.class;
            }
            else {
                return String.class;
            }
        }

        public boolean isCellEditable(int row, int col) {
            // Note that the data/cell address is constant,
            // no matter where the cell appears onscreen.
            return false;
        }

        public Object getValueAt(int rowIndex, int columnIndex) {
            String theData = (String) ((Vector<?>) getDataVector().get(rowIndex)).get(columnIndex);
            Object result = null;
            result = theData;
            return result;
        }

    }

    /*****************************************************/
    private final class SuchenInTagen extends Thread implements Runnable {
        private Statement stmt = null;
        private ResultSet rs = null;

        private int belegt = 0;
        private String anzeigedatum;
        private String[] suchkrit = null;
        private JLabel lbaktuell;
        private String startdat;
        private MyFolgeTermineTableModel mod;
        private Vector<String> atermine = new Vector<String>();
        private int treffer = 1;

        private void setzeStatement(JLabel lbaktuell, String startdat, String[] suchkrit, MyFolgeTermineTableModel mod) {
            this.lbaktuell = lbaktuell;
            this.startdat = startdat;
            this.mod = mod;
            anzeigedatum = lbaktuell.getText();
            this.suchkrit = suchkrit;
            start();
        }

        public void run() {

            try {
                stmt = (Statement) Reha.instance.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_UPDATABLE);
                while (!muststop) {
                    try {
                        rs = (ResultSet) stmt.executeQuery(
                                "select * from flexkc where datum = '" + DatFunk.sDatInSQL(anzeigedatum) + "' LIMIT "
                                        + Integer.toString(KollegenListe.maxKalZeile));
                        //// System.out.println("Nach for..."+exStatement[i]);
                        // SchnellSuche.thisClass.setLabelDatum("nach ExecuteQuery");

                        while (rs.next() && !muststop) {
                            try {
                                /* in Spalte 301 steht die Anzahl der belegten Blöcke */
                                belegt = rs.getInt(301);
                                String name = "";
                                String nummer = "";
                                String termin = "";
                                String sdatum = "";
                                String sorigdatum = "";
                                String uhrzeit = "";
                                String skollege = "";
                                int ikollege = 0;
                                for (int ii = 0; ii < belegt; ii++) {
                                    name = rs.getString("T" + (ii + 1));
                                    nummer = rs.getString("N" + (ii + 1));
                                    skollege = rs.getString(303)
                                                 .substring(0, 2);
                                    if (skollege.substring(0, 1)
                                                .equals("0")) {
                                        ikollege = Integer.parseInt(skollege.substring(1, 2));
                                    } else {
                                        ikollege = Integer.parseInt(skollege);
                                    }
                                    if (suchkrit.length > 1) {
                                        if (name.contains(suchkrit[0]) || nummer.contains(suchkrit[1])) {
                                            uhrzeit = rs.getString("TS" + (ii + 1));
                                            sorigdatum = rs.getString(305);
                                            sdatum = DatFunk.sDatInDeutsch(sorigdatum);
                                            skollege = (String) KollegenListe.getKollegenUeberReihe(ikollege);

                                            termin = DatFunk.WochenTag(sdatum) + " - " + sdatum + " - " + uhrzeit
                                                    + "  -  " + name + " - " + nummer + " - " + skollege;
                                            atermine.add(Integer.toString(treffer));
                                            atermine.add(DatFunk.WochenTag(sdatum));
                                            atermine.add(sdatum);
                                            atermine.add(uhrzeit.substring(0, 5));
                                            atermine.add(name);
                                            atermine.add(nummer);
                                            atermine.add(skollege);
                                            atermine.add("k.A.");
                                            mod.addRow((Vector) atermine.clone());
                                            treffer++;
                                            atermine.clear();
                                        }
                                    } else {
                                        if (name.contains(suchkrit[0]) || nummer.contains(suchkrit[0])) {
                                            uhrzeit = rs.getString("TS" + (ii + 1));
                                            sorigdatum = rs.getString(305);
                                            sdatum = DatFunk.sDatInDeutsch(sorigdatum);
                                            skollege = (String) KollegenListe.getKollegenUeberReihe(ikollege);

                                            termin = DatFunk.WochenTag(sdatum) + " - " + sdatum + " - " + uhrzeit
                                                    + "  -  " + name + " - " + nummer + " - " + skollege;
                                            atermine.add(Integer.toString(treffer));
                                            atermine.add(DatFunk.WochenTag(sdatum));
                                            atermine.add(sdatum);
                                            atermine.add(uhrzeit.substring(0, 5));
                                            atermine.add(name);
                                            atermine.add(nummer);
                                            atermine.add(skollege);
                                            atermine.add("k.A.");
                                            mod.addRow((Vector) atermine.clone());
                                            treffer++;
                                            atermine.clear();
                                        }

                                    }
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }

                    } catch (SQLException ev) {
                        // System.out.println("SQLException: " + ev.getMessage());
                        // System.out.println("SQLState: " + ev.getSQLState());
                        // System.out.println("VendorError: " + ev.getErrorCode());
                    }
                    anzeigedatum = DatFunk.sDatPlusTage(anzeigedatum, 1);
                    if (DatFunk.TageDifferenz(startdat, anzeigedatum) > 90) {
                        muststop = true;
                    } else {
                        this.lbaktuell.setText(anzeigedatum);
                    }

                }

                buts[1].setEnabled(false);
                buts[0].setEnabled(true);

            } catch (SQLException ex) {
                // System.out.println("von stmt -SQLState: " + ex.getSQLState());
            }

            finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) { // ignore }
                        rs = null;
                    }
                    if (stmt != null) {
                        try {
                            stmt.close();
                        } catch (SQLException sqlEx) { // ignore }
                            stmt = null;
                        }
                    }
                }
            }

        }

    }

    /*****************************************************/
}
