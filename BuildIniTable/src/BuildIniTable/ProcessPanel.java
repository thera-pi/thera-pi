package BuildIniTable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import org.jdesktop.swingworker.SwingWorker;
import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.JXTable;


import CommonTools.ButtonTools;
import CommonTools.FileTools;
import CommonTools.JCompTools;
import CommonTools.JRtaCheckBox;
import CommonTools.SqlInfo;
import CommonTools.ini.INIFile;
import crypt.Verschluesseln;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.mysql.jdbc.PreparedStatement;

public class ProcessPanel extends JXPanel{
    /**
     * 
     */
    private static final long serialVersionUID = 2467154459698276827L;
    public JRtaCheckBox[] check = {null,null,null,null,null,null,null,null,null,null};
    public JButton[] buts = {null,null,null};
    public JTextArea area = null;
    public JXTable tab = null;
    public MyIniTableModel tabmod = null;
    public Vector<String> inivec = new Vector<String>(); 
    ActionListener al = null;
    public ProcessPanel(){
        super(new BorderLayout());
        setPreferredSize(new Dimension(500,500));
        activateListener();
        add(mainpanel(),BorderLayout.CENTER);
        add(mainpanel());
        validate();
        System.out.println(BuildIniTable.thisClass.mandantIkvec);
        System.out.println(BuildIniTable.thisClass.mandantNamevec);
        
    }
    private JXPanel mainpanel(){
        JXPanel pan = new JXPanel();
        pan.setBackground(Color.WHITE);
        String x = "10dlu,125dlu,5dlu,p:g,10dlu";
        String y = "10dlu,p,20dlu,";
        for(int i = 0; i < BuildIniTable.thisClass.anzahlmandanten;i++){
            y = y +"p,2dlu,";
        }
        y=y+"40dlu,p,5dlu,fill:0:grow(1.00),5dlu";
        
        FormLayout lay = new FormLayout(x,y);
        CellConstraints cc = new CellConstraints();
        
        pan.setLayout(lay);
        
        JLabel lab = new JLabel("<html><font size=+1><font color=#0000FF>Bitte kreuzen Sie links<u> die Mandanten</u> an, für die Sie die DB-Tabelle <b>'inidatei'</b> erzeugen wollen.&nbsp;&nbsp;In der Liste rechts sind bereits INI-Dateien für die Aufnahme in die DB-Tabelle markiert. Sie können zusätzliche INI-Dateien markieren (wird jedoch ausdrücklich nicht empfohlen /st.)</font></font></html>");
        //lab.setFont(new Font("Arial",12,Font.PLAIN));
        //lab.setForeground(Color.BLUE);
        pan.add(lab,cc.xyw(2,2,3,CellConstraints.FILL,CellConstraints.DEFAULT));
        int lastY = 0;
        for(int i = 0; i < BuildIniTable.thisClass.anzahlmandanten;i++){
            check[i] = new JRtaCheckBox(BuildIniTable.thisClass.mandantIkvec.get(i)+ " - "+BuildIniTable.thisClass.mandantNamevec.get(i));
            lastY = 3+((i*2)+1);
            pan.add(check[i],cc.xy(2,lastY));
        }
        buts[0] = ButtonTools.macheButton("Tabelle erzeugen", "erzeugen", al);

        pan.add(buts[0],cc.xy(2, lastY+3));
        
        tabmod = new MyIniTableModel();
        tabmod.setColumnIdentifiers(new String[] {"in Tabelle","INI-Datei"});
        tab = new JXTable(tabmod);
        JScrollPane tabscr = JCompTools.getTransparentScrollPane(tab);
        tabscr.validate();
        pan.add(tabscr,cc.xywh(4,4,1,lastY,CellConstraints.FILL,CellConstraints.FILL));

        area = new JTextArea();
        area.setFont(new Font("Courier",Font.PLAIN,12));
        area.setLineWrap(true);
        area.setName("logbuch");
        area.setWrapStyleWord(true);
        area.setEditable(false);
        area.setBackground(Color.WHITE);
        area.setForeground(Color.BLACK);
        JScrollPane span = JCompTools.getTransparentScrollPane(area);
        span.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        span.validate();
        pan.add(span,cc.xyw(2,lastY+5,3,CellConstraints.FILL,CellConstraints.FILL));
        pan.validate();
        getIniList();
        return pan;
    }
    private void getIniList(){
        if(BuildIniTable.thisClass.anzahlmandanten <= 0){
            return;
        }
        File dir = new File(BuildIniTable.thisClass.pfadzurini+"/"+BuildIniTable.thisClass.mandantIkvec.get(0)+"/");
        File[] contents = dir.listFiles();
        inivec.clear();
        for(int i = 0; i < contents.length;i++){
            if(contents[i].getName().endsWith(".ini")){
                if(!contents[i].getName().equals("rehajava.ini") && ! contents[i].getName().equals("inicontrol.ini") && ! contents[i].getName().equals("firmen.ini")){
                    inivec.add(contents[i].getName());                  
                }
            }
        }
        Comparator<String> comparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                // TODO Auto-generated method stub
                String s1 = (String)o1;
                String s2 = (String)o2;
                return s1.compareTo(s2);
            }
        };
        Collections.sort(inivec,comparator);
        Vector<Object> dummy = new Vector<Object>();
        boolean logwert = false;
        List<String> listini = Arrays.asList(BuildIniTable.thisClass.inis);
        for(int i = 0; i < inivec.size();i++){
            dummy.clear();
            if(listini.contains(inivec.get(i))){
                logwert = true;
            }else{
                logwert = false;
            }
            dummy.add(logwert);
            dummy.add(inivec.get(i));
            tabmod.addRow((Vector<?>)dummy.clone());
        }
    }
    private void startAction(){
        String ipanddb,username,password;
        Connection testconn = null;
        setTextArea("Starte Logbuch!");
        INIFile dummyini = null;
        String kopf = "INIinDB";
        String sanzahl = "INIAnzahl";
        INIFile inicontrol = null;
        int anzahl = 0;
        boolean overwrite = true;
        for(int i = 0; i < BuildIniTable.thisClass.anzahlmandanten;i++){
            try{
            Thread.sleep(1000); 
            overwrite = true;
            anzahl = 0;
            if(check[i].isSelected()){
                //Datenbankparameter einlesen
                File testfile = new File(BuildIniTable.thisClass.pfadzurini+"/"+BuildIniTable.thisClass.mandantIkvec.get(i)+"/inicontrol.ini");
                if(testfile.exists()){
                    int frage = JOptionPane.showConfirmDialog(null,"Für diesen Mandanten existiert bereits eine 'inicontrol.ini'\n"+
                            "Wollen Sie diese Datei mit der aktuellen Auswahl überschreiben?","Achtung wichtige Benutzeranfrage",JOptionPane.YES_NO_OPTION);
                    if(frage==JOptionPane.NO_OPTION){
                        overwrite = false;
                    }
                }
                if(overwrite){
                    inicontrol = new INIFile(BuildIniTable.thisClass.pfadzurini+"/"+BuildIniTable.thisClass.mandantIkvec.get(i)+"/inicontrol.ini");
                    inicontrol.addSection(kopf, null);
                    inicontrol.setStringProperty(kopf, sanzahl, "0", null);
                }
                setTextArea("\n\nErmittle Datenbankparameter für Mandant: "+BuildIniTable.thisClass.mandantIkvec.get(i));
                Thread.sleep(500);

                INIFile ini = new INIFile(BuildIniTable.thisClass.pfadzurini+"/"+BuildIniTable.thisClass.mandantIkvec.get(i)+"/rehajava.ini");
                ipanddb = ini.getStringProperty("DatenBank", "DBKontakt1");
                username = ini.getStringProperty("DatenBank", "DBBenutzer1");
                String pw = String.valueOf(ini.getStringProperty("DatenBank","DBPasswort1"));
                Verschluesseln man = Verschluesseln.getInstance();
                man.init();
                password = man.decrypt (pw);
                setTextArea("Datenbankparameter o.k.");
                Thread.sleep(500);
                
                setTextArea("Öffne Datenbank für Mandant : "+BuildIniTable.thisClass.mandantIkvec.get(i));
                testconn = BuildIniTable.thisClass.starteDB(ipanddb, username, password);
                Thread.sleep(500);

                if(testconn != null){
                    setTextArea("Datenbankkontakt hergestellt");
                    Thread.sleep(500);
                    setTextArea("Überprüfe ob Tabelle inidatei bereits existiert");
                    Thread.sleep(500);
                    Vector<Vector<String>>testvec = SqlInfo.holeFelder("show table status like 'inidatei'");
                    if(testvec.size() <= 0){
                        setTextArea("Tabelle inidatei existiert nicht");
                        Thread.sleep(500);
                        setTextArea("Erzeuge Tabelle");
                        Thread.sleep(500);
                        SqlInfo.sqlAusfuehren(createIniTableStmt());
                        Thread.sleep(500);
                    }else{
                        setTextArea("Tabelle inidatei existiert bereits");
                        Thread.sleep(500);
                    }
                    int fehler = 0;
                    /*
                    for(int i2 = 0; i2 < BuildIniTable.thisClass.inis.length;i2++){
                        try{
                            setTextArea("Schreibe INI in Tabelle -> "+BuildIniTable.thisClass.inis[i2]);
                            Thread.sleep(500);
                            dummyini = new INIFile(BuildIniTable.thisClass.pfadzurini+"/"+BuildIniTable.thisClass.mandantIkvec.get(i)+"/"+BuildIniTable.thisClass.inis[i2]);
                            schreibeIniInTabelle(BuildIniTable.thisClass.inis[i2],dummyini.saveToStringBuffer().toString().getBytes());
                            setTextArea("Datensatz für "+BuildIniTable.thisClass.inis[i2]+" erfolgreich erzeugt");
                            Thread.sleep(500);
                        }catch(Exception ex){
                            fehler += 1;
                            setTextArea("Fehler bei der Erstellung des Datensatzes ----> "+BuildIniTable.thisClass.inis[i2]);
                        }
                    }
                    */
                    for(int i2 = 0; i2 < tab.getRowCount();i2++){
                        try{
                            if( tab.getValueAt(i2, 0) == Boolean.TRUE){
                                setTextArea("Schreibe INI in Tabelle -> "+tab.getValueAt(i2, 1).toString());
                                Thread.sleep(500);
                                dummyini = new INIFile(BuildIniTable.thisClass.pfadzurini+"/"+BuildIniTable.thisClass.mandantIkvec.get(i)+"/"+tab.getValueAt(i2, 1).toString());
                                schreibeIniInTabelle(tab.getValueAt(i2, 1).toString(),dummyini.saveToStringBuffer().toString().getBytes());
                                setTextArea("Datensatz für "+tab.getValueAt(i2, 1).toString()+" erfolgreich erzeugt");
                                anzahl++;
                                if(overwrite){
                                    inicontrol.setStringProperty(kopf, "DBIni"+Integer.toString(anzahl),tab.getValueAt(i2, 1).toString() ,null);
                                }
                                Thread.sleep(500);

                            }
                        }catch(Exception ex){
                            fehler += 1;
                            setTextArea("Fehler bei der Erstellung des Datensatzes ----> "+tab.getValueAt(i2, 1).toString());
                        }
                    }
                    if(overwrite){
                        inicontrol.setStringProperty(kopf, sanzahl, Integer.toString(anzahl), null);
                        inicontrol.save();
                        setTextArea("Erstelle inicontrol.ini\n");
                    }
                    

                    setTextArea("\nUmsetzung der Inidateien in die Tabelle --> inidatei <-- mit "+Integer.toString(fehler)+" Fehlern beendet\n");
                }
                //Datenbank öffnen
            }
            }catch(Exception ex){
                setTextArea("Fehler!!!!!!");
                setTextArea(ex.getMessage());
            }
        }
        
    }
    private void setTextArea(String text){
        area.setText(area.getText()+text+"\n");
        area.setCaretPosition(area.getText().length());
    }
    private void activateListener(){
        al = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String cmd = e.getActionCommand();
                    if(cmd.equals("erzeugen")){
                        new SwingWorker<Void,Void>(){
                            @Override
                            protected Void doInBackground() throws Exception {
                                startAction();
                                return null;
                            }
                            
                        }.execute();
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
            
        };
    }
    public static String createIniTableStmt(){
        StringBuffer buf = new StringBuffer();
        buf.append( "CREATE TABLE IF NOT EXISTS inidatei (" );
        buf.append( "dateiname varchar(250) DEFAULT NULL," );
        buf.append( "inhalt text," );
        buf.append( "id int(11) NOT NULL AUTO_INCREMENT," );
        buf.append( "PRIMARY KEY (id)" );
        buf.append( ") ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1" ) ;
        return buf.toString();
    }
    /*******************************************************************************/   
    public static boolean schreibeIniInTabelle(INIFile file){
        boolean ret = false;
        try{
            schreibeIniInTabelle(file.getFileName(),file.saveToStringBuffer().toString().getBytes());
            file.getInputStream().close();
            file = null;
            ret = true;
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return ret;
    }
/*******************************************************************************/   
    public static boolean iniDateiTesten(String inidatei,String ik){
        boolean ret = false;
        try{
            if(SqlInfo.holeEinzelFeld("select dateiname from inidatei where dateiname = '"+inidatei+"' Limit 1").equals("")){
                schreibeIniInTabelle(inidatei,FileTools.File2ByteArray(new File(BuildIniTable.thisClass.pfadzurini+"/"+ik+"/"+inidatei)));  
            }
            ret = true;
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return ret;
    }   
    public static boolean schreibeIniInTabelle(String inifile,byte[] buf){
        boolean ret = false;
        try{
            
            
            Statement stmt = null;;
            ResultSet rs = null;
            PreparedStatement ps = null;
            try {
                stmt = (Statement) BuildIniTable.thisClass.conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_UPDATABLE );
                String select = null;
                if(SqlInfo.holeEinzelFeld("select dateiname from inidatei where dateiname='"+inifile+"' LIMIT 1").equals("")){
                        select = "insert into inidatei set dateiname = ? , inhalt = ?";
                }else{
                    select = "update inidatei set dateiname = ? , inhalt = ? where dateiname = '"+inifile+"'" ;                     
                }
                ps = (PreparedStatement) BuildIniTable.thisClass.conn.prepareStatement(select);
                ps.setString(1, inifile);
                ps.setBytes(2, buf);              
                ps.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }catch(Exception ex){
                ex.printStackTrace();
            }
            finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException sqlEx) {
                        rs = null;
                    }
                }   
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException sqlEx) { // ignore }
                        stmt = null;
                    }
                }
                if(ps != null){
                    ps.close();
                }
            }
            ret = true;
        }catch(Exception ex){
            
        }
        return ret;
    }
/*******************************************************************************/
    class MyIniTableModel extends DefaultTableModel{
           /**
         * 
         */
        private static final long serialVersionUID = 1L;

        public Class<?> getColumnClass(int columnIndex) {
            if(columnIndex==0){
                return Boolean.class;
            }
            return String.class;
        }

        public boolean isCellEditable(int row, int col) {
            if (col == 0){
                return true;
            }else{
                return false;
            }
        }
           
    }
}
