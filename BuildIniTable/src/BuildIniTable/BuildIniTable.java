package BuildIniTable;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import CommonTools.SqlInfo;
import CommonTools.ini.INIFile;

public class BuildIniTable implements WindowListener {
	public static BuildIniTable thisClass;
	SqlInfo sqlInfo = null;
	JFrame jFrame = null;
	Vector<String> mandantIkvec = new Vector<String>();
	Vector<String> mandantNamevec = new Vector<String>();
	public Connection conn;
	public boolean DbOk;
	public String[] inis = {"preisgruppen.ini","terminkalender.ini","gruppen.ini","icons.ini","fristen.ini","color.ini",
			"dta301.ini","gutachten.ini","ktraeger.ini","sqlmodul.ini","thbericht.ini"
	};
	
	public String pfadzurmandini;
	public String pfadzurini;
	public int anzahlmandanten;
	
	public static void main(String[] args) {
		BuildIniTable application = new BuildIniTable();
		application.getInstance().sqlInfo = new SqlInfo();
		application.getInstance().getJFrame();
		try{
			
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	private BuildIniTable getInstance(){
		return this;
	}
	
	public JFrame getJFrame(){
		try {
			UIManager.setLookAndFeel("com.jgoodies.looks.plastic.PlasticXPLookAndFeel");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		int frage = JOptionPane.showConfirmDialog(null,"Haben Sie von Ihren Datenbanken eine Sicherungskopie erstellt?","Achtung wichtige Benutzeranfrage", JOptionPane.YES_NO_OPTION);
		if(frage != JOptionPane.YES_OPTION){
			System.exit(0);
		}
		
		thisClass = this;
		thisClass.pfadzurmandini = thisClass.mandantTesten();
		thisClass.pfadzurini = thisClass.pfadzurmandini.replace("mandanten.ini","");		
		INIFile ini = new INIFile(thisClass.pfadzurmandini);
		thisClass.anzahlmandanten = Integer.parseInt(ini.getStringProperty("TheraPiMandanten", "AnzahlMandanten"));
		for(int i = 0; i < thisClass.anzahlmandanten; i++){
			mandantIkvec.add(ini.getStringProperty("TheraPiMandanten", "MAND-IK"+Integer.toString(i+1)));
			mandantNamevec.add(ini.getStringProperty("TheraPiMandanten", "MAND-NAME"+Integer.toString(i+1)));
		}		

		
		jFrame = new JFrame();
		sqlInfo.setFrame(jFrame);
		jFrame.addWindowListener(this);
		jFrame.setSize(600,600);
		jFrame.setPreferredSize(new Dimension(600,600));
		jFrame.setTitle("Thera-Pi  INI-Tabelle(n) erzeugen");
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.setLocationRelativeTo(null);
		ProcessPanel pan = new ProcessPanel();
		jFrame.getContentPane().setLayout(new BorderLayout());
		jFrame.getContentPane().add (pan, BorderLayout.CENTER);
		//jFrame.pack();
		jFrame.setVisible(true);
		
		
		return jFrame;
	}
	private String mandantTesten(){
		String mandini = java.lang.System.getProperty("user.dir");
		mandini = mandini.replace("\\","/")+"/ini/mandanten.ini";
		System.out.println(mandini);
		if(! mandIniExist(mandini)){
			JOptionPane.showMessageDialog(null, "Das System kann die mandanten.ini nicht finden!\nBitte navigieren Sie in das Verzeichnis in dem sich die\nmandanten.ini befindet und wählen Sie die mandanten.ini aus!");
			String sret = dateiDialog(mandini); 
			if(! sret.endsWith("/ini/mandanten.ini")){
				JOptionPane.showMessageDialog(null,"Sie haben die falsche(!!!) Datei ausgewählt, das Programm wird beendet!");
				System.exit(0);
			}
			return sret;
		}else{
			return mandini;
		}
	}
	private boolean mandIniExist(String abspath){
		File f = new File(abspath);
		return f.exists();
	}
	private String dateiDialog(String pfad){
		String sret = "";
		final JFileChooser chooser = new JFileChooser("Verzeichnis wählen");
        chooser.setDialogType(JFileChooser.OPEN_DIALOG);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        final File file = new File(pfad);

        chooser.setCurrentDirectory(file);

        chooser.addPropertyChangeListener(new PropertyChangeListener() {
            public void propertyChange(PropertyChangeEvent e) {
                if (e.getPropertyName().equals(JFileChooser.SELECTED_FILE_CHANGED_PROPERTY)
                        || e.getPropertyName().equals(JFileChooser.DIRECTORY_CHANGED_PROPERTY)) {
                    final File f = (File) e.getNewValue();
                }
            }

        });
        chooser.setVisible(true);
        final int result = chooser.showOpenDialog(null);

        if (result == JFileChooser.APPROVE_OPTION) {
            File inputVerzFile = chooser.getSelectedFile();
            String inputVerzStr = inputVerzFile.getPath();
            if(inputVerzFile.getName().trim().equals("")){
            	sret = "";
            }else{
            	sret = inputVerzStr.trim().replace("\\", "/");
            }
        }else{
        	sret = "";
        }
        chooser.setVisible(false); 
        return sret;
	}
	
	
	public Connection starteDB(String hostAndDb, String user, String pw)throws Exception{
		final String sDB = "SQL";
		if (conn != null){
			try{
			conn.close();}
			catch(final SQLException e){}
		}
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException e) {
			//e.printStackTrace();
    		System.out.println(sDB+"Treiberfehler: " + e.getMessage());
    		DbOk = false;
    		return null;
		} catch (IllegalAccessException e) {
			//e.printStackTrace();
    		System.out.println(sDB+"Treiberfehler: " + e.getMessage());
    		DbOk = false;
    		return null;
		} catch (ClassNotFoundException e) {
			//e.printStackTrace();
    		System.out.println(sDB+"Treiberfehler: " + e.getMessage());
    		DbOk = false;
    		return null;
		}	
    	try {
			conn = (Connection) DriverManager.getConnection(hostAndDb,user,pw);
			sqlInfo.setConnection(conn);
			DbOk = true;
			System.out.println("Datenbankkontakt hergestellt");
    	} 
    	catch (final SQLException ex) {
    		//System.out.println("SQLException: " + ex.getMessage());
    		//System.out.println("SQLState: " + ex.getSQLState());
    		//System.out.println("VendorError: " + ex.getErrorCode());
    		DbOk = false;
    		return null;
    	}
        return conn;
	}
	
	@Override
	public void windowOpened(WindowEvent e) {
	}
	@Override
	public void windowClosing(WindowEvent e) {
		if(conn != null){
			try {
				conn.close();
				System.out.println("Connection geschlossen");
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
	@Override
	public void windowClosed(WindowEvent e) {
	}
	@Override
	public void windowIconified(WindowEvent e) {
	}
	@Override
	public void windowDeiconified(WindowEvent e) {
	}
	@Override
	public void windowActivated(WindowEvent e) {
	}
	@Override
	public void windowDeactivated(WindowEvent e) {
	}
	
}
