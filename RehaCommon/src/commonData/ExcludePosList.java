package commonData;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Liste mit Heilmittelpositionen, die von der Behandlungs- u. HB-Zaehlung auszunehmen sind
 */
public class ExcludePosList extends HmPosList {
    static String[] tmpArr = {"X4002", "X0204", "X3011", "X3010", "X3008" };
    static String[] tmpArrErgoSchienen = {"54405", "54406" };
    
    public ExcludePosList(String prefix) {
        super (Arrays.asList(tmpArr));
        this.addAll(Arrays.asList(tmpArrErgoSchienen));
        this.setPrefix(prefix);
    }

}
