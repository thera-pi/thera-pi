package commonData;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Liste mit Heilmittelpositionen, die nur 1x pro Verordnung erlaubt sind
 */
public class EinzelPosList extends HmPosList {

    static String[] tmpArr = {
            "X4002",    // Funktionsanalyse (Ergo)
            "X9944",    // Hygienepauschale ab 2020-05 bis 2022-06
            "X1907"     // Hygienepauschale für Heimbewohner ab 2023-01
            };
    
    public EinzelPosList(String prefix) {
        super (Arrays.asList(tmpArr));
        this.setPrefix(prefix);
    }

}
