package commonData;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class HmPosList extends ArrayList {
    
    /**
     * init mit Daten aus XML-Version der HMR
     */
    @SuppressWarnings("unchecked")
    public HmPosList(List<String> initValues) {
        super(initValues);
    }
    
    /**
     * init mit Daten aus Tabelle hmrcheck (Disziplin-spezifisch)
     */
    public HmPosList(String[] hmPos, String prefix) {
        super();
        String tmpStr = null;
        for (int i = 0; i < hmPos.length; i++) {
            tmpStr = prefix + hmPos[i];
            if (!this.contains(tmpStr)) {
                this.add(tmpStr);
            }
        }
    }

    /**
     * wandelt generische HM-Codes der HMR in Disziplin-spezifische
     */
    public void setPrefix(String prefix) {
        ArrayList<String> tmpList = new ArrayList<String>();
        String tmpStr = null;
        for (int i = 0; i < this.size(); i++) {
            tmpStr = ((String) this.get(i)).replace("X", prefix);
            this.set(i, tmpStr);
        }
    }
    
}
