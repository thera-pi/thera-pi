package commonData;

import java.util.ArrayList;
import java.util.Arrays;

public class HbReplacablePosList extends HmPosList {

    static String[] tmpArr = {"X9901", "X9902" };
    String currPrefix = null;
    
    public HbReplacablePosList(String prefix) {
        super (Arrays.asList(tmpArr));
        this.setPrefix(prefix);
        currPrefix = prefix;
    }

    /**
     * wandelt alte HB-Positionen entsprechend Rahmenvertrag Physio/Massage 21-07-21 um
     * x9901       HB-Einzel (alt)
     * x9902       HB-mehrere (alt)
     */
    public String getReplacementFor(String hmPos) {
        String retVal = hmPos;
        if (this.contains(hmPos)) {
            switch (hmPos.substring(1)) {
            case "9901":
                retVal = currPrefix + "9933";
                break;
            case "9902":
                retVal = currPrefix + "9934";
                break;
            default:
                retVal = hmPos;
            }
        }
        return (retVal);
    }
    
}

