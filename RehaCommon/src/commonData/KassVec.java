package commonData;

import java.util.Vector;

import CommonTools.SqlInfo;
import CommonTools.StringTools;

public class KassVec {
    private Vector<Vector<String>> vecvec_kassen;
    private Vector<String> vec_kasse;    


    // Indices für Zugriffe auf vec_kasse
    final int KUERZEL = 0, PREISGRUPPE = 1, KASSEN_NAM1 = 2, KASSEN_NAM2 = 3, STRASSE = 4, PLZ = 5, ORT = 6, POSTFACH = 7, FAX = 8,
            TELEFON = 9, IK_NUM = 10, KV_NUMMER = 11, MATCHCODE = 12, KMEMO = 13, RECHNUNG = 14,
            IK_KASSE = 15, IK_PHYSIKA = 16, IK_NUTZER = 17, IK_KOSTENT = 18, IK_KVKARTE = 19, IK_PAPIER = 20, 
            EMAIL1 = 21, EMAIL2 = 22, EMAIL3 = 23, ID = 24, HMRABRECHNUNG = 25,
            PGKG = 26, PGMA = 27, PGER = 28, PGLO = 29, PGRH = 30, PGPO = 31;

    public KassVec() {
        vecvec_kassen = new Vector<Vector<String>>();
        vec_kasse = new Vector<String>();
    }
    
    public boolean init(int idInDB) {
        String cmd = "select * from kass_adr where id='" + idInDB + "' LIMIT 1";
        return getRecord (cmd);
    }
    
    public boolean init(String idInDb) {
        return init(Integer.parseInt(idInDb));
    }

    private boolean getRecord (String cmd) {
        this.vecvec_kassen = SqlInfo.holeFelder(cmd);
        if(this.vecvec_kassen.size()<=0){
            // KasseVektor ist leer
            this.vec_kasse = null;
            return Boolean.FALSE;
        }
        setTo1stVec_kasse();
        return Boolean.TRUE;
    }

    public boolean createEmptyVec() {
        try {
            String cmd = "describe arzt";
            this.vecvec_kassen = SqlInfo.holeFelder(cmd);
            for (int i = 0; i < this.vecvec_kassen.size(); i++){
                this.vec_kasse.add(i, "");
            }
            return Boolean.TRUE;
        } catch (Exception e) {
            e.printStackTrace();
            return Boolean.FALSE;
        }
    }

    public KassVec getInstance(){
        return this;
    }

    private void setTo1stVec_kasse() {
        this.vec_kasse = this.vecvec_kassen.get(0);
    }

    private int getIntAt(int index){
        return StringTools.ZahlTest(this.vec_kasse.get(index));
    }
    private void setIntAt(int index, int data){
        this.vec_kasse.set(index, Integer.valueOf(data).toString());
    }
    private void setIntAt(int index, String data){
        this.vec_kasse.set(index, Integer.valueOf(data).toString());
    }
    
    private String getStringAt(int index){
        return this.vec_kasse.get(index).trim();
    }
    private void setStringAt(int index, String data) {
        this.vec_kasse.set(index, data.trim());
    }

    /*
     * Kompatibilitätsmodus
     */
    public void setvecvec_kassen(Vector<Vector<String>> vecvec_tmp) {
        this.vecvec_kassen = vecvec_tmp;
        setTo1stVec_kasse();
    }

    public Vector<Vector<String>> getVecVec_kassen() {
        return this.vecvec_kassen;
    }
    // Kompatibilitätsmodus Ende

    public Vector<String> getVec_kasse() {
        return vec_kasse;
    }

    public void setVec_kasse(Vector<String> vec_tmp) {
        this.vec_kasse = vec_tmp;
    }
    
    public int getVecSize() {
        return vec_kasse.size();
    }

    public boolean isEmpty(){
        if (getVecSize() <= 0){
            return true;
        }else{
            return false;           
        }
    }

    public String getKuerzel() {
        return getStringAt(KUERZEL);
    }
    public void setKuerzel(String data) {
        setStringAt(KUERZEL, data);
    }

    public String getPrGrp() {
        return getStringAt(PREISGRUPPE);
    }
    public void setPrGrp(String data) {
        setStringAt(PREISGRUPPE, data);
    }

    public String getName1() {
        return getStringAt(KASSEN_NAM1);
    }
    public void setName1(String data) {
        setStringAt(KASSEN_NAM1, data);
    }

    public String getName2() {
        return getStringAt(KASSEN_NAM2);
    }
    public void setName2(String data) {
        setStringAt(KASSEN_NAM2, data);
    }

    public String getStr() {
        return getStringAt(STRASSE);
    }
    public void setStr(String data) {
        setStringAt(STRASSE, data);
    }

    public String getPlz() {
        return getStringAt(PLZ);
    }
    public void setPlz(String data) {
        setStringAt(PLZ, data);
    }

    public String getOrt() {
        return getStringAt(ORT);
    }
    public void setOrt(String data) {
        setStringAt(ORT, data);
    }

    public String getPFach() {
        return getStringAt(POSTFACH);
    }
    public void setPFach(String data) {
        setStringAt(POSTFACH, data);
    }

    public String getFax() {
        return getStringAt(FAX);
    }
    public void setFax(String data) {
        setStringAt(FAX, data);
    }

    public String getTel() {
        return getStringAt(TELEFON);
    }
    public void setTel(String data) {
        setStringAt(TELEFON, data);
    }

    public String getIkNum() {
        return getStringAt(IK_NUM);
    }
    public void setIkNum(String data) {
        setStringAt(IK_NUM, data);
    }

    public String getKvNum() {
        return getStringAt(IK_NUM);
    }
    public void setKvNum(String data) {
        setStringAt(IK_NUM, data);
    }

    public String getMatch() {
        return getStringAt(MATCHCODE);
    }
    public void setMatch(String data) {
        setStringAt(MATCHCODE, data);
    }

    public String getKMemo() {   // Notiz in Kassen-Übersicht
        return getStringAt(KMEMO);
    }
    public void setKMemo(String data) {
        setStringAt(KMEMO, data);
    }

    public String getRechng() {
        return getStringAt(RECHNUNG);
    }
    public void setRechng(String data) {
        setStringAt(RECHNUNG, data);
    }

    public String getIkKass() {
        return getStringAt(IK_KASSE);
    }
    public void setIkKass(String data) {
        setStringAt(IK_KASSE, data);
    }

    public String getIkPhys() {
        return getStringAt(IK_PHYSIKA);
    }
    public void setIkPhys(String data) {
        setStringAt(IK_PHYSIKA, data);
    }

    public String getIkNutz() {
        return getStringAt(IK_NUTZER);
    }
    public void setIkNutz(String data) {
        setStringAt(IK_NUTZER, data);
    }

    public String getIkKTraeger() {
        return getStringAt(IK_KOSTENT);
    }
    public void setIkKTraeger(String data) {
        setStringAt(IK_KOSTENT, data);
    }

    public String getIkKVK() {
        return getStringAt(IK_KVKARTE);
    }
    public void setIkKVK(String data) {
        setStringAt(IK_KVKARTE, data);
    }

    public String getIkPap() {
        return getStringAt(IK_PAPIER);
    }
    public void setIkPap(String data) {
        setStringAt(IK_PAPIER, data);
    }

    public String getEMail1() {
        return getStringAt(EMAIL1);
    }
    public void setEMail1(String data) {
        setStringAt(EMAIL1, data);
    }

    public String getEMail2() {
        return getStringAt(EMAIL2);
    }
    public void setEMail2(String data) {
        setStringAt(EMAIL2, data);
    }

    public String getEMail3() {
        return getStringAt(EMAIL3);
    }
    public void setEMail3(String data) {
        setStringAt(EMAIL3, data);
    }

    public int getId() {
        return getIntAt(ID);
    }
    public String getIdS() {
        return getStringAt(ID);
    }
    public void setId(int data) {
        setIntAt(ID, data);
    }

    public String getHmAbrg() {
        return getStringAt(HMRABRECHNUNG);
    }
    public void setHmAbrg(String data) {
        setStringAt(HMRABRECHNUNG, data);
    }

    public String getPgKg() {
        return getStringAt(PGKG);
    }
    public void setPgKg(String data) {
        setStringAt(PGKG, data);
    }

    public String getPgMa() {
        return getStringAt(PGMA);
    }
    public void setPgMa(String data) {
        setStringAt(PGMA, data);
    }

    public String getPgEr() {
        return getStringAt(PGER);
    }
    public void setPgEr(String data) {
        setStringAt(PGER, data);
    }

    public String getPgLo() {
        return getStringAt(PGLO);
    }
    public void setPgLo(String data) {
        setStringAt(PGLO, data);
    }

    public String getPgRh() {
        return getStringAt(PGRH);
    }
    public void setPgRh(String data) {
        setStringAt(PGRH, data);
    }

    public String getPgPo() {
        return getStringAt(PGPO);
    }
    public void setPgPo(String data) {
        setStringAt(PGPO, data);
    }

    public void writeKasse2DB(){
        StringBuffer cmd = new StringBuffer();
        cmd.append("kuerzel='" + getKuerzel() + "', "); 
        cmd.append("preisgruppe='" + getPrGrp() + "', ");
        cmd.append("kassen_nam1='" + getName1() + "', ");
        cmd.append("kassen_nam2='" + getName2() + "', ");
        cmd.append("strasse='" + getStr() + "', ");
        cmd.append("plz='" + getPlz() + "', ");
        cmd.append("ort='" + getOrt() + "', ");
        cmd.append("postfach='" + getPFach() + "', ");
        cmd.append("fax='" + getFax() + "', ");
        cmd.append("telefon='" + getTel() + "', ");
        cmd.append("ik_num='" + getIkNum() + "', ");
        cmd.append("kv_nummer='" + getKvNum() + "', ");        
        cmd.append("matchcode='" + getMatch() + "', ");
        cmd.append("kmemo='" + getKMemo() + "', ");
        cmd.append("rechnung='" + getRechng() + "', ");
        cmd.append("ik_kasse='" + getIkKass() + "', ");
        cmd.append("ik_nutzer='" + getIkNutz() + "', ");
        cmd.append("ik_kostent='" + getIkKTraeger() + "', ");
        cmd.append("ik_kvkarte='" + getIkKVK() + "', ");
        cmd.append("ik_papier='" + getIkPap() + "', ");
        cmd.append("email1='" + getEMail1() + "', ");
        cmd.append("email2='" + getEMail2() + "', ");
        cmd.append("email3='" + getEMail3() + "', ");
        cmd.append("id='" + getId() + "', ");
        cmd.append("hmrabrechnung='" + getHmAbrg() + "', ");
        cmd.append("pgkg='" + getPgKg() + "', ");
        cmd.append("pgma='" + getPgMa() + "', ");
        cmd.append("pger='" + getPgEr() + "', ");
        cmd.append("pglo='" + getPgLo() + "', ");
        cmd.append("pgrh='" + getPgRh() + "', ");
        cmd.append("pgpo='" + getPgPo() + "'");
        if (getId() < 0) {
            cmd.insert(0, "insert into kass_adr set "); // insert new
        } else {
            cmd.insert(0, "update kass_adr set ");  // update existing
            cmd.append(" where id = " + getId());
        }
        SqlInfo.sqlAusfuehren(cmd.toString());
        int tmp = Integer.valueOf(SqlInfo.holeEinzelFeld(new String ("select max(id) from kass_adr")));
        setId(tmp);
    }
}
