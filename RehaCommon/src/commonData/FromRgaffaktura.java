package commonData;

import java.util.Vector;

import CommonTools.DatFunk;
import CommonTools.SqlInfo;

public class FromRgaffaktura {
    /**
     * Daten aus 'rgaffaktura' bereitstellen
     */
    int posRnr = 0;
    int posBetragOffen = 1;
    int posRgDat = 2;
    int posRgBetrag = 3;
    int posId = 4;

    Vector<String> currData;

    public FromRgaffaktura(String rezNr) {
        String cmd = "select a.rnr,a.roffen,a.rdatum,a.rgbetrag,a.id from rgaffaktura a "
                + "left join storno_rechnung b on a.rnr = b.rnr where a.reznr='" + rezNr
                + "' and b.id IS null and a.rnr like 'RGR-%' LIMIT 1";
        Vector<Vector<String>> tmp = SqlInfo.holeFelder(cmd);
        if (tmp.isEmpty()) {
            this.currData = null;   
        } else {
            this.currData = tmp.get(0);
        }
    }
    public String getRgrNr () {
        return currData.get(posRnr);
    }
    public String getRgDatum () {
        String tmp = currData.get(posRgDat);
        tmp = DatFunk.sDatInDeutsch(tmp);
        return tmp;
    }
    public String getBetrag () {
        String tmp = currData.get(posRgBetrag)
                             .replace(".", ",");
        return tmp;
    }
    public String getBetrOffen() {
        String tmp = currData.get(posBetragOffen)
                             .replace(".", ",");
        return tmp;
    }
    public String getId() {
        String tmp = currData.get(posId);
        return tmp;
    }
    public boolean hasRGR() {
        return currData != null ? true : false;
    }
    public boolean isOffen() {
        return "0.00".equals(currData.get(posBetragOffen)) ? false : true;
    }
}