package commonData;

import java.util.Arrays;

public class HbDeletePosList extends HmPosList {

    static String[] tmpArr = {"X9906", "X9907", "X9909", "X9910", "X9911" };
    String currPrefix = null;
    
    public HbDeletePosList(String prefix) {
        super (Arrays.asList(tmpArr));
        this.setPrefix(prefix);
        currPrefix = prefix;
    }

    /**
     * löscht obsolete HB-Positionen entsprechend Rahmenvertrag Physio/Massage 21-07-21
     * x9906       HP pauschal (alt)
     * x9907       Wegegeld je km °
     * x9909       Wegegeld pausch. innerh. geschl. Ortschaften (alt)
     * x9910       Wegegeld je km bei Überschreiten der Ortsgrenze (alt)
     * x9911       ??
     */
}
