package CommonTools;

import java.util.Vector;

import javax.swing.JOptionPane;

public class StringTools {
    private static String TRIGGER_EINRUECKEN = "          ";

    public static String EGross(String string) {
        if (string == null) {
            return "";
        }
        if (string.trim()
                  .equals("")) {
            return "";
        }

        String test = String.valueOf(string.trim()).toLowerCase();
        String neuString = "";
        try {
            boolean zerhackt = false;
            for (int y = 0; y < 1; y++) {

                if ((test.indexOf(" ") < 0) && (test.indexOf("-") < 0) && (test.indexOf("/") < 0)) {
                    neuString = test.substring(0, 1)
                                    .toUpperCase()
                            + (test.length() > 1 ? test.substring(1) : "");
                    test = String.valueOf(neuString.trim());
                    /***********/
                    try {
                        test = setCase (test, "Mc");
                        test = setCase (test, "D'");
//                        test = setCase (test, "Mac"); // Mackenroth -> MacKenroth
                    } catch (Exception ex) {
                        
                    }
                    /***********/
                    return test;
                }

                if (test.indexOf(" ") > -1) {
                    String[] splitString = test.split(" ");
                    for (int i = 0; i < splitString.length; i++) {
                        neuString = neuString + (splitString[i].substring(0, 1)
                                                               .toUpperCase())
                                + (splitString[i].length() > 1 ? splitString[i].substring(1) : "");
                        neuString = neuString + " ";
                    }
                    test = String.valueOf(neuString.trim());
                    zerhackt = true;
                }

                if (test.indexOf(" - ") > -1) {
                    neuString = "";
                    String[] splitString = test.split(" - ");
                    for (int i = 0; i < splitString.length; i++) {
                        neuString = neuString + (splitString[i].substring(0, 1)
                                                               .toUpperCase())
                                + (splitString[i].length() > 1 ? splitString[i].substring(1) : "");
                        neuString = neuString + (i < (splitString.length - 1) ? " - " : "");
                    }
                    test = String.valueOf(neuString.trim());
                    zerhackt = true;
                    break;
                }

                if (test.indexOf("-") > -1) {
                    neuString = "";
                    String praefix = "";
                    String[] splitString = test.split("-");
                    if (splitString[0].indexOf(" ") >= 0) {
                        praefix = splitString[0].substring(0, splitString[0].lastIndexOf(" ") + 1);
                        splitString[0] = String.valueOf(splitString[0].substring(splitString[0].lastIndexOf(" ") + 1))
                                               .trim();
                    }
                    for (int i = 0; i < splitString.length; i++) {
                        if (i == 0) {
                            neuString = neuString + praefix;
                        }
                        neuString = neuString + (splitString[i].substring(0, 1)
                                                               .toUpperCase())
                                + (splitString[i].length() > 1 ? splitString[i].substring(1) : "");
                        neuString = neuString + (i < (splitString.length - 1) ? "-" : "");
                    }
                    test = String.valueOf(neuString.trim());
                    zerhackt = true;
                    // System.out.println("in - Ergebnis = "+test);
                }

                if (test.indexOf("/") > -1 && !zerhackt) {
                    neuString = "";
                    String[] splitString = test.split("/");
                    for (int i = 0; i < splitString.length; i++) {
                        splitString[i] = splitString[i].trim();

                        neuString = neuString + (splitString[i].substring(0, 1)
                                                               .toUpperCase())
                                + (splitString[i].length() > 1 ? splitString[i].substring(1) : "");
                        neuString = neuString + (i < (splitString.length - 1) ? " / " : "");
                    }
                    test = String.valueOf(neuString.trim());
                    // System.out.println(test);
                }
            }
            test = replaceMatches(test, "prof.", "Prof.");
            test = replaceMatches(test, "dr.", "Dr.");
            test = replaceMatches(test, "dres.", "Dres.");
            test = replaceMatches(test, " Med. ", " med. ");
            test = replaceMatches(test, "Med.", "med.");
            test = replaceMatches(test, " Von ", " von ");
            test = replaceMatches(test, " VON ", " von ");
            test = replaceMatches(test, "Von ", " von ");
            test = replaceMatches(test, " Und ", " und ");
            test = replaceMatches(test, " UND ", " und ");
            test = replaceMatches(test, " Zu ", " zu ");
            test = replaceMatches(test, " ZU ", " zu ");

            test = replaceMatches(test, " An ", " an ");
            test = replaceMatches(test, " Am ", " am ");
            test = replaceMatches(test, " Auf ", " auf ");
            test = replaceMatches(test, " Der ", " der ");
            test = replaceMatches(test, " Die ", " die ");
            test = replaceMatches(test, " Bei ", " bei ");
            test = replaceMatches(test, " Beim ", " beim ");
            test = replaceMatches(test, " Den ", " den ");
            test = replaceMatches(test, " Dem ", " dem ");
            test = replaceMatches(test, " Ob ", " ob ");
            test = replaceMatches(test, " Über ", " über ");
            test = replaceMatches(test, " Überm ", " überm ");
            test = replaceMatches(test, " Unter ", " unter ");
            test = replaceMatches(test, " A.d. ", " a.d. ");
            test = replaceMatches(test, " U.d. ", " u.d. ");

            test = replaceMatches(test, "Aok", "AOK");
            test = replaceMatches(test, "-Die ", "-die ");
            test = replaceMatches(test, "gesundheitskasse", "Gesundheitskasse");
            test = replaceMatches(test, "Gek", "GEK");
            test = replaceMatches(test, "Bkk", "BKK");
            test = replaceMatches(test, "Bek ", "BEK ");
            test = replaceMatches(test, "Ikk", "IKK");
            test = replaceMatches(test, "Lkk", "LKK");
            test = replaceMatches(test, "Tkk", "TKK");
            test = replaceMatches(test, "Dak", "DAK");
            test = replaceMatches(test, "Ddg", "DDG");
            test = replaceMatches(test, " str.", " Str.");

            test = replaceMatches(test, " U. ", " u. ");

        } catch (java.lang.StringIndexOutOfBoundsException ex) {
            //// System.out.println(ex);
            return "" + test;
        } catch (Exception ex) {
            return "" + test;
        }

        return test;
        // return neuString.trim();
    }

    private static String setCase(String name, String begin) {
        String txt = name;
        if (txt.startsWith(begin)) {
            int pos = begin.length() + 1;
            if (txt.length() >= pos) {
                if (!txt.substring(pos - 1, pos)
                        .equals(" ")) {
                    txt = begin + txt.substring(pos - 1, pos)
                                     .toUpperCase()
                            + txt.substring(pos);
                }
            }
        }
        return txt;
    }

    private static String replaceMatches(String txt,String find, String replace) {
        String test = txt;
        if (test.indexOf(find) > -1) {
            String neu= test.replaceAll(find, replace);
            test = String.valueOf(neu.trim());
        }
        return test;
    }


    public static String NullTest(String string) {
        if (string == null) {
            return "";
        } else {
            return string;
        }
    }

    public static int ZahlTest(String string) {
        if (string == null) {
            return -1;
        } else {
            int zahl;
            try {
                zahl = Integer.valueOf(string.trim());
            } catch (NumberFormatException ex) {
                zahl = -1;
            }
            return zahl;
        }
    }

    public static String Escaped(String string) {
        String escaped = string.replaceAll("\'", "\\\\'");
        escaped = escaped.replaceAll("\"", "\\\\\"");
        return escaped;
    }

    public static String EscapedDouble(String string) {
        String escaped = string.replaceAll("\'", "\\\\\\'");
        return escaped;
    }

    public static String Escape4SQL(String string) {
        String escaped = string.replaceAll("'", "''");
        return escaped;
    }

    public static String fuelleMitZeichen(String string, String zeichen, boolean vorne, int endlang) {
        String orig = string;
        String praefi = zeichen;
        String dummy = "";
        String sret = "";
        int solllang = endlang;
        int istlang = orig.length();
        int differenz = solllang - istlang;
        if (differenz > 0) {
            for (int i = 0; i < differenz; i++) {
                dummy = dummy + praefi;
            }
            if (vorne) {
                sret = dummy + orig;
            } else {
                sret = orig + dummy;
            }
        } else {
            sret = orig;
        }
        return sret;
    }

    public static String getDisziplin(String reznr) {
        if (reznr.startsWith("KG")) {
            return "Physio";
        } else if (reznr.startsWith("MA")) {
            return "Massage";
        } else if (reznr.startsWith("ER")) {
            return "Ergo";
        } else if (reznr.startsWith("LO")) {
            return "Logo";
        } else if (reznr.startsWith("RH")) {
            return "Reha";
        } else if (reznr.startsWith("PO")) {
            return "Podo";
        } else if (reznr.startsWith("RS")) {
            return "Rsport";
        } else if (reznr.startsWith("FT")) {
            return "Ftrain";
        }
        return "Physio";
    }

    public static Vector<String> fliessTextZerhacken(String textcontent, int max_line_lenght, String trenner) {
        // Ausgabe in eine Datei schreiben
        /*
         * RandomAccessFile file = null; Writer out = null; try { file = new
         * RandomAccessFile("C:/kontroll.txt", "rw"); out = new OutputStreamWriter(new
         * FileOutputStream(file.getFD()), "UTF-8"); } catch (FileNotFoundException e) {
         * e.printStackTrace(); } catch (UnsupportedEncodingException e) {
         * e.printStackTrace(); } catch (IOException e) { e.printStackTrace(); }
         */
        Vector<String> dtavec = new Vector<String>();
        try {
            String[] teile = textcontent.split(trenner);
            String ohneumbruch = null;
            String LEER = " ";
            String reststring = "";
            String dummy = "";
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            for (i = 0; i < teile.length; i++) {
                // out.append("0: Eintritt in Uebergeordnete
                // for/next"+System.getProperty("line.separator"));
                ohneumbruch = teile[i].replace("\f", "")
                                      .replace("\r", "")
                                      .replace("\n", "")
                                      .replace("\t", " ");
                reststring = String.valueOf(ohneumbruch);
                if (ohneumbruch.length() == 0) {
                    dtavec.add("");
                    // out.append("1: = leer"+System.getProperty("line.separator"));
                } else if (ohneumbruch.length() > 0 && ohneumbruch.length() <= max_line_lenght) {
                    if (ohneumbruch.trim()
                                   .length() > 0) {
                        // out.append("2: "+(ohneumbruch.startsWith(TRIGGER_EINRUECKEN) ? ohneumbruch :
                        // ohneumbruch.trim())+System.getProperty("line.separator"));
                        dummy = testeString(
                                (ohneumbruch.startsWith(TRIGGER_EINRUECKEN) ? ohneumbruch : ohneumbruch.trim()),
                                max_line_lenght);
                        // dtavec.add( (ohneumbruch.startsWith(TRIGGER_EINRUECKEN) ? ohneumbruch :
                        // ohneumbruch.trim()) );
                        dtavec.add(String.valueOf(dummy));
                    }
                    // hier neu >= anstatt >
                } else if (ohneumbruch.length() > max_line_lenght) {
                    // out.append("3: = reststring");
                    for (i2 = 0; i2 < reststring.length(); i2++) {
                        // out.append("4.0: Eintritt in Pruefung: "+(reststring.length() <
                        // max_line_lenght ? reststring : " Laenge ="+reststring.length()
                        // )+System.getProperty("line.separator"));
                        if (reststring.length() <= max_line_lenght) {
                            if (reststring.trim()
                                          .length() > 0) {
                                dummy = testeString(
                                        (reststring.startsWith(TRIGGER_EINRUECKEN) ? reststring : reststring.trim()),
                                        max_line_lenght);
                                // dtavec.add( (reststring.startsWith(TRIGGER_EINRUECKEN) ? reststring :
                                // reststring.trim()) );
                                dtavec.add(String.valueOf(dummy));
                                // out.append("4.1: "+(ohneumbruch.startsWith(TRIGGER_EINRUECKEN) ? ohneumbruch
                                // : ohneumbruch.trim())+System.getProperty("line.separator"));
                            }
                            break;
                        } else {

                            for (i3 = max_line_lenght - 1; i3 >= 0; i3--) {
                                // System.out.println("6: untere for/next");
                                dummy = "";
                                if (reststring.substring(i3, i3 + 1)
                                              .equals(LEER)) {
                                    if (reststring.substring(0, i3)
                                                  .trim()
                                                  .length() > 0) {
                                        // dtavec.add(reststring.substring(0,i3).trim());
                                        dummy = testeString((reststring.substring(0, i3)
                                                                       .startsWith(TRIGGER_EINRUECKEN)
                                                                               ? reststring.substring(0, i3)
                                                                               : reststring.substring(0, i3)
                                                                                           .trim()),
                                                max_line_lenght);
                                        // dtavec.add( (reststring.substring(0,i3).startsWith(TRIGGER_EINRUECKEN) ?
                                        // reststring.substring(0,i3) : reststring.substring(0,i3).trim()) );
                                        dtavec.add(String.valueOf(dummy));
                                        // out.append("7: "+(reststring.substring(0,i3).startsWith(TRIGGER_EINRUECKEN) ?
                                        // reststring.substring(0,i3) :
                                        // reststring.substring(0,i3).trim())+System.getProperty("line.separator"));
                                    }
                                    // reststring = reststring.substring(i3).trim();
                                    reststring = String.valueOf((reststring.substring(i3)
                                                                           .startsWith(TRIGGER_EINRUECKEN)
                                                                                   ? reststring.substring(i3)
                                                                                   : reststring.substring(i3)
                                                                                               .trim()));
                                    // out.append("8: "+reststring+System.getProperty("line.separator"));
                                    if ((reststring.length() <= max_line_lenght) && (i2 >= reststring.length())) {
                                        // out.append("11!!!!!: Laenge = "+reststring.length()+" / "+
                                        // " Stand i2 = "+i2+" / Stand i3 = "+i3+" String =
                                        // "+reststring+System.getProperty("line.separator"));
                                        dummy = testeString(reststring, max_line_lenght);
                                        dtavec.add(String.valueOf(dummy));
                                        String mes = "Achtung!\n" + "Spezielle Fallkonstellation\n" + "Reststring = "
                                                + reststring + " / Laenge =" + Integer.toString(reststring.length())
                                                + "\n" + "Stand i3 = " + Integer.toString(i3) + "\n" + "Stand i2 = "
                                                + Integer.toString(i2) + "\n";
                                        // JOptionPane.showMessageDialog(null, mes);
                                    }
                                    break;
                                } else if (i == 0) {
                                    // hier neu >= anstatt >
                                    if (reststring.length() > max_line_lenght) {
                                        // out.append("9: > max_line_length"+System.getProperty("line.separator"));
                                        continue;
                                    } else {
                                        dummy = testeString(reststring, max_line_lenght);
                                        // dtavec.add(reststring);
                                        dtavec.add(String.valueOf(dummy));
                                        // out.append("10: "+reststring+System.getProperty("line.separator"));
                                        break;
                                    }

                                }
                            }
                        }
                        /******** ende der i2 for/next **********/
                    }
                }
            }
            // out.flush();
            // out.close();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Fehler in Fließtextaufbereitung. Bericht keinesfalls!!!! versenden");
        }
        /*
         * for(int i = 0; i < dtavec.size();i++){
         * System.out.println("L="+StringTools.fuelleMitZeichen(
         * Integer.toString(dtavec.get(i).length()), "0", true, 3)+": "+dtavec.get(i));
         * }
         */
        return (Vector<String>) dtavec.clone();
    }

    public static String testeString(String string, int lang) {
        if (string.length() > lang) {
            JOptionPane.showMessageDialog(null, "Der String " + string + " ist länger als" + Integer.toString(lang)
                    + " Zeichen\n" + "Bericht bitte keinesfalls versenden!!!!!");
        }
        return string;
    }

    public static String do301String(String string) {
        String ret = string;
        ret = ret.replace("?", "??")
                 .replace("'", "?'")
                 .replace(":", "?:")
                 .replace("+", "?+");
        ret = ret.replace("ü", "}")
                 .replace("ä", "{")
                 .replace("ö", "|")
                 .replace("ß", "~");
        ret = ret.replace("Ü", "]")
                 .replace("Ä", "[")
                 .replace("Ö", "\\");
        ret = ret.replace("„", "\"")
                 .replace("“", "\"");

        // ret = ret.replace(":", "?:").replace(",","?,");
        return ret;
    }

    public static String do301NormalizeString(String string) {
        String ret = string;
        ret = ret.replace("}", "ü")
                 .replace("{", "ä")
                 .replace("|", "ö")
                 .replace("~", "ß");
        ret = ret.replace("]", "Ü")
                 .replace("[", "Ä")
                 .replace("\\", "Ö");
        ret = ret.replace("??", "?")
                 .replace("?:", ":")
                 .replace("?'", "'");
        return ret;
    }

    public static int holeZahlVorneNullen(String zahl) {
        int ret = 0;
        for (int i = 0; i < zahl.length(); i++) {
            try {
                if ("0123456789".contains(zahl.substring(i, i + 1))) {
                    return Integer.parseInt(zahl.substring(i));
                }
            } catch (Exception ex) {
                return 0;
            }
        }
        return ret;
    }



    public static String richteNummer(String nummer) {
        try {
            if (nummer.equals("")) {
                return "";
            }
            if (nummer.length() < 3) {
                return "";
            }
            if (!("KGMAERLORHPORSFT").contains(nummer.substring(0, 2))) {
                return "";
            }
            if (nummer.indexOf("\\") >= 0) {
                return nummer.substring(0, nummer.indexOf("\\"));
            }
            return nummer;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }

    public static String cleanupIcdMarker(String sIn) {
        String sTmp = sIn;
        sTmp = sTmp.replace(" ICD10 1: ", "");
        sTmp = sTmp.replace("ICD10 2: ", "");
        sTmp = sTmp.replace("@@", "");
        if ("\n".equals(sTmp)) {
            sTmp = "";
        }
        return sTmp;
    }

    public static String chkIcdFormat(String string) {
        int posDot = string.indexOf(".");
        if ((string.length() > 3) && (posDot < 0)) {
            String tmp1 = string.substring(0, 3);
            String tmp2 = string.substring(3);
            return tmp1 + "." + tmp2;
        }
        return string;
    }

    public static String delZusaetzeFromIcdString(String string) {
        String String1 = string.trim()
                               .substring(0, 1)
                               .toUpperCase();
        String String2 = string.trim()
                               .substring(1)
                               .toUpperCase()
                               .replace(" ", "")
                               .replace("*", "")
                               .replace("!", "")
                               .replace("+", "")
                               .replace("R", "")
                               .replace("L", "")
                               .replace("B", "")
                               .replace("G", "")
                               .replace("V", "")
                               .replace("Z", "");
        if (String2.endsWith(".")) {
            String2 = String2.substring(0, String2.length() - 1);
        }
        return String1 + String2;
    
    }
}
