package CommonTools;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.JFormattedTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;

/*****************************************************************/
public class JRtaDateFieldDocument extends javax.swing.text.PlainDocument {
    // **** Attribute
    private static final String DREI = "0123";// Erlaubte Ziffern Tag 10er
    private static final String MONAT = "01"; // Erlaubte Zeichen Monat 10er
    private Calendar initDate = new GregorianCalendar(); // Calender fuers init
    private String initString; // Voreingestellter String
    private static int trenner1 = 2, trenner2 = 5; // Position vor dem Trenner
    private JFormattedTextField textComponent; // Für Referenz auf das TextFeld
    int newOffset; // Caret Position bei Trennern
    private String emptyDateString = "  .  .    ";
    SimpleDateFormat datumsFormat = new SimpleDateFormat("dd.MM.yyyy"); // Konv.
    // **** Attribute Ende

    // **** Konstruktor 1
    public JRtaDateFieldDocument(JFormattedTextField textComponent, boolean datumHeute) {
        this.textComponent = textComponent; // Hiermit wird jetzt gearbeitet
        initDate.setTime(new Date()); // Kalender auf heute
        initString = datumsFormat.format(initDate.getTime()); // Nach String

        try { // Jetzt den Inhalt mit dem Datum
            if (datumHeute) {
                insertString(0, initString, null); // initialisieren
            } else {
                insertString(0, emptyDateString, null); // initialisieren
            }
        } catch (Exception KonstrEx) {
            KonstrEx.printStackTrace();
        }
    }

    // **** Konstruktor 1 Ende
    // **** Konstruktor 2
    public JRtaDateFieldDocument(JRtaTextField textComponent, Calendar givenDate) {
        this.textComponent = textComponent; // Hiermit wird jetzt gearbeitet
        initDate = givenDate; // Kalender auf Parameter
        initString = datumsFormat.format(initDate.getTime()); // Nach String
        try { // Jetzt den Inhalt mit dem Datum
            insertString(0, initString, null); // initialisieren
        } catch (Exception KonstrEx) {
            KonstrEx.printStackTrace();
        }
    }
    // **** Konstruktor 2 Ende



    // **** Überschreiben Insert-Methode
    @Override
    public void insertString(int offset, String zeichen, AttributeSet attributeSet) throws BadLocationException {
        int lenInhalt = super.getLength();
        if (zeichen.equals(initString) || zeichen.equals(emptyDateString) || (zeichen.length() == 10)) {
            // Wenn initString oder leeres Datum oder komplettes Datum, und richtig, gleich rein
            if (lenInhalt > 0) {
                super.remove(0, lenInhalt);
            }
            super.insertString(0, zeichen, attributeSet);
        } else if (zeichen.length() == 1) { // Wenn nicht, nur Einzelzeichen annehmen
            try {
                Integer.parseInt(zeichen);
            } catch (Exception NumEx) { // Kein Integer?
                return; // Keine Verarbeitung!
            }
            if (offset == 0) { // Tage auf 10 20 30 prüfen
                if (DREI.indexOf(String.valueOf(zeichen.charAt(0))) == -1) {
                    // Toolkit.getDefaultToolkit().beep();
                    return;
                }
            }
            if (offset == 1) { // Tage 32-39 unterbinden
                if (textComponent.getText()
                                 .substring(0, 1)
                                 .equals("3")) {
                    int tag = Integer.valueOf(zeichen)
                                     .intValue();
                    if (tag > 1) {
                        // Toolkit.getDefaultToolkit().beep();
                        return;
                    }
                }
            }
            if (offset == 1) { // Tag 00 unterbinden
                if (textComponent.getText()
                                 .substring(0, 1)
                                 .equals("0")) {
                    int tag = Integer.valueOf(zeichen)
                                     .intValue();
                    if (tag == 0) {
                        // Toolkit.getDefaultToolkit().beep();
                        return;
                    }
                }
            }
            if (offset == 2) { // Monate auf 0x-1x prüfen
                               // (Caret links vom Trenner)
                if (MONAT.indexOf(String.valueOf(zeichen.charAt(0))) == -1) {
                    // Toolkit.getDefaultToolkit().beep();
                    return;
                }
            }
            if (offset == 3) { // Monate auf 0x-1x prüfen
                               // (Caret rechts vom Trenner)
                if (MONAT.indexOf(String.valueOf(zeichen.charAt(0))) == -1) {
                    // Toolkit.getDefaultToolkit().beep();
                    return;
                }
            }
            if (offset == 4) { // Monate 13-19 unterbinden
                if (textComponent.getText()
                                 .substring(3, 4)
                                 .equals("1")) {
                    int monat = Integer.valueOf(zeichen)
                                       .intValue();
                    if (monat > 2) {
                        // Toolkit.getDefaultToolkit().beep();
                        return;
                    }
                }
            }
            if (offset == 4) { // Monat 00 unterbinden
                if (textComponent.getText()
                                 .substring(3, 4)
                                 .equals("0")) {
                    int monat = Integer.valueOf(zeichen)
                                       .intValue();
                    if (monat == 0) {
                        // Toolkit.getDefaultToolkit().beep();
                        return;
                    }
                }
            }

            newOffset = offset;
            if (atSeparator(offset)) { // Wenn am trenner, dann den offset
                newOffset++; // vor dem einfügen um 1 verschieben
                textComponent.setCaretPosition(newOffset);
            }
            super.remove(newOffset, 1); // Aktuelles zeichen entfernen
            super.insertString(newOffset, zeichen, attributeSet); // Neues einfügen
        }
    }
    // **** Überschreiben Insert Ende

    // **** Überschreiben Remove
    @Override
    public void remove(int offset, int length) throws BadLocationException {
        if (atSeparator(offset))
            textComponent.setCaretPosition(offset - 1);
        else
            textComponent.setCaretPosition(offset);
    }
    // **** Überschreiben Remove Ende

    // **** Hilfsmethode für die Punkte zwischen den Feldern
    private boolean atSeparator(int offset) {
        return offset == trenner1 || offset == trenner2;
    }
    // **** Hilfsmethode Ende

    public Calendar getInitDate() {
        return initDate;
    }
}