package splashWin;

import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.jdesktop.swingx.JXPanel;

public class SplashInhalt extends JXPanel {
    BufferedImage img1;

    SplashInhalt() {
        super(new GridLayout(1, 1));
        setBorder(null);
        try {
            File grafik = new File (RehaxSwing.progHome + "icons/Splash_Screen.png");
            img1 = ImageIO.read(grafik);
        } catch (IOException e) {
            e.printStackTrace();
        }
        repaint();

    }

    @Override
    public void paintComponent(Graphics g) {
        g.drawImage(this.img1, 0, 0, this);
    }
}
